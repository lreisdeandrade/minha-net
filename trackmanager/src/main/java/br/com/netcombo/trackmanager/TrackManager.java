package br.com.netcombo.trackmanager;

import java.security.PublicKey;
import java.util.HashSet;

import br.com.netcombo.trackmanager.events.Event;

/**
 * Created by Leandro on 23/02/17.
 */

public class TrackManager {

    private static TrackManager instance;
    private HashSet<Tracker> trackers;

    private TrackManager () {
        this.trackers = new HashSet<Tracker>();
    }

    public static TrackManager getInstance(){
        if(TrackManager.instance == null) {
            TrackManager.instance = new TrackManager();
        }
        return TrackManager.instance;
    }


    public void addTracker(Tracker tracker){
        trackers.add(tracker);
    }

    public void removeTracker(Class cls) {
        HashSet<Tracker> removeTrackers = new HashSet<Tracker>();

        for (Tracker tracker: this.trackers) {
            if (cls.isInstance(tracker)) {
                removeTrackers.add(tracker) ;
            }
        }
    }

    public HashSet<Tracker> getTrackers() {
        return this.trackers;
    }

    public void trackEvent(Event event){
        //TODO fazer em background
        for (Tracker tracker: this.trackers) {
            tracker.trackEvent(event);
        }
    }

}
