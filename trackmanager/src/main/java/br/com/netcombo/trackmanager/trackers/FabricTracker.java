package br.com.netcombo.trackmanager.trackers;

import com.crashlytics.android.answers.AddToCartEvent;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SearchEvent;
import java.util.Currency;

import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.Tracker;
import br.com.netcombo.trackmanager.events.EventContentView;
import br.com.netcombo.trackmanager.events.EventEcommerceAddToCart;
import br.com.netcombo.trackmanager.events.EventLogin;
import br.com.netcombo.trackmanager.events.EventSearch;

/**
 * Created by Leandro on 23/02/17.
 */

public class FabricTracker implements Tracker {

    private Answers fabricTracker;


    public FabricTracker(Answers fabricTracker) {
        this.fabricTracker = fabricTracker;
    }

    @Override
    public void trackEvent(Event event) {

        this.fabricTracker.getInstance();
        switch (event.getType()) {
            case Custom:

                break;
            case EcommerceAddToCart:
                EventEcommerceAddToCart eventEcommerceAddToCart = (EventEcommerceAddToCart) event;

                this.fabricTracker.logAddToCart(new AddToCartEvent()
                        .putItemPrice(eventEcommerceAddToCart.getPrice())
                        .putCurrency(Currency.getInstance(eventEcommerceAddToCart.getCurrency()))
                        .putItemName(eventEcommerceAddToCart.getLabel())
                        .putItemType(eventEcommerceAddToCart.getCategory())
                        .putItemId(eventEcommerceAddToCart.getItemId()));

                break;
            case EcommercePurchase:

                break;
            case EcommerceStartCheckout:

                break;
            case ContentView:
                EventContentView eventContentView = (EventContentView) event;

                this.fabricTracker.getInstance().logContentView(new ContentViewEvent()
                        .putContentName(eventContentView.getLabel())
                        .putContentType(eventContentView.getCategory())
                        .putCustomAttribute("action", eventContentView.getAction())
                        .putContentId(eventContentView.getItemId()));
                break;
            case ContentSearch:
                EventSearch eventSearch = (EventSearch) event;

                this.fabricTracker.getInstance().logSearch(new SearchEvent()
                        .putQuery(event.getLabel())
                        .putCustomAttribute("action", eventSearch.getAction()));
                break;
            case ContentShare:
                break;
            case ContentRated:

                break;
            case UsersSignUp:

                break;
            case UsersLogin:
                EventLogin eventLogin = (EventLogin) event;

                this.fabricTracker.getInstance().logLogin(new LoginEvent()
                        .putMethod(eventLogin.getAction())
                        .putSuccess(true));

                break;
            case GamingLevelStart:

                break;
            case GamingLevelEnd:

                break;

        }

    }
}
