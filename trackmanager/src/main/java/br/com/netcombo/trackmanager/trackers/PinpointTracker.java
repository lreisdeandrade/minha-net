package br.com.netcombo.trackmanager.trackers;

import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.Tracker;
import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsEvent;


/**
 * Created by Leandro on 23/02/17.
 */

public class PinpointTracker implements Tracker {

    private com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsClient analyticsClient ;

    public PinpointTracker(com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsClient analyticsClient) {
        this.analyticsClient = analyticsClient;
    }

    @Override
    public void trackEvent(Event event) {
        final AnalyticsEvent analyticsEvent = this.analyticsClient.createEvent(event.getType().toString())
                 .withAttribute("Category", event.getCategory())
                 .withAttribute("Action", event.getAction())
                 .withAttribute("Label", event.getLabel());

        this.analyticsClient.recordEvent(analyticsEvent);
        this.analyticsClient.submitEvents();
    }
}
