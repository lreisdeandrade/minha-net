package br.com.netcombo.trackmanager.events;

/**
 * Created by Leandro on 03/03/17.
 */

public class EventContentView extends Event{
    private String itemId;

    public EventContentView(String category, String action, String label, String itemId) {
        super(category, action, label, EventType.ContentView);

        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }
}
