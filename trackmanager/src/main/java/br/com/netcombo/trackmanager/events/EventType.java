package br.com.netcombo.trackmanager.events;

/**
 * Created by Leandro on 01/03/17.
 */

public enum EventType {

    Default,

    Custom,

    EcommerceAddToCart,
    EcommercePurchase,
    EcommerceStartCheckout,

    ContentView,
    ContentSearch,
    ContentShare,
    ContentRated,

    UsersSignUp,
    UsersLogin,
    UsersInvite,

    GamingLevelStart,
    GamingLevelEnd


}
