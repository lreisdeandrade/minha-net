package br.com.netcombo.trackmanager.events;

/**
 * Created by Leandro on 03/03/17.
 */

public class EventSearch extends Event {

    public EventSearch(String category, String action, String label, EventType type) {
        super(category, action, label, type);
    }
}
