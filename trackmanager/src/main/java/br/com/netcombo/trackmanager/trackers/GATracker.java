package br.com.netcombo.trackmanager.trackers;

import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.Tracker;

import com.google.android.gms.analytics.HitBuilders;
/**
 * Created by Leandro on 23/02/17.
 */

public class GATracker implements Tracker {

    private com.google.android.gms.analytics.Tracker gaTracker ;

    public GATracker(com.google.android.gms.analytics.Tracker gaTracker) {
        this.gaTracker = gaTracker;
    }

    @Override
    public void trackEvent(Event event) {
        gaTracker.send(new HitBuilders.EventBuilder()
                .setCategory(event.getCategory())
                .setAction(event.getAction())
                .setLabel(event.getLabel())
                .build());
    }
}
