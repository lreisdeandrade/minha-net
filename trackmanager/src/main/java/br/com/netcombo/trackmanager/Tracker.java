package br.com.netcombo.trackmanager;

import br.com.netcombo.trackmanager.events.Event;

/**
 * Created by Leandro on 23/02/17.
 */

public interface Tracker  {

    public void trackEvent(Event event);
}
