package br.com.netcombo.trackmanager.events;

/**
 * Created by Leandro on 23/02/17.
 */

public abstract class Event {

    private String category ;
    private String action ;
    private String label ;
    private EventType type ;

    public Event(String category, String action, String label, EventType type) {
        this.category = category;
        this.action = action;
        this.label = label;
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public String getAction() {
        return action;
    }
    public String getLabel() {
        return label;
    }
    public EventType getType() {
        return type;
    }
}
