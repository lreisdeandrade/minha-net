package br.com.netcombo.trackmanager.events;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by Leandro on 03/03/17.
 */

public class EventEcommerceAddToCart extends Event {

    private String itemId;
    private BigDecimal price;
    private String currency;

    public EventEcommerceAddToCart(String category, String action, String label, String itemId, BigDecimal price,
                          String currency) {
        super(category, action, label, EventType.EcommerceAddToCart);

        this.itemId = itemId;
        this.price = price;
        this.currency = currency;
    }

    public String getItemId() {
        return itemId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }
}
