package br.com.netcombo.trackmanager;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.android.gms.analytics.GoogleAnalytics;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.netcombo.trackmanager.trackers.FabricTracker;
import br.com.netcombo.trackmanager.trackers.GATracker;
import br.com.netcombo.trackmanager.trackers.PinpointTracker;
import io.fabric.sdk.android.Fabric;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private Context context;

    @Before
    public void setup() {
        this.context = InstrumentationRegistry.getTargetContext();
    }

        @Test
        public void addTrackers_isCorrect ()throws Exception {
            // GOOGLE
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this.context);
            com.google.android.gms.analytics.Tracker tracker = analytics.newTracker("UA-000-1");
            GATracker gaTracker = new GATracker(tracker);

            TrackManager trackManager = TrackManager.getInstance();


            // Fabric
            Fabric.with(this.context, new Crashlytics());
            FabricTracker fabricTracker = new FabricTracker(Answers.getInstance());
            trackManager.addTracker(fabricTracker);
            trackManager.addTracker(gaTracker);

            assertEquals(2, TrackManager.getInstance().getTrackers().size());
        }
    }
