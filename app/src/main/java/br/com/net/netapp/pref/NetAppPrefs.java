package br.com.net.netapp.pref;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Leandro on 07/09/17.
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface NetAppPrefs {

    @DefaultBoolean(true)
    boolean firsTime();

}