package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.net.netapp.BuildConfig;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 06/09/17.
 */

@RealmClass
public class App extends RealmObject {

    @SerializedName("_id")
    @Expose
    @PrimaryKey
    private String id;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("settings")
    @Expose
    private Settings settings;

    @SerializedName("androidVersion")
    @Expose
    private String androidVersion;

    private Boolean enabledTouchId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public Boolean getEnabledTouchId() {
        return enabledTouchId;
    }

    public void setEnabledTouchId(Boolean enabledTouchId) {
        this.enabledTouchId = enabledTouchId;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public static void createDefault() {
        Realm realm = Realm.getDefaultInstance();


        realm.executeTransaction(realm1 -> {
            App app = new App();

            app.setId("default");
            app.setKey("a1041cb0-56da-11e7-9873-e953f76b8e41");
            app.setCode("net-hdtv");
            app.setName("minha net");
            app.setAndroidVersion(BuildConfig.VERSION_NAME);

            Settings settings = new Settings();
            settings.setSelectedStateId(1);
            settings.setSolrDefaultCityId(1);

            app.setSettings(settings);
            app.setEnabledTouchId(false);

            realm1.insertOrUpdate(app);

        });

    }

    @Override
    public String toString() {
        return "App{" +
                "id='" + id + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", settings=" + settings +
                ", androidVersion='" + androidVersion + '\'' +
                ", enabledTouchId=" + enabledTouchId +
                '}';
    }
}
