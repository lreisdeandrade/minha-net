package br.com.net.netapp.service.mobilebackend.models;

/**
 * Created by Leandro on 10/08/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WorkOrder extends RealmObject{

    public static final SimpleDateFormat DATEPARSER_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'+'00:00");
    public static final SimpleDateFormat DATA_MES = new SimpleDateFormat("dd 'de' MMMM");
    public static final SimpleDateFormat HORA_MIN = new SimpleDateFormat("HH:mm");

    @SerializedName("workorderId")
    @Expose
    @PrimaryKey
    private String workOrderId;

    @SerializedName("timeSlotStart")
    @Expose
    private String timeSlotStart;
    @SerializedName("timeSlotEnd")
    @Expose
    private String timeSlotEnd;

    @SerializedName("scheduleDate")
    @Expose
    private String scheduleDate;


    public String getVTFormatedTime() {
        Date hourStart = null;
        Date hourEnd = null;
        Date hourExtended = null;
        Calendar cal = null;
        // Log.d(TAG,"timeSlotStart " +  this.timeSlotStart);
        try {

            hourStart = DATEPARSER_FORMAT.parse(this.scheduleDate);
            hourEnd = DATEPARSER_FORMAT.parse(this.timeSlotEnd);
            hourExtended = DATEPARSER_FORMAT.parse(this.timeSlotEnd);

            cal = Calendar.getInstance(); // creates calendar
            cal.setTime(hourExtended); // sets calendar time/date
            cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
            cal.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
            //  Log.d(TAG, e.toString());
        }

        return DATA_MES.format(hourStart) + " Entre " + HORA_MIN.format(hourStart) + " e " + HORA_MIN.format(hourEnd);
    }

    public String getVTExtendedTime(){
        Date hourExtended = null;
        Calendar cal = null;
        //Log.d(TAG,"timeSlotStart " +  this.timeSlotStart);
        try {

            hourExtended = DATEPARSER_FORMAT.parse(this.timeSlotEnd);
            cal = Calendar.getInstance(); // creates calendar
            cal.setTime(hourExtended); // sets calendar time/date
            cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
            cal.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
            //    Log.d(TAG, e.toString());
        }

        return "Não se esqueça que o serviço pode se estender até " + HORA_MIN.format(cal.getTime());
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public String getTimeSlotStart() {
        return timeSlotStart;
    }

    public void setTimeSlotStart(String timeSlotStart) {
        this.timeSlotStart = timeSlotStart;
    }

    public String getTimeSlotEnd() {
        return timeSlotEnd;
    }

    public void setTimeSlotEnd(String timeSlotEnd) {
        this.timeSlotEnd = timeSlotEnd;
    }



}