package br.com.net.netapp.service.mobilebackend.clients.account.workorder;

import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Leandro on 10/08/17.
 */

public interface IWorkOrderClient {

    @GET("technical-support/workorder")
    Call<ResponseDataCollection<WorkOrder>> getWorkOrders(@Header("x-access-token") String xAccessToken);
}
