package br.com.net.netapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.com.net.netapp.R;
import br.com.net.netapp.ui.component.epg.SearchListener;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import br.com.net.netapp.ui.search.TabSearchFragment;
import br.com.net.netapp.ui.search.TabSearchFragment_;

/**
 * Created by Leandro on 02/06/17.
 */

public abstract class SearchActivity extends BaseActivity implements CustomRecyclerListener,
        MenuItemCompat.OnActionExpandListener {


    private Menu menu;
    private SearchView searchView;
    private MenuItem menuItem;
    private SearchListener listener;
    private TabSearchFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        return super.onCreateOptionsMenu(menu);
    }

    public void addSearchButton() {
        menuItem = menu.add(Menu.NONE, R.id.search_item, menu.size() + 1,
                R.string.menu_item_epg_search).setIcon(R.drawable.ic_search);
        MenuItemCompat.setShowAsAction(menuItem,
                MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW |
                        MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        if (searchView == null) {
            searchView = new SearchView((this).getSupportActionBar().getThemedContext());
        }
        MenuItemCompat.setActionView(menuItem, searchView);
        MenuItemCompat.setOnActionExpandListener(menuItem, this);
    }

    public SearchView getSearchItem() {
        return searchView;
    }

    public void setSearchListener(SearchListener listener) {
        this.listener = listener;
    }

    public Menu getMenu() {
        return menu;
    }

    public void inflateSearchResultsView() {
        fragment = TabSearchFragment_.builder().highLightChannels(null).build();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        if (listener != null) {
            listener.onSearchOpen();
        }
        if (fragment == null) {
            inflateSearchResultsView();
        }
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        if (listener != null) {
            listener.onSearchClose();
        }
        onBackPressed();
        return true;
    }

    @Override
    public void onItemClick(View view, Object item, int position) {
        //UNUSED
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void removeInstanceFragment(){
        fragment = null;
    }
}
