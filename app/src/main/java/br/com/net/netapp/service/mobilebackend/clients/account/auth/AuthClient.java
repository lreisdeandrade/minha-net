package br.com.net.netapp.service.mobilebackend.clients.account.auth;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.contract.IContractClient;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.models.Signout;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 19/09/17.
 */

public class AuthClient extends BaseClient {
    private String TAG = "EpgClient";
    private IAuthClient authClient;

    public AuthClient() {
        super(BuildConfig.BASE_URL_ACCOUNT);

        this.authClient = this.retrofit.create(IAuthClient.class);
    }

    public void signin(String user, String password, final CallBackClient<User> callback) {

        HashMap<String, String> paramsMap = new LinkedHashMap<String, String>();
        paramsMap.put("username", user);
        paramsMap.put("password", password);

        this.authClient.signin(paramsMap).enqueue(new Callback<ResponseData<User>>() {
            @Override
            public void onResponse(Call<ResponseData<User>> call, Response<ResponseData<User>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                } else if (response.code() == 400){

                    callback.onFailure(new Exception("erro: " + "usuário ou senha inválido"));
                }else{
                    callback.onFailure(new Exception("erro: " + "Ocorreu um erro"));

                }
            }

            @Override
            public void onFailure(Call<ResponseData<User>> call, Throwable t) {
            }
        });
    }

    public void signout(String accessToken, final CallBackClient<Signout> callback) {

        this.authClient.signout(accessToken).enqueue(new Callback<ResponseData<Signout>>() {
            @Override
            public void onResponse(Call<ResponseData<Signout>> call, Response<ResponseData<Signout>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onFailure(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<ResponseData<Signout>> call, Throwable t) {
            }
        });
    }

    public void autoSignin(String accessToken, final CallBackClient<User> callback) {

        this.authClient.autoSignin(accessToken).enqueue(new Callback<ResponseData<User>>() {
            @Override
            public void onResponse(Call<ResponseData<User>> call, Response<ResponseData<User>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onFailure(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<ResponseData<User>> call, Throwable t) {
            }
        });
    }
}
