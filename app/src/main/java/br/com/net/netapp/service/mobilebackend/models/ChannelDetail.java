package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 17/05/17.
 */

@RealmClass
public class ChannelDetail extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;
    @SerializedName("number")
    private Integer number;

    @SerializedName("categoryId")
    private Integer categoryId;

    @SerializedName("order")
    private Integer order;

    @SerializedName("logoUrl")
    private String logoUrl;

    @SerializedName("exhibitions")
    private RealmList<Exhibition> exhibitions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public RealmList<Exhibition> getExhibitions() {
        return exhibitions;
    }

    public void setExhibitions(RealmList<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }
}