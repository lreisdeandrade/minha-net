package br.com.net.netapp.ui.component.item;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;

/**
 * Created by thiagomagalhaes on 19/05/17.
 */

@EViewGroup(R.layout.item_exhibition)
public class ExhibitionItemView extends LinearLayout implements BindableItemView<Exhibition> {

    @ViewById
    TextView hourTextView;

    @ViewById
    TextView genreTextView;

    @ViewById
    TextView titleTextView;

    @ViewById
    RelativeLayout linearBase;

    public ExhibitionItemView(Context context) {

        super(context);
    }

    @Override
    public void bind(Exhibition object) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());

        hourTextView.setText(dateFormatter.format(object.getStartDate()));

        genreTextView.setText(object.getGenre());

        if (object.isRunning()) {
            linearBase.setBackgroundColor(Color.parseColor("#ecf5fc"));
        } else {
            linearBase.setBackgroundColor(Color.parseColor("#f8f8f8"));
        }




        titleTextView.setText(object.getTitle());
    }
}
