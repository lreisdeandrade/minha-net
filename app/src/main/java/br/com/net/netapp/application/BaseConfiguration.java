package br.com.net.netapp.application;

import android.app.Application;
import android.content.Context;

import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsClient;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.squareup.leakcanary.LeakCanary;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.pref.NETAppPref_;
import br.com.net.netapp.service.aws.mobile.AWSMobileClient;
import br.com.net.netapp.service.aws.mobile.push.PushManager;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.trackers.FabricTracker;
import br.com.netcombo.trackmanager.trackers.GATracker;
import br.com.netcombo.trackmanager.trackers.PinpointTracker;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by elourenco on 25/04/17.
 */


@EBean
public class BaseConfiguration {

    @Pref
    NETAppPref_ netAppPref;

    protected static final String TAG = "BaseConfiguration";

//    @Bean
//    SessionManager sessionManager;

    public void setupMemoryLeakDetect(Application application, Context context) {
        if (LeakCanary.isInAnalyzerProcess(context)) {
            return;
        }
        LeakCanary.install(application);
    }

    public void setupRealm(Context context) {
        Realm.init(context);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfig);
    }

    @Background
    public void registerAWSMobileClientPushNotification(Context context) {
        AWSMobileClient.initializeMobileClientIfNecessary(context);
        PushManager pushManager = AWSMobileClient.defaultMobileClient()
                .getPushManager();

        try {
            pushManager.registerDevice();

            if (pushManager.isRegistered()) {
                pushManager.setPushEnabled(true);
                pushManager.subscribeToTopic(pushManager.getDefaultTopic());
                AWSMobileClient.defaultMobileClient().getPinpointManager().getTargetingClient().updateEndpointProfile();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }


    //Setup TrackManager
    @Background
    public void setupTrackManager(Context context) {
        createTrackManagerGoogleAnalitycs(context);
        createTrackManagerPinpoint(context);
        createTrackManagerFabricIO(context);
    }

    private void createTrackManagerPinpoint(Context context) {
        AWSMobileClient.initializeMobileClientIfNecessary(context);
        AnalyticsClient analyticsClient = AWSMobileClient.defaultMobileClient().getPinpointManager().getAnalyticsClient();
        PinpointTracker pinpointTracker = new PinpointTracker(analyticsClient);
        TrackManager.getInstance().addTracker(pinpointTracker);
    }

    private void createTrackManagerGoogleAnalitycs(Context context) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        Tracker tracker = analytics.newTracker(BuildConfig.GOOGLE_ANALITYCS);
        GATracker gaTracker = new GATracker(tracker);
        TrackManager.getInstance().addTracker(gaTracker);
    }

    private void createTrackManagerFabricIO(Context context) {
        Fabric.with(context, new Crashlytics());
        FabricTracker fabricTracker = new FabricTracker(Answers.getInstance());
        TrackManager.getInstance().addTracker(fabricTracker);
    }
}
