package br.com.net.netapp.manager;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.activity.SinopseActivity_;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.event.UserTrySignoutEvent;
import br.com.net.netapp.model.User;
import br.com.net.netapp.pref.NETAppPref_;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.auth.AuthClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.reminder.ReminderClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.service.mobilebackend.models.Signout;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import br.com.net.netapp.ui.AlarmHelper;
import br.com.net.netapp.ui.NotificationHelper;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by Leandro on 24/04/17.
 */

@EBean
public class SessionManager {

    @Pref
    NETAppPref_ netAppPref;

    @RootContext
    Context context;

    AuthClient authClient;

    private String TAG = "SessionManager";
    private Realm realm = Realm.getDefaultInstance();

    public boolean isActive() {
        boolean userIsActive = realm.where(User.class).count() > 0;
        return userIsActive;
    }

    public User getUser() {
        User user = realm.where(User.class).findFirst();
        return user;
    }

    public Contract getContractSelected(String contractSelectedCode) {
        Contract contract = realm.where(Contract.class).equalTo("code", contractSelectedCode).findFirst();
        return contract;
    }

    public void createUserInSession(final User user) {
        realm.beginTransaction();
        realm.insertOrUpdate(user);
        realm.commitTransaction();

        loadReminders();

    }


    protected void loadReminders() {
        ReminderClient reminderClient = new ReminderClient();
        reminderClient.loadReminders(getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                Timber.e(errorMessage.getMessage(), "onError: %s", errorMessage.getMessage());
//                finish();
            }

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);
                sincReminders();
//                finish();
            }
        });
    }

    private void sincReminders() {
        realm.executeTransaction(realm -> {
            RealmResults<Reminder> reminders = realm.where(Reminder.class).findAll();
            for (Reminder reminder : reminders) {
                scheduleNotification(reminder.getExhibition());
            }
        });
    }

    private void scheduleNotification(Exhibition exhibition) {
        String text = context.getString(R.string.reminder_text, exhibition.getTitle());
        NotificationHelper notificationHelper = NotificationHelper.getInstance();
        Intent sinopseIntent = SinopseActivity_.intent(context).idExhibition(exhibition.getId()).get();
        Notification notification = notificationHelper.sendExpandLayoutNotification(
                sinopseIntent, context.getString(R.string.reminder_intro), text);
        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(new Date());
        calendar.setTime(exhibition.getStartDate());
        calendar.add(Calendar.MINUTE, -15);
        AlarmHelper.scheduleNotification(context, notification, Integer.parseInt(exhibition.getId()), calendar.getTimeInMillis());
    }

    public App getAppSettings() {
        App app = realm.where(App.class).findFirst();
        return app;
    }

    public void deleteUserInSession() {
        realm.beginTransaction();
        realm.where(User.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void showLogoutDialog() {

        new MaterialDialog.Builder(context)
                .title("Logout")
                .positiveText(R.string.sim)
                .negativeText(R.string.nao)
                .content(R.string.logout_content).onPositive((dialog, which) -> {
            EventBus.getDefault().post(new UserTrySignoutEvent());

            logout();

        }).show();

    }

    public void logout() {

        authClient = new AuthClient();
        authClient.signout(getUser().getToken(), new CallBackClient<Signout>() {
            @Override
            public void onSuccess(Signout result) {
                Log.d(TAG, result.toString());

                cleanDataBaseUserInfos();
                deleteUserInSession();

                EventBus.getDefault().post(new UserSignoutEvent());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, t.toString());

            }
        });
    }

    public void cleanDataBaseUserInfos() {
        realm.executeTransaction(realm1 -> {
            RealmResults<Reminder> reminders = realm1.where(Reminder.class).findAll();
            if (reminders != null) {
                reminders.deleteAllFromRealm();
            }
        });

        realm.executeTransaction(realm1 -> {
            RealmResults<WorkOrder> workOrders = realm1.where(WorkOrder.class).findAll();
            if (workOrders != null) {
                workOrders.deleteAllFromRealm();
            }
        });

        realm.executeTransaction(realm1 -> {
            RealmResults<BookmarkChannel> bookmarks = realm1.where(BookmarkChannel.class).findAll();
            if (bookmarks != null) {
                bookmarks.deleteAllFromRealm();
            }
        });

        realm.executeTransaction(realm1 -> {
            RealmResults<Contract> contracts = realm1.where(Contract.class).findAll();
            if (contracts != null) {
                contracts.deleteAllFromRealm();
            }
        });
    }

    public HashMap<String, String> generateDeviceInfo() {
        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();
        queryStringMap.put("app", BuildConfig.APPLICATION_ID);
        if (isActive()) {
            queryStringMap.put("userId", getUser().getDocumentId());
            queryStringMap.put("nome", getUser().getName());
        }
        queryStringMap.put("notificationToken", netAppPref.gcmToken().get());
        queryStringMap.put("platform", "android");
        queryStringMap.put("deviceType", "smartphone");
        queryStringMap.put("version", BuildConfig.VERSION_NAME);
        queryStringMap.put("logged", String.valueOf(isActive()));

        return queryStringMap;
    }
}
