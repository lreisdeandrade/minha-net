package br.com.net.netapp.service.mobilebackend.clients.tv.bookmark;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Leandro on 27/06/17.
 */

public interface IBookmarkClient {
    @GET("bookmark/live-channels")
    Call<ResponseDataCollection<BookmarkChannel>> loadBookmarks(@Header("x-access-token") String xAccessToken, @QueryMap HashMap<String, String> queryString);

    @POST("bookmark/live-channels")
    Call<ResponseData<BookmarkChannel>> createBookmark(@Header("x-access-token") String xAccessToken,
                                                       @Body HashMap<String, String> queryString);

    @DELETE("bookmark/live-channels/{channelId}")
    Call<ResponseBody> deleteBookmark(@Header("x-access-token") String xAccessToken,
                                      @Path("channelId") int channelId);

}
