package br.com.net.netapp.service.mobilebackend.clients.tv.reminder;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReminderClient extends BaseClient {

    private String TAG = "br.com.net.netapp.ReminderClient";
    private IReminderClient iReminderClient;

    public ReminderClient() {
        super(BuildConfig.BASE_URL_TV);

        this.iReminderClient = this.retrofit.create(IReminderClient.class);
    }

    public void loadReminders(String xAcessToken, final RealmCallbackClient callbackClient) {

        this.iReminderClient.loadReminders(xAcessToken).enqueue(new Callback<ResponseDataCollection<Reminder>>() {
            @Override
            public void onResponse(Call<ResponseDataCollection<Reminder>> call,
                                   Response<ResponseDataCollection<Reminder>> response) {

                if (response.isSuccessful()) {
                    callbackClient.onSuccess(response.body().getDataInfo());
                } else {
                    ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                    callbackClient.onError(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<ResponseDataCollection<Reminder>> call, Throwable t) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                callbackClient.onError(errorMessage);
            }
        });
    }

    public void createReminder(String xAcessToken, String exhibitionId, final RealmCallbackClient callbackClient) {

        this.iReminderClient.createReminder(xAcessToken, exhibitionId)
                .enqueue(new Callback<ResponseData<Reminder>>() {
                    @Override
                    public void onResponse(Call<ResponseData<Reminder>> call,
                                           Response<ResponseData<Reminder>> response) {

                        if (response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().data);
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<Reminder>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }

    public void deleteReminder(String xAccessToken, String reminderId, final RealmCallbackClient callbackClient) {
        this.iReminderClient.deleteReminder(xAccessToken, reminderId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            //TODO: Verificar se o tratamento correto é mesmo esse,
                            // com envio de um boolean
                            callbackClient.onSuccess(true);
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }

}
