package br.com.net.netapp.service.mobilebackend.clients.tv.search;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.models.Data;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 05/06/17.
 */

public class SearchClient extends BaseClient {
    private String TAG = "EpgClient";
    private ISearchClient searchClient;

    public SearchClient() {
        super(BuildConfig.BASE_URL_TV);

        this.searchClient = this.retrofit.create(ISearchClient.class);
    }

    public void search(String query, String cityId, final CallBackClient callbackClient) {
        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("q", query);
        queryStringMap.put("cityId", cityId);


        this.searchClient.searchItems(queryStringMap).enqueue(new Callback<ResponseData<Data>>() {
            @Override
            public void onResponse(Call<ResponseData<Data>> call, Response<ResponseData<Data>> response) {
                if(response.isSuccessful()) {
                    callbackClient.onSuccess(response.body().getData());
                } else {
                    ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                    callbackClient.onFailure(new Throwable(errorMessage.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseData<Data>> call, Throwable t) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                callbackClient.onFailure(t);
            }
        });
    }
}