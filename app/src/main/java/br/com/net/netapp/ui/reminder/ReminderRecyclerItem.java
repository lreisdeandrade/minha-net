package br.com.net.netapp.ui.reminder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 09/06/17.
 */

public class ReminderRecyclerItem extends CustomRecyclerItem<Reminder,
        ReminderRecyclerItem, ReminderRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends ReminderRecyclerItem.ViewHolder> FACTORY = new ReminderRecyclerItem.ItemFactory();
    private Context context;

    public ReminderRecyclerItem(Context context, Reminder item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.reminder_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.reminder_recycler_item;
    }

    @Override
    public void bindView(ReminderRecyclerItem.ViewHolder viewHolder) {
        super.bindView(viewHolder);
        Reminder viewModel = getModel();
        viewHolder.reminderDateStart.setText(viewModel.getExhibition().getFormatedDayMonthAndHour());
        viewHolder.reminderTitle.setText(viewModel.getExhibition().getTitle());

        if(viewModel.getExhibition().getTvShow() != null)
        viewHolder.reminderGenre.setText(viewModel.getExhibition().getTvShow().getGenres() != null ? viewModel.getExhibition().getTvShow().getGenres() : "");

    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<ReminderRecyclerItem.ViewHolder> {
        public ReminderRecyclerItem.ViewHolder create(View v) {
            return new ReminderRecyclerItem.ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ReminderRecyclerItem.ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView reminderTitle;
        TextView reminderDateStart;
        TextView reminderGenre;


        public ViewHolder(View view) {
            super(view);
            this.reminderTitle = (TextView) view.findViewById(R.id.reminder_title);
            this.reminderDateStart = (TextView) view.findViewById(R.id.reminder_date_start);
            this.reminderGenre = (TextView) view.findViewById(R.id.reminder_genre);
        }
    }
}