package br.com.net.netapp.receiver;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.net.netapp.activity.MainActivity_;
import br.com.net.netapp.activity.MyTechnicianActivity_;
import br.com.net.netapp.activity.SplashScreenActivity;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.helper.NotificationHandler;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.EventContentView;
import timber.log.Timber;

/**
 * Created by Leandro on 14/09/17.
 */

@EReceiver
public class NotificationBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "NotificationBroadcastReceiver";
    @Bean
    NotificationHandler notificationHandler;

    private PendingIntent pendingIntent = null;
    private String pushNotification;
    private String title = "";
    private String custom = "";
    private String body = "";
    private String action = "";
    private JSONObject jsonPush;


    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("Receive");

        pushNotification = intent.getExtras().getString("message");

        if (pushNotification != null) {
            generateCustomInfoNotification(context);
        }
    }

    private void generateCustomInfoNotification(Context context) {
        try {
            jsonPush = new JSONObject(pushNotification);
            custom = jsonPush.getString("custom");

            body = jsonPush.getString("body");
            title = jsonPush.getString("title");

            if (custom != null) {
                JSONObject customObj = new JSONObject(custom);
                action = customObj.getString("action");
                String operation = customObj.getString("operation");

                if (operation != null) {
                    if (operation.equals(Constants.DISPLACEMENT)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "deslocamento-tecnico", null));
                    } else if (operation.equals(Constants.CREATE)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "abertura-vt", null));
                    } else if (operation.equals(Constants.TECHNICAL_FEEDBACK)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "avalição técnica", null));
                    } else if (operation.equals(Constants.TECHNICAL_CHAT)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "chat técnico", null));
                    } else if (operation.equals(Constants.TECHNICAL_CANCEL)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "cancelamento-vt", null));
                    } else if (operation.equals(Constants.TECHNICAL_RESCHEDULING)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "reagendamento-vt", null));
                    } else if (operation.equals(Constants.TECHNICAL_FINISH)) {
                        TrackManager.getInstance().trackEvent(new EventContentView("meu-tecnico", "click-push", "finalizou-vt", null));
                    }
                }

                if (action != null && !operation.equals(Constants.TECHNICAL_CANCEL) && !operation.equals(Constants.TECHNICAL_FINISH)) {
                    if (AppContext.isOnForeground(context)) {
                        pendingIntent = PendingIntent.getActivity(context,
                                0, new Intent(context, MyTechnicianActivity_.class).putExtra("action", action).putExtra("operation", operation),
                                Notification.DEFAULT_LIGHTS | PendingIntent.FLAG_ONE_SHOT);
                    } else {
                        pendingIntent = PendingIntent.getActivity(context,
                                0, new Intent(context, MainActivity_.class).putExtra("action", action).putExtra("operation", operation),
                                Notification.DEFAULT_LIGHTS | PendingIntent.FLAG_ONE_SHOT);
                    }
                } else if (!AppContext.isOnForeground(context)) {
                    pendingIntent = PendingIntent.getActivity(context,
                            0, new Intent(context, MainActivity_.class),
                            Notification.DEFAULT_LIGHTS | PendingIntent.FLAG_ONE_SHOT);
                }

                notificationHandler.buildNotification(context, title, body, pendingIntent);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
