package br.com.net.netapp.ui.contract;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import br.com.net.netapp.R;
import br.com.net.netapp.manager.SessionManager_;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 20/09/17.
 */

public class ContractRecyclerItem extends CustomRecyclerItem<Contract,
        ContractRecyclerItem, ContractRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends ContractRecyclerItem.ViewHolder> FACTORY = new ContractRecyclerItem.ItemFactory();
    private Context context;

    public ContractRecyclerItem(Context context, Contract item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.contract_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.contract_recycler_item;
    }

    @Override
    public void bindView(ContractRecyclerItem.ViewHolder viewHolder) {
        super.bindView(viewHolder);
        Contract viewModel = getModel();
        viewHolder.contractCode.setText(String.format("%s/%s", viewModel.getOperationCode(), viewModel.getCode()));
        viewHolder.contractAddress.setText(String.format("%s - %s\nCEP: %s",
                viewModel.getAddress().getCity(),
                viewModel.getAddress().getStateCode(),
                viewModel.getAddress().getZipCode()));

        if (SessionManager_.getInstance_(context).isActive() &&
                SessionManager_.getInstance_(context).getUser().getContractSelectedCode() != null  &&
                SessionManager_.getInstance_(context).getUser().getContractSelectedCode().equals(viewModel.getCode())) {
            viewHolder.lnContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.cinzaClaro));
        }
    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<ContractRecyclerItem.ViewHolder> {
        public ContractRecyclerItem.ViewHolder create(View v) {
            return new ContractRecyclerItem.ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ContractRecyclerItem.ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView contractCode, contractAddress;
        LinearLayout lnContainer;


        public ViewHolder(View view) {
            super(view);
            this.contractCode = (TextView) view.findViewById(R.id.contract_code);
            this.contractAddress = (TextView) view.findViewById(R.id.contract_address);
            this.lnContainer = (LinearLayout) view.findViewById(R.id.ln_container);

        }
    }
}