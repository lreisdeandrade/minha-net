package br.com.net.netapp.ui.region;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;

import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.pref.NetAppPrefs_;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.region.RegionClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.Channel;
import br.com.net.netapp.service.mobilebackend.models.City;
import br.com.net.netapp.service.mobilebackend.models.Epg;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Screen;
import br.com.net.netapp.service.mobilebackend.models.State;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by Leandro on 06/09/17.
 */

@EViewGroup(R.layout.region_view)
public class RegionView extends RelativeLayout {

    Context context;
    Realm realm;

    private RegionClient stateClient;

    @ViewById
    TextView regionStateSelected, regionCitySelected;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    LinearLayout lnEstado, lnCity;

    @ViewById
    FloatingActionButton fabSelectConfig;

    @Bean
    SessionManager sessionManager;

    @org.androidannotations.annotations.App
    AppContext appContext;

    int selectedCityId;
    String ufSelected;

    ArrayList<String> stateStringList;
    RealmList<State> statesList;

    RealmList<City> cityList;
    ArrayList<String> cityStringList;

    RegionListener listener;

    @Pref
    NetAppPrefs_ netAppPrefs;

    String adminArea;
    String adminSubArea;

    public RegionView(Context context, Realm realm, String adminArea, String adminSubArea, RegionListener listener) {
        super(context);
        this.context = context;
        this.realm = realm;
        this.listener = listener;
        this.adminArea = adminArea;
        this.adminSubArea = adminSubArea;
    }

    @AfterViews
    protected void setupView() {


        this.stateClient = new RegionClient();

        RealmResults<State> statesResult = realm.where(State.class).findAll();

        if (statesResult != null && !statesResult.isEmpty()) {
            statesList = new RealmList<>();
            statesList.addAll(statesResult.subList(0, statesResult.size()));
            setupScreenFromCache(statesList);
            statesResult.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
                @Override
                public void onChange(RealmResults<State> element) {

                }
            });
        }

        loadStates();
    }

    private void setupScreenFromCache(RealmList<State> statesList) {

        cityList = new RealmList<>();
        for (State state : statesList) {
            if (state.getName().equals(sessionManager.getAppSettings().getSettings().getSelectedStateName())) {
                for (City city : state.getCities()) {
                    cityList.add(city);
                }
            }
        }

        regionStateSelected.setText(sessionManager.getAppSettings().getSettings().getSelectedStateName());

        regionCitySelected.setText(sessionManager.getAppSettings().getSettings().getSelectedCityName());
    }

    private void loadStates() {
        progressBar.setVisibility(View.VISIBLE);
        this.stateClient.loadStates(new RealmCallbackClient() {

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);

                statesList = response;
                progressBar.setVisibility(View.GONE);


                if (adminArea != null && adminSubArea != null) {
                    setupLocationAutomatic();
                } else {

                    if (!regionStateSelected.getText().toString().equals(sessionManager.getAppSettings().getSettings().getSelectedStateName())) {
                        showStates(statesList);
                    }
                }
            }

            @Override
            public void onError(ErrorMessage errorMessage) {
                Timber.d("error %s", errorMessage.toString());
                progressBar.setVisibility(View.GONE);

                State.createDefault();

                RealmResults<State> statesResult = realm.where(State.class).findAll();

                RealmList<State> states = new RealmList<State>();

                states.addAll(statesResult.subList(0, statesResult.size()));

                if (!regionStateSelected.getText().toString().equals(sessionManager.getAppSettings().getSettings().getSelectedStateName())) {
                    showStates(states);
                }
            }
        });
    }

    private void setupLocationAutomatic() {

        boolean findCity = false;
        for (State state : statesList) {
            if (state.getName().equals(adminArea)) {
                regionStateSelected.setText(state.getName());
                cityList = state.getCities();
                for (City city : state.getCities()) {
                    if (city.getName().equals(adminSubArea)) {
                        findCity = true;
                        regionCitySelected.setText(city.getName());
                        selectedCityId = city.getExternalIds().getSolrId();
                        updateRealmSettings();
                    }
                }
            }
        }

        if (!findCity) {

            new MaterialDialog.Builder(context)
                    .title("Atenção")
                    .neutralText(R.string.btn_ok)
                    .content(R.string.location_not_find)
                    .onNeutral((dialog, which) -> {
                        cityClicked();
                        dialog.dismiss();

                    }).show();
        }
    }

    protected void showStates(RealmList<State> states) {

        AlertDialog.Builder alertEstados = new AlertDialog.Builder(context);
        alertEstados.setTitle("Selecione seu Estado:");

        stateStringList = new ArrayList<>();

        for (State state : states) {
            stateStringList.add(state.getName());

        }

        String[] stateArray = new String[states.size()];
        stateArray = stateStringList.toArray(stateArray);

        alertEstados.setItems(stateArray, (dialog, which) -> {
            dialog.dismiss();
            regionStateSelected.setText(stateStringList.get(which));
            regionCitySelected.setText("");
//            fabSelectConfig.setBackgroundTintList(getResources().getColorStateList(R.color.color_primary));
//            fabSelectConfig.setEnabled(true);
            setupCitiesByState();

        });

        alertEstados.show();

    }

    protected void setupCitiesByState() {

        cityList = new RealmList<>();

        RealmResults<State> state = realm.where(State.class).equalTo("name", regionStateSelected.getText().toString()).findAll();

        cityList = state.get(0).getCities();


        AlertDialog.Builder alertCities = new AlertDialog.Builder(context);
        alertCities.setTitle("Selecione sua Cidade:");

        ArrayList<String> cityStringList = new ArrayList<>();

        for (City city : state.get(0).getCities()) {
            cityStringList.add(city.getName());

        }

        String[] cityArray = new String[cityStringList.size()];
        cityArray = cityStringList.toArray(cityArray);

        alertCities.setItems(cityArray, (dialog, which) -> {
            dialog.dismiss();
            regionCitySelected.setText(cityStringList.get(which));
            City citySelected = realm.where(City.class).equalTo("name", regionCitySelected.getText().toString()).findFirst();
            selectedCityId = citySelected.getExternalIds().getSolrId();

        });

        alertCities.show();

    }

    @Click(R.id.ln_estado)
    protected void stateClicked() {
        if (statesList != null) {
            showStates(statesList);
        } else {
            loadStates();
        }
    }

    @Click(R.id.ln_city)
    protected void cityClicked() {
        if (cityList != null) {
            showCities(cityList);
        }
    }

    protected void showCities(RealmList<City> cities) {

        AlertDialog.Builder alertEstados = new AlertDialog.Builder(context);
        alertEstados.setTitle("Selecione sua Cidade:");

        cityStringList = new ArrayList<>();

        for (City city : cities) {
            cityStringList.add(city.getName());

        }

        String[] stateArray = new String[cities.size()];
        stateArray = cityStringList.toArray(stateArray);

        alertEstados.setItems(stateArray, (dialog, which) -> {
            dialog.dismiss();
            regionCitySelected.setText(cityStringList.get(which));
            City citySelected = realm.where(City.class).equalTo("name", regionCitySelected.getText().toString()).findFirst();
            selectedCityId = citySelected.getExternalIds().getSolrId();

        });

        alertEstados.show();

    }

    @Click(R.id.fab_select_config)
    protected void applyConfigs() {

        if (regionCitySelected.getText().toString().isEmpty() || regionCitySelected.getText().toString() == null) {
            new MaterialDialog.Builder(context)
                    .title("Atenção")
                    .neutralText(R.string.btn_ok)
                    .content("Selecione uma cidade para continuar")
                    .onNeutral((dialog, which) -> {
                        dialog.dismiss();
                    }).show();
        } else {
            if (regionCitySelected.getText().toString().equals(sessionManager.getAppSettings().getSettings().getSelectedCityName())) {

            } else {
                updateRealmSettings();
                sincRealmEpg();
                sincRealmScreen();
            }
            listener.onRegionChange();
        }
    }

    private void sincRealmScreen() {

        realm.executeTransaction(realm1 -> {
            RealmResults<Screen> listOfScreens = realm1.where(Screen.class).findAll();

            for (Screen screen : listOfScreens) {

                screen.getHighlights().deleteAllFromRealm();

                screen.deleteFromRealm();
            }

            listOfScreens.deleteAllFromRealm();


        });
    }

    protected void sincRealmEpg() {
        Timber.d("sincronizando dados %s", selectedCityId);

        realm.executeTransaction(realm1 -> {
            RealmResults<Epg> listOfEpg = realm1.where(Epg.class).findAll();

            for (Epg oldEpg : listOfEpg) {

                for (Channel channel : oldEpg.getChannels()) {

                    channel.getExhibitions().deleteAllFromRealm();
                }

                oldEpg.getChannels().deleteAllFromRealm();
            }

            listOfEpg.deleteAllFromRealm();


        });
    }

    protected void updateRealmSettings() {

        realm.executeTransaction(realm1 -> {
            App app = sessionManager.getAppSettings();
            app.getSettings().setSelectedCityId(selectedCityId);
            app.getSettings().setSelectedCityName(regionCitySelected.getText().toString());
            app.getSettings().setSelectedStateName(regionStateSelected.getText().toString());
            realm1.insertOrUpdate(app);

            appContext.trackManagerEventPush("configuração", "selecionar-cidade",
                    regionCitySelected.getText().toString(), "");

            appContext.trackManagerEventPush("configuração", "selecionar-estado",
                    regionStateSelected.getText().toString(), "");
        });

    }

}
