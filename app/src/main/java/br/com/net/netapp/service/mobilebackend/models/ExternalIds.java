package br.com.net.netapp.service.mobilebackend.models;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import io.realm.RealmObject;
        import io.realm.annotations.PrimaryKey;
        import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 06/09/17.
 */

@RealmClass
public class ExternalIds extends RealmObject {

    @SerializedName("solrId")
    @Expose
    @PrimaryKey
    private Integer solrId;

    public Integer getSolrId() {
        return solrId;
    }

    public void setSolrId(Integer solrId) {
        this.solrId = solrId;
    }

}
