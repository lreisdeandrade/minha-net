package br.com.net.netapp.service.mobilebackend.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by gibranlyra on 06/06/17.
 */

@RealmClass
public class Screen extends RealmObject {

    private String statusCode;
    private RealmList<Highlight> highlights;

    private String id;
    //Todo Change type to date
    private String createdAt;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public RealmList<Highlight> getHighlights() {
        return highlights;
    }

    public void setHighlights(RealmList<Highlight> highlights) {
        this.highlights = highlights;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
