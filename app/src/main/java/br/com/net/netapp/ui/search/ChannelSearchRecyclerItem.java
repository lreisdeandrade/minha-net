package br.com.net.netapp.ui.search;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.squareup.picasso.Picasso;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.ChannelDTO;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 05/06/17.
 */

public class ChannelSearchRecyclerItem extends CustomRecyclerItem<ChannelDTO,
        ChannelSearchRecyclerItem, ChannelSearchRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();
    private Context context;

    public ChannelSearchRecyclerItem(Context context, ChannelDTO item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.channel_search_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.channel_search_recycler_item;
    }

    @Override
    public void bindView(ViewHolder viewHolder) {
        super.bindView(viewHolder);
        ChannelDTO viewModel = getModel();
        viewHolder.channelName.setText(viewModel.getName());

        viewHolder.channelNumber.setText(viewModel.getNumber().toString());

        if (viewModel.getLogoUrl() != null &&
                !viewModel.getLogoUrl().isEmpty()) {

            String imageUrl = viewModel.getLogoUrl();
            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(ContextCompat.getDrawable(context,
                            R.drawable.placeholder))
                    .into(viewHolder.channelImage);
        } else {
            Picasso.with(context)
                    .load(R.drawable.placeholder)
                    .into(viewHolder.channelImage);
        }
    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView channelName;
        TextView channelNumber;
        ImageView channelImage;

        public ViewHolder(View view) {
            super(view);
            this.channelName = (TextView) view.findViewById(R.id.bookmark_channel_name);
            this.channelNumber = (TextView) view.findViewById(R.id.bookmark_channel_number);
            this.channelImage = (ImageView) view.findViewById(R.id.channel_image_view);
        }
    }
}