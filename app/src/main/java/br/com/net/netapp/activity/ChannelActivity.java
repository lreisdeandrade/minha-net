package br.com.net.netapp.activity;


import android.app.Activity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rohit.recycleritemclicksupport.RecyclerItemClickSupport;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.adapter.ChannelDetailCalendarAdapter;
import br.com.net.netapp.adapter.ChannelDetailExhibitionAdapter;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.bookmark.BookmarkClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.channel.ChannelClient;
import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.service.mobilebackend.models.ChannelDetail;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.events.EventContentView;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmQuery;

@EActivity(R.layout.activity_channel)
public class ChannelActivity extends BaseActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    RecyclerView recyclerView;

    @ViewById
    GridView gridView;

    @ViewById
    RelativeLayout loading;

    @ViewById
    TextView textViewErrorMessage, titleTextView, numberTextView;

    @ViewById
    ProgressBar progressComponent, progressBarBookmark;

    @ViewById
    ImageView logoImageView;

    @ViewById
    ImageView img_menu;

    @ViewById
    ImageButton icFavorite;


    @Bean
    ChannelDetailExhibitionAdapter recyclerViewAdapter;

    @Bean
    SessionManager sessionManager;

    ChannelDetailCalendarAdapter gridViewAdapter;

    private RealmObject channelRealm;

    private ChannelClient channelClient;

    private BookmarkClient bookmarkClient;

    ArrayList<String> days;

    ChannelDetail channelDetail;

    @Extra
    int id;

    @AfterViews
    void afterViews() {

        this.setSupportActionBar(toolbar);

        this.channelClient = new ChannelClient();
        this.bookmarkClient = new BookmarkClient();

        this.setupDaysCalendar();

        this.channelRealm = getRealm().where(ChannelDetail.class).equalTo("id", id).findFirst();
        checkChannelDetail();
        showVtIfExists();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void checkChannelDetail() {

        if (channelRealm != null) {
            this.channelDetail = (ChannelDetail) channelRealm;
            addRealmChangeListener();
            setupHeaderView();
            this.configRecyclerViewExhibition();

        } else {

            this.channelClient.loadChannelDetail(id, String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new RealmCallbackClient() {
                @Override
                public void onError(ErrorMessage errorMessage) {
                    //loadingComponent.setVisibility(View.GONE);
                    progressComponent.setVisibility(View.INVISIBLE);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                    textViewErrorMessage.setText(errorMessage.getMessage());
                }

                @Override
                public void onSuccess(RealmObject response) {
                    super.onSuccess(response);

                    ChannelActivity.this.channelDetail = (ChannelDetail) response;
                    setupHeaderView();
                    ChannelActivity.this.configRecyclerViewExhibition();

                }
            });
        }

    }

    @Click(R.id.ic_favorite)
    protected void favoriteClicked() {

        if (!sessionManager.isActive()) {
            ActivityRoutes.getInstance().openLoginActivity(this, Constants.LOGIN_BOOKMARK_RESULT);
        } else {
            if (icFavorite.isSelected()) {
                removeBookmark();
            } else {
                createBookmark();
            }
        }
    }

    @OnActivityResult(Constants.LOGIN_BOOKMARK_RESULT)
    void onResult(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            createBookmark();
        }
    }

    protected void removeBookmark() {
        progressBarBookmark.setVisibility(View.VISIBLE);
        bookmarkClient.deleteBookmark(sessionManager.getUser().getToken(), id, new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                progressBarBookmark.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(boolean success) {
                super.onSuccess(success);
                progressBarBookmark.setVisibility(View.GONE);
                icFavorite.setSelected(false);

                Event event = new EventContentView("canal", "retirar-favorito",
                        ChannelActivity.this.channelDetail.getName(),
                        ChannelActivity.this.channelDetail.getId().toString());
                TrackManager.getInstance().trackEvent(event);
            }
        });
    }

    protected void createBookmark() {
        progressBarBookmark.setVisibility(View.VISIBLE);
        bookmarkClient.createChannelBookmark(sessionManager.getUser().getToken(), id, String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {

                progressBarBookmark.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(RealmObject response) {
                super.onSuccess(response);
                progressBarBookmark.setVisibility(View.GONE);

                icFavorite.setSelected(true);

                if (channelDetail != null) {
                    Event event = new EventContentView("canal", "favorito",
                            ChannelActivity.this.channelDetail.getName(),
                            ChannelActivity.this.channelDetail.getId().toString());

                    TrackManager.getInstance().trackEvent(event);
                }
            }
        });
    }

    protected void configRecyclerViewExhibition() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);

        this.updateRecyclerViewWithDate(new Date());

        recyclerView.setAdapter(recyclerViewAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.scrollToPosition(recyclerViewAdapter.getCurrentItemPosition());

        RecyclerItemClickSupport.addTo(recyclerView).setOnItemClickListener(new RecyclerItemClickSupport.OnItemClickListener() {

            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Exhibition exhibition = recyclerViewAdapter.getItem(position);

                SinopseActivity_.intent(recyclerView.getContext()).idExhibition(exhibition.getId()).start();

            }
        });
    }


    private void addRealmChangeListener() {
        if (this.channelRealm != null) {
            this.channelRealm.addChangeListener(new RealmChangeListener<RealmModel>() {
                @Override
                public void onChange(RealmModel element) {
                    setupHeaderView();

                }
            });
        }
    }

    private void setupDaysCalendar() {

        days = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

        DateTime startDate = DateTime.now().withTimeAtStartOfDay();

        for (int i = 0; i < 7; i++) {

            Duration duration = Days.days(i).toStandardDuration();

            Date date = startDate.withDurationAdded(duration, 1).toDate();

            days.add(dateFormat.format(date));
        }

        gridViewAdapter = new ChannelDetailCalendarAdapter(this);

        gridViewAdapter.setDiasCalendario(days);

        gridView.setAdapter(gridViewAdapter);
    }

    private void updateRecyclerViewWithDate(Date date) {

        DateTime dateTime = new DateTime(date.getTime());

        Date startDate = dateTime.withTimeAtStartOfDay().toDate();

        Date endDate = dateTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();

        RealmList<Exhibition> exhibitions = getRealm().where(ChannelDetail.class).equalTo("id", id).findFirst().getExhibitions();

        if (exhibitions != null) {
            RealmQuery<Exhibition> query = exhibitions.where().between("startDate", startDate, endDate);

            recyclerViewAdapter.init(query);

            recyclerViewAdapter.notifyDataSetChanged();

            loading.setVisibility(View.GONE);
        }
    }

    private void setupHeaderView() {

        loading.setVisibility(View.GONE);

        BookmarkChannel bookmarkChannel = getRealm().where(BookmarkChannel.class).equalTo("id", id).findFirst();
        if (bookmarkChannel != null) {
            icFavorite.setSelected(true);
        } else {
            icFavorite.setSelected(false);
        }

        Picasso.with(this).load(this.channelDetail.getLogoUrl()).into(logoImageView);

        titleTextView.setText(this.channelDetail.getName());

        numberTextView.setText(String.valueOf(this.channelDetail.getNumber()));

        this.configRecyclerViewExhibition();

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (this.channelRealm != null) {
            this.channelRealm.removeAllChangeListeners();
        }
    }

    @ItemClick(R.id.grid_view)
    protected void calendarioClicked(int position) {
        gridViewAdapter.setPositionSelected(position);
        gridViewAdapter.notifyDataSetChanged();
        Date dateSelected = null;
        try {
            dateSelected = new SimpleDateFormat("yyyyMMdd").parse(days.get(position) + " 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateTime dateStartCurrent = new DateTime(dateSelected).withTimeAtStartOfDay();

        loading.setVisibility(View.VISIBLE);
        updateRecyclerViewWithDate(dateStartCurrent.toDate());

        AppContext.getInstance().trackManagerEventPush("grade", "calendario", dateSelected.toString(), "");

    }

    @Click(R.id.img_menu)
    protected void backClicked() {
        onBackPressed();
    }
}