package br.com.net.netapp.activity;

import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Iterator;

import br.com.net.netapp.R;
import br.com.net.netapp.adapter.EpgChannelCategoryAdapter;
import br.com.net.netapp.event.LoadFiltersEvent;
import br.com.net.netapp.event.OnChangesFilterChanged;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.Filter;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.epg.EpgClient;
import br.com.net.netapp.service.mobilebackend.models.Category;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.events.EventContentView;

/**
 * Created by elourenco on 10/05/17.
 */

@EActivity(R.layout.activity_epg_filter_category)
public class EpgFilterCategoryActivity extends BaseActivity {
    private static final String TAG = "EpgFilterCategoryActivity";

    @ViewById
    ListView listViewFilterCategory;

    @ViewById
    Button buttonCancelar, buttonSalvar;

    @ViewById
    AppCompatButton clearFilters;

    @Bean
    EpgChannelCategoryAdapter adapter;

    @ViewById
    RelativeLayout loading;

    @Bean
    SessionManager sessionManager;

    boolean hascleared = false;


    @AfterViews
    void setupView() {

        buttonSalvar.setAlpha((float) 0.4);
        clearFilters.setAlpha((float) 0.4);

        listViewFilterCategory.setAdapter(adapter);

        EpgClient epgClient = new EpgClient();
        epgClient.loadEpgChannelCategories(String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
            }
        });

        if (adapter.getOriginFilter().containsValue(true)) {
            clearFilters.setEnabled(true);
            clearFilters.setAlpha((float) 1.0);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void finishLoadCategories(LoadFiltersEvent loadFiltersEvent) {
        loading.animate().alpha(0.0f);
        loading.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Click(R.id.buttonSalvar)
    void onClickSave() {

        if (adapter.getChangeFilter().size() > 0) {
            for (Integer id : adapter.getChangeFilter().keySet()) {
                Category category = getRealm().where(Category.class).equalTo("id", id).findFirst();
                if (adapter.getChangeFilter().get(id)) {
                    Filter.addCategory(category);
                    Event event = new EventContentView("grade", "incluir-filtro", category.getName(), null);
                    TrackManager.getInstance().trackEvent(event);
                } else {
                    if (Filter.has(category))
                        Filter.removeCategory(category);
                    Event event = new EventContentView("grade", "remover-filtro", category.getName(), null);
                    TrackManager.getInstance().trackEvent(event);

                }
            }
        }

        setResult(RESULT_OK);
        finish();
    }

    @Click(R.id.buttonCancelar)
    protected void onClickCancelar() {
        super.onBackPressed();
    }

    @Click
    protected void clearFilters() {

        Iterator<Integer> iter = adapter.getChangeFilter().keySet().iterator();

        while (iter.hasNext()) {
            Integer changeFilter = iter.next();

            if (adapter.getOriginFilter() != null && !adapter.getChangeFilter().get(changeFilter)) {

                if (adapter.getChangeFilter().containsKey(changeFilter)) {
                    adapter.getChangeFilter().put(changeFilter, false);
//                    adapter.getChangeFilter().remove(changeFilter);
                }

            } else {

                adapter.getChangeFilter().put(changeFilter, false);
            }
        }

        for (Category c : Filter.categoriesList()) {
            if (adapter.getChangeFilter().containsKey(c.getId())) {
                adapter.getChangeFilter().put(c.getId(), !adapter.getOriginFilter().get(c.getId()));
                hascleared = true;
            }
        }

        clearFilters.setEnabled(false);
        clearFilters.setAlpha((float) (clearFilters.isEnabled() ? 1.0 : 0.4));

        buttonSalvar.setEnabled(hascleared);
        buttonSalvar.setAlpha((float) (buttonSalvar.isEnabled() ? 1.0 : 0.4));
        adapter.notifyDataSetChanged();

    }


    @Subscribe
    public void onChangesFilterChanged(OnChangesFilterChanged event) {
        clearFilters.setEnabled(isClearEnable(event));
        clearFilters.setAlpha((float) (clearFilters.isEnabled() ? 1.0 : 0.4));

        buttonSalvar.setEnabled(!adapter.getChangeFilter().isEmpty());
        buttonSalvar.setAlpha((float) (buttonSalvar.isEnabled() ? 1.0 : 0.4));
    }

    private boolean isClearEnable(OnChangesFilterChanged event) {
        return event.getCount() > 0 || adapter.getOriginFilter().size() > 1;
    }
}
