package br.com.net.netapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.net.netapp.R;

/**
 * Created by thiagomagalhaes on 19/05/17.
 */

public class ChannelDetailCalendarAdapter extends CalendarAdapter {


    public ChannelDetailCalendarAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = super.getView(position, convertView, parent);

        TextView weekDay = (TextView) v.findViewById(R.id.day_name);

        TextView day = (TextView) v.findViewById(R.id.day_number);

        int primaryColor = ContextCompat.getColor(context, R.color.color_primary);

        if (position == positionSelected) {

            weekDay.setTextColor(primaryColor);

            day.setTextColor(primaryColor);

        } else {

            weekDay.setTextColor(Color.BLACK);

            day.setTextColor(Color.BLACK);
        }

        return v;
    }
}
