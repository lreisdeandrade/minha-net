package br.com.net.netapp.service.mobilebackend.models;

import java.util.ArrayList;

/**
 * Created by Leandro on 05/06/17.
 */

public class Data {

    private ArrayList<ChannelDTO> channels;
    private ArrayList<Exhibition> exhibitions;

    public ArrayList<ChannelDTO> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<ChannelDTO> channels) {
        this.channels = channels;
    }

    public ArrayList<Exhibition> getExhibitions() {
        return exhibitions;
    }

    public void setExhibitions(ArrayList<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    @Override
    public String toString() {
        return "Data{" +
                "channels=" + channels +
                ", exhibitions=" + exhibitions +
                '}';
    }
}
