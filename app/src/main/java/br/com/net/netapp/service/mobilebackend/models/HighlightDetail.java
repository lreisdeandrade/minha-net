package br.com.net.netapp.service.mobilebackend.models;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 08/06/17.
 */

@RealmClass
public class HighlightDetail extends RealmObject{

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EE, dd 'de' MMMM à's' HH:mm");


    private Date endDate;
    private TvShow tvShow;
    private Channel channel;
    private long id;
    private String title;
    private Date startDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public TvShow getTvShow() {
        return tvShow;
    }

    public void setTvShow(TvShow tvShow) {
        this.tvShow = tvShow;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getFormatedTvShowDate() {
        return DATE_FORMAT.format(this.startDate);
    }
    @Override
    public String toString() {
        return "HighlightDetail{" +
                "endDate=" + endDate +
                ", tvShow=" + tvShow +
                ", channel=" + channel +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", startDate=" + startDate +
                '}';
    }
}
