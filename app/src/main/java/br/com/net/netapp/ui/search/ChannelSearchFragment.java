package br.com.net.netapp.ui.search;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.activity.ChannelActivity_;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.service.mobilebackend.models.ChannelDTO;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;

/**
 * Created by Leandro on 05/06/17.
 */

@EFragment(R.layout.fragment_search)
public class ChannelSearchFragment extends Fragment implements
        CustomRecyclerListener<ChannelSearchRecyclerItem> {

    @FragmentArg("channels")
    ArrayList<ChannelDTO> channels;

    @ViewById
    FrameLayout fragmentContainerLayout;

    private CustomRecycler<ChannelSearchRecyclerItem> recycler;
    CustomRecyclerListener listener;
    private ArrayList<ChannelSearchRecyclerItem> recyclerItems;

    @AfterViews
    protected void afterViews(){
        recycler = new CustomRecycler<>(getContext(), this);
        fragmentContainerLayout.addView(recycler);
        if (getActivity() instanceof CustomRecyclerListener) {
            this.listener = ((CustomRecyclerListener) getActivity());
            add(channels);
            recycler.setViewLoaded(true);
        } else {
            throw new RuntimeException("Must implement CustomRecyclerListener");
        }
    }

    public void add(List<ChannelDTO> items) {
        recyclerItems = new ArrayList<>();
        for (ChannelDTO channel : items) {
            recyclerItems.add(new ChannelSearchRecyclerItem(getContext(), channel));
        }
        recycler.add(recyclerItems);
    }

    public void removeAll(){
        if(recycler != null)
        recycler.removeAll();
    }

    @Override
    public void onItemClick(View view, ChannelSearchRecyclerItem item, int position) {
        AppContext.getInstance().trackManagerEventPush("busca", "canal", item.getModel().getName(), null);
        listener.onItemClick(view, item, position);
        ChannelActivity_.intent(this).id(item.getModel().getId()).start();
    }

    @Override
    public void onLongItemClick(View view, ChannelSearchRecyclerItem item, int position) {

    }

    public CustomRecycler<ChannelSearchRecyclerItem> getRecycler() {
        return recycler;
    }
}
