package br.com.net.netapp.helper;

import android.location.Location;

/**
 * Created by thiagomagalhaes on 26/01/17.
 */

public interface LocationProviderListener {

    void onSuccess(Location location);

    void onFailure();
}