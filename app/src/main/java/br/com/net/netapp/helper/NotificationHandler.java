package br.com.net.netapp.helper;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import org.androidannotations.annotations.EBean;

import br.com.net.netapp.R;
import br.com.net.netapp.activity.ChannelActivity_;
import br.com.net.netapp.activity.MainActivity_;
import br.com.net.netapp.activity.SinopseActivity_;

/**
 * Created by Leandro on 22/05/17.
 */

@EBean
public class NotificationHandler {

    public void configDeepLinkFlow(Context context, Intent intent) {
        Uri uri = intent.getData();
        if (uri.getHost().equals("exhibition")) {

            String idExhibition = uri.getLastPathSegment() != null ?
                    uri.getLastPathSegment().trim() : "";

            if (idExhibition.matches("[0-9]+")) {
                SinopseActivity_.intent(context).idExhibition(idExhibition).start();
            }
        } else if (uri.getHost().equals("channel")) {

            String idChannel = uri.getLastPathSegment() != null ?
                    uri.getLastPathSegment().trim() : "";

            if (idChannel.matches("[0-9]+")) {
                ChannelActivity_.intent(context).id(Integer.valueOf(idChannel)).start();
            }
        }
    }

    public void configPushNotification(Context context, String title, String message, Uri deeplink) {

        String host = deeplink.getHost() != null ? deeplink.getHost() : "";
        PendingIntent contentIntent;
        switch (host) {
            case "exhibition":

                String idExhibition = deeplink.getLastPathSegment() != null ?
                        deeplink.getLastPathSegment().trim() : "";

                if (idExhibition.matches("[0-9]+")) {

                    Intent notificationSynopsisIntent = new Intent(context, SinopseActivity_.class);

                    notificationSynopsisIntent.putExtra("idExhibition", Long.valueOf(idExhibition));

                    notificationSynopsisIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    contentIntent = PendingIntent.getActivity(context, 0, notificationSynopsisIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                } else {
                    Intent notificationIntent = new Intent(context, MainActivity_.class);

                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                }

                buildNotification(context, title, message, contentIntent);

                break;

            case "channel":

                String idChannel = deeplink.getLastPathSegment() != null ?
                        deeplink.getLastPathSegment().trim() : "";

                if (idChannel.matches("[0-9]+")) {
                    Intent notificationSynopsisIntent = new Intent(context, ChannelActivity_.class);

                    notificationSynopsisIntent.putExtra("id", Integer.valueOf(idChannel));

                    notificationSynopsisIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    contentIntent = PendingIntent.getActivity(context, 0, notificationSynopsisIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                } else {
                    Intent notificationIntent = new Intent(context, MainActivity_.class);

                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                }

                buildNotification(context, title, message, contentIntent);

                break;

            default:

                Intent notificationIntent = new Intent(context, MainActivity_.class);

                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                buildNotification(context, title, message, contentIntent);
        }

    }


    public void buildNotification(Context context, String title, String message, PendingIntent contentIntent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(
                R.drawable.ic_trail_notification)
                .setContentTitle(title)
//                .setColor(ContextCompat.getColor(context, R.color.color_primary))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(contentIntent);

        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, builder.build());
    }
}
