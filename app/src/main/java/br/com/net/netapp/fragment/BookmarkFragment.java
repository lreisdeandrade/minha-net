package br.com.net.netapp.fragment;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.bookmark.BookmarkClient;
import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.ui.bookmark.BookmarkRecyclerItem;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.events.EventContentView;
import io.realm.RealmList;
import timber.log.Timber;

/**
 * Created by Leandro on 26/06/17.
 */

@EFragment(R.layout.fragment_bookmark)
public class BookmarkFragment extends BaseFragment implements
        CustomRecyclerListener<BookmarkRecyclerItem> {

    private static final String TAG = "BookmarkFragment";
    @Bean
    SessionManager sessionManager;

    @ViewById
    LinearLayout containerFavoriteLogin;

    BookmarkClient bookmarkClient;

    CustomRecyclerListener listener;

    private CustomRecycler<BookmarkRecyclerItem> recycler;

    private ArrayList<BookmarkRecyclerItem> recyclerItems;

    List<BookmarkChannel> bookmarks;

    @ViewById
    FrameLayout fragmentContainerLayout;

    @AfterViews
    protected void afterViews() {

        bookmarkClient = new BookmarkClient();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getActivity() instanceof CustomRecyclerListener) {
            this.listener = ((CustomRecyclerListener) getActivity());

        } else {
            throw new RuntimeException("You must implements CustomRecyclerListener");
        }

        setupScreen();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        isLogged(sessionManager.isActive());

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLogout(UserSignoutEvent event) {
        Log.d(TAG, "userlogout");
        isLogged(sessionManager.isActive());

    }

    protected void isLogged(Boolean isActive) {
        if (isActive) {
            loadBookmarks();
            fragmentContainerLayout.setVisibility(View.VISIBLE);
            containerFavoriteLogin.setVisibility(View.GONE);

        } else {
            containerFavoriteLogin.setVisibility(View.VISIBLE);
            fragmentContainerLayout.setVisibility(View.GONE);

        }
    }

    protected void loadBookmarks() {

        bookmarkClient.loadBookmarks(sessionManager.getUser().getToken(), String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {

            }

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);
                bookmarks = (List<BookmarkChannel>) response;

                Timber.d("onSuccess: %s", bookmarks.toString());
                add(bookmarks);
            }

        });
    }

    public void add(List<BookmarkChannel> bookmarks) {
        recyclerItems = new ArrayList<>();

        for (BookmarkChannel bookmarkChannel : bookmarks) {
            recyclerItems.add(new BookmarkRecyclerItem(getContext(), bookmarkChannel));
        }

        if (recycler != null) {
            recycler.add(recyclerItems);

        } else {
            recycler = new CustomRecycler<>(getContext(), this);
            recycler.add(recyclerItems);

        }
    }


    protected void setupScreen() {
        recycler = new CustomRecycler<>(getContext(), this);
        fragmentContainerLayout.addView(recycler);
        recycler.setViewLoaded(true);
        recycler.setNoContentText(getResources().getString(R.string.empty_bookmark));
    }

    @Override
    public void onItemClick(View view, BookmarkRecyclerItem item, int position) {

        ActivityRoutes.getInstance().openChannelActivity(getContext(), item.getModel().getId());
    }

    @Override
    public void onLongItemClick(View view, BookmarkRecyclerItem item, int position) {
        new MaterialDialog.Builder(getActivity())
                .title("Favoritos")
                .positiveText(R.string.sim)
                .negativeText(R.string.nao)
                .content(R.string.remove_bookmark).onPositive((dialog, which) -> {
            try {

                bookmarkClient.deleteBookmark(sessionManager.getUser().getToken(), item.getModel().getId(), new RealmCallbackClient() {
                    @Override
                    public void onError(ErrorMessage errorMessage) {

                    }

                    @Override
                    public void onSuccess(boolean success) {
                        super.onSuccess(success);
                        deleteFromRealm(item.getModel());

                    }
                });
                recycler.remove(position);

            } catch (Exception e) {

                Timber.e(e, "onLongItemClick: %s", e.getMessage());
                Toast.makeText(getContext(), "Não foi possível remover o canal", Toast.LENGTH_SHORT).show();
            }
        }).onNegative((dialog, which) -> {
            dialog.dismiss();
        })

                .show();
    }

    private void deleteFromRealm(BookmarkChannel bookmark) {

        getRealm().executeTransaction(realm1 -> {
            BookmarkChannel bookmarkChannel = realm1.where(BookmarkChannel.class).equalTo("id", bookmark.getId()).findFirst();
            bookmarkChannel.deleteFromRealm();
        });

        Event event = new EventContentView("canal", "retirar-favorito",
                bookmark.getName(),
                String.valueOf(bookmark.getId()));

        TrackManager.getInstance().trackEvent(event);


    }
}
