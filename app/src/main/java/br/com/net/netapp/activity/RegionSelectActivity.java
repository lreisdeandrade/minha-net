package br.com.net.netapp.activity;

import android.Manifest;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.IOException;
import java.util.Locale;

import br.com.net.netapp.R;
import br.com.net.netapp.helper.LocationProvider;
import br.com.net.netapp.helper.LocationProviderListener;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.pref.NetAppPrefs_;
import br.com.net.netapp.service.mobilebackend.clients.app.AppClient;
import br.com.net.netapp.ui.region.RegionListener;
import br.com.net.netapp.ui.region.RegionView;
import br.com.net.netapp.ui.region.RegionView_;
import timber.log.Timber;

/**
 * Created by Leandro on 05/09/17.
 */

@EActivity(R.layout.activity_region_select)
public class RegionSelectActivity extends BaseActivity implements RegionListener {

    private static final String TAG = "RegionSelectActivity";
    @ViewById
    LinearLayout container;

    @ViewById
    public Toolbar toolbar;

    @Pref
    NetAppPrefs_ netAppPrefs;

    @Extra
    Boolean isFromSplash;

    @ViewById
    RelativeLayout loading;

    @Bean
    SessionManager sessionManager;

    private AppClient appClient;
    private LocationProvider locationProvider;


    @AfterViews
    protected void setupView() {
        locationProvider = new LocationProvider(RegionSelectActivity.this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Selecione sua cidade");
        }

        appClient = new AppClient();

        checkConnection();
    }

    protected void checkConnection() {
        if (Connectivity.create(this).isAvailable()) {
            initApp();
        } else {
            new MaterialDialog.Builder(this)
                    .title("Atenção")
                    .positiveText("sim")
                    .content("Você está sem internet. Tentar novamente ?")
                    .onPositive((dialog, which) -> {
                        checkConnection();
                    })
                    .onNeutral((dialog, which) -> {
                        dialog.dismiss();
                    }).show();
        }
    }

    protected void initApp() {

        checkLocationPermission();

    }

    private void checkLocationPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {


                        String provider = Settings.Secure.getString(getContentResolver(),
                                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                        if (!provider.equals("")) {
                            //GPS Enabled
                            getUserLocation();
                        } else {
                            new MaterialDialog.Builder(RegionSelectActivity.this)
                                    .title("Atenção")
                                    .positiveText(R.string.btn_ok)
                                    .negativeText("não")
                                    .content("Deseja habilitar o GPS para capturar sua localização automática ? ")
                                    .onNegative((dialog, which) -> {
                                        RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), null, null, RegionSelectActivity.this);
                                        container.addView(contractView);
                                        dialog.dismiss();

                                    })
                                    .onPositive((dialog, which) -> {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    }).show();
                        }
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), null, null, RegionSelectActivity.this);
                        container.addView(contractView);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();

                    }
                }).check();
    }


    @Override
    public void onRestart() {
        super.onRestart();

        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.equals("")) {
            //GPS Enabled
            if (sessionManager.getAppSettings() != null) {
                getUserLocation();

            }
        }
    }

    private void getUserLocation() {

        loading.setVisibility(View.VISIBLE);
        locationProvider.getLocation(new LocationProviderListener() {
            @Override
            public void onSuccess(Location location) {
                Geocoder gcd = new Geocoder(RegionSelectActivity.this, Locale.getDefault());

                loading.setVisibility(View.INVISIBLE);

                if (location != null) {
                    try {

                        Address address = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0);

                        RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), address.getAdminArea(), address.getSubAdminArea(), RegionSelectActivity.this);
                        container.addView(contractView);

                    } catch (IOException e) {
                        e.printStackTrace();
                        RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), null, null, RegionSelectActivity.this);
                        container.addView(contractView);
                    }
                } else {
                    RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), null, null, RegionSelectActivity.this);
                    container.addView(contractView);
                }
            }

            @Override
            public void onFailure() {
                loading.setVisibility(View.INVISIBLE);

                RegionView contractView = RegionView_.build(RegionSelectActivity.this, getRealm(), null, null, RegionSelectActivity.this);
                container.addView(contractView);
                Timber.d("Erro");
            }
        });
    }

    @Override
    public void onRegionChange() {
        if (isFromSplash) {
            MainActivity_.intent(this).start();
            finish();
        } else {
            finish();
        }
    }
}

