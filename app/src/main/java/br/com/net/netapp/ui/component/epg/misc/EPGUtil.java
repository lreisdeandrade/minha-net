package br.com.net.netapp.ui.component.epg.misc;

import android.content.Context;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

/**
 * Created by Leandro on 04/05/17.
 */

public class EPGUtil {
    private static final String TAG = "EPGUtil";
    private static final DateTimeFormatter dtfShortTime = DateTimeFormat.forPattern("HH:mm");

    public static String getShortTime(long timeMillis) {
        return dtfShortTime.print(timeMillis);
    }

    public static String getWeekdayName(long dateMillis) {
        final Locale myLocale = new Locale("pt", "BR");
        LocalDate date = new LocalDate(dateMillis);
        return date.dayOfWeek().getAsShortText(myLocale);
    }

    public static String getDayInMonth(long dateMillis) {
        LocalDate date = new LocalDate(dateMillis);
        return date.dayOfMonth().getAsShortText();
    }

    public static void loadImageInto(Context context, String url, int width, int height, Target target) {
        Picasso.with(context).load(url) .resize(600,300)
                .centerInside()
                .onlyScaleDown()
                .into(target);

    }
}
