package br.com.net.netapp.ui.search;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.squareup.picasso.Picasso;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 05/06/17.
 */

public class ExhibitionSearchRecyclerItem extends CustomRecyclerItem<Exhibition,
        ExhibitionSearchRecyclerItem, ExhibitionSearchRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();
    private Context context;

    public ExhibitionSearchRecyclerItem(Context context, Exhibition item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.exhibition_search_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.exhibition_search_recycler_item;
    }

    @Override
    public void bindView(ViewHolder viewHolder) {
        super.bindView(viewHolder);
        Exhibition viewModel = getModel();
        viewHolder.exhibitionTitle.setText(viewModel.getTitle());

        if (viewModel.getStartDate() != null) {
            viewHolder.exhibitionDateStart.setText(viewModel.getFormatedDayMonthAndHour());

        }
        if (viewModel.getTvShow().getPosterUrl() != null && !viewModel.getTvShow().getPosterUrl().isEmpty()) {

            String imageUrl = viewModel.getTvShow().getPosterUrl();
            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(ContextCompat.getDrawable(context,
                            R.drawable.placeholder))
                    .into(viewHolder.exhibitionImage);
        } else {
            Picasso.with(context)
                    .load(R.drawable.placeholder)
                    .into(viewHolder.exhibitionImage);
        }
    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView exhibitionTitle;
        TextView exhibitionDateStart;
        ImageView exhibitionImage;

        public ViewHolder(View view) {
            super(view);
            this.exhibitionTitle = (TextView) view.findViewById(R.id.bookmark_exhibition_title);
            this.exhibitionDateStart = (TextView) view.findViewById(R.id.exhibition_date_start);
            this.exhibitionImage = (ImageView) view.findViewById(R.id.exhibition_image_view);
        }
    }
}