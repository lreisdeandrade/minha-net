package br.com.net.netapp.service.mobilebackend.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;

/**
 * Created by elourenco on 20/04/17.
 */

public class ScreenItem implements AsymmetricItem {
    private int columnSpan;
    private int rowSpan;
    private int position;
    private Highlight highlight;

    public Highlight getHighlight() {
        return highlight;
    }

    public void setHighlight(Highlight highlight) {
        this.highlight = highlight;
    }

    public ScreenItem(int columnSpan, int rowSpan, Highlight destaque) {
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
        this.position = destaque.getOrder();
        this.highlight = destaque;
    }

    public ScreenItem(Parcel in) {
        readFromParcel(in);
    }

    @Override public int getColumnSpan() {
        return columnSpan;
    }

    @Override public int getRowSpan() {
        return rowSpan;
    }

    public int getPosition() {
        return position;
    }

    @Override public String toString() {
        return String.format("%s: %sx%s", position, rowSpan, columnSpan);
    }

    @Override public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
    }

    @Override public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(columnSpan);
        dest.writeInt(rowSpan);
        dest.writeInt(position);
    }

    /* Parcelable interface implementation */
    public static final Parcelable.Creator<ScreenItem> CREATOR = new Parcelable.Creator<ScreenItem>() {
        @Override public ScreenItem createFromParcel(@NonNull Parcel in) {
            return new ScreenItem(in);
        }

        @Override @NonNull public ScreenItem[] newArray(int size) {
            return new ScreenItem[size];
        }
    };
}