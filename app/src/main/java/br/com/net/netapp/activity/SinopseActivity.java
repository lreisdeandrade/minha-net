package br.com.net.netapp.activity;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.reminder.ReminderClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.sinopse.SinopseClient;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.Decoder;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.ui.AlarmHelper;
import br.com.net.netapp.ui.NotificationHelper;
import io.realm.RealmObject;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by Leandro on 17/05/17.
 */

@EActivity(R.layout.activity_sinopse)
public class SinopseActivity extends BaseActivity {

    private static final String TAG = "SinopseActivity";
    @Extra
    String idExhibition;

    @ViewById
    RelativeLayout loading;

    @ViewById
    ProgressBar progressBarReminder, progressComponent;

    @ViewById
    ImageView icBack, exhibitionLogoUrl, channelImage;

    @ViewById
    ImageButton icReminder;

    @ViewById
    View gradientView;

    @App
    AppContext appContext;

    @ViewById
    TextView title, genre, duration, ageRating, channelName, channelNumber, exhibitionDetail,
            exhibitionDirector, exhibitionCast, numberDay, dateAndHour, monthAndYear, textViewErrorMessage;

    @ViewById
    LinearLayout containerTitleExhibition, containerSubInfo, containerDateInfo, containerAgeRating;

    @Bean
    SessionManager sessionManager;

    private RealmObject exhibitionRealm;

    protected SinopseClient sinopseClient;
    protected ReminderClient reminderClient;

    private Exhibition exhibition;

    @AfterViews
    protected void afterViews() {

        this.sinopseClient = new SinopseClient();
        this.reminderClient = new ReminderClient();
        super.initCoordinator();
        icBack.bringToFront();

        this.exhibitionRealm = getRealm().where(Exhibition.class).equalTo("id", idExhibition).findFirst();
        checkSynopsis();

        showVtIfExists();
    }

    protected void checkSynopsis() {

        if (this.exhibitionRealm != null) {
            exhibition = (Exhibition) exhibitionRealm;

            if (exhibition.getTvShow() != null && exhibition.getChannelId() != null) {
                setupExhibition(exhibition);
            } else {
                this.sinopseClient.loadSinopse(idExhibition, new RealmCallbackClient() {
                    @Override
                    public void onSuccess(RealmObject response) {
                        super.onSuccess(response);
                        SinopseActivity.this.exhibitionRealm = response;
                        checkSynopsis();
                    }

                    @Override
                    public void onError(ErrorMessage errorMessage) {
                        Timber.d(errorMessage.toString());
                        progressComponent.setVisibility(View.INVISIBLE);
                        textViewErrorMessage.setVisibility(View.VISIBLE);
                        textViewErrorMessage.setText(errorMessage.getMessage());

                    }
                });
            }

        } else {
            this.sinopseClient.loadSinopse(idExhibition, new RealmCallbackClient() {
                @Override
                public void onSuccess(RealmObject response) {
                    super.onSuccess(response);
                    SinopseActivity.this.exhibitionRealm = response;
                    checkSynopsis();

                }

                @Override
                public void onError(ErrorMessage errorMessage) {
                    Timber.d(errorMessage.toString());
                    progressComponent.setVisibility(View.INVISIBLE);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                    textViewErrorMessage.setText(errorMessage.getMessage());

                }
            });
        }
    }

    @Click(R.id.channel_image)
    protected void channelImageClicked() {

        Exhibition exhibition = (Exhibition) this.exhibitionRealm;

        ChannelActivity_.intent(this).id(exhibition.getChannelId()).start();

    }

    @Click(R.id.ic_reminder)
    protected void reminderClicked() {
        if (exhibition.canScheduleLocalNotification()) {
            if (!sessionManager.isActive()) {

                ActivityRoutes.getInstance().openLoginActivity(this, Constants.LOGIN_REMINDER_RESULT);

            } else {
                if (icReminder.isSelected()) {
                    removeReminder();
                } else {
                    createReminder();
                }
            }
        } else {
            new MaterialDialog.Builder(this)
                    .title("Atenção")
                    .neutralText(R.string.btn_ok)
                    .content(R.string.synopsis_reminder_alert).onPositive((dialog, which) -> {
                dialog.dismiss();
            }).show();
        }
    }

    @Click(R.id.ic_rec)
    protected void recClicked() {
        if (Connectivity.create(this).isAvailable()) {
            if (sessionManager.isActive()) {
                if (exhibition.canRecordExhibition()) {
//                    ActivityRoutes.getInstance().openDecodersActivity(this, Constants.RECORD_RESULT);
                    onResultRecord(Activity.RESULT_OK, null);
                } else {
                    showRecordDialogPassedExhibition();
                }
            } else {
                ActivityRoutes.getInstance().openLoginActivity(this, Constants.LOGIN_RECORD_RESULT);
            }
        } else {
            new MaterialDialog.Builder(this)
                    .title("Atenção")
                    .content(R.string.no_connection)
                    .neutralText(R.string.btn_ok)
                    .onNeutral((dialog, which) -> {
                        dialog.dismiss();
                    }).show();
        }
    }

    @OnActivityResult(Constants.LOGIN_REMINDER_RESULT)
    void onResultReminder(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            createReminder();
        } else {
            Toast.makeText(this, "Login não realizado", Toast.LENGTH_SHORT).show();
        }
    }

    @OnActivityResult(Constants.LOGIN_RECORD_RESULT)
    void onResultLoginRecord(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            if (exhibition.canRecordExhibition()) {
                onResultRecord(RESULT_OK, null);
            } else {
                showRecordDialogPassedExhibition();
            }
        }else {
            Toast.makeText(this, "Login não realizado", Toast.LENGTH_SHORT).show();
        }
    }

    protected void showRecordDialogPassedExhibition() {
        new MaterialDialog.Builder(SinopseActivity.this)
                .customView(R.layout.dialog_record_exhibition_passed, true)
                .positiveText(R.string.btn_ok)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                }).show();
    }

    @OnActivityResult(Constants.RECORD_RESULT)
    void onResultRecord(int resultCode, @OnActivityResult.Extra(value = "decoderCode") String decoderCode) {

        if (resultCode == Activity.RESULT_OK) {
            progressBarReminder.setVisibility(View.VISIBLE);

            Contract contractDefault = sessionManager.getContractSelected(sessionManager.getUser().getContractSelectedCode());

            if (contractDefault == null) {
                ActivityRoutes.getInstance().openContractsActivity(this, Constants.RECORD_RESULT);
            } else {
                List<Decoder> decodersHDMax = contractDefault.getDecoders().where().equalTo("isHDMax", true).findAll();

                RealmResults<Contract> contracts = getRealm().where(Contract.class).findAll();

                if (contracts != null && decodersHDMax.size() == 1 && contracts.size() == 1) {

                    sinopseClient.recordExhibition(sessionManager.getUser().getToken(), exhibition.getId(), decoderCode == null ? decodersHDMax.get(0).getCode() : decoderCode, sessionManager.getUser().getContractSelectedCode(), sessionManager.getContractSelected(sessionManager.getUser().getContractSelectedCode()).getOperationCode(), new CallBackClient<ResponseData>() {
                        @Override
                        public void onSuccess(ResponseData result) {
                            progressBarReminder.setVisibility(View.INVISIBLE);

                            new MaterialDialog.Builder(SinopseActivity.this)
                                    .customView(R.layout.dialog_record_sucess, true)
                                    .positiveText(R.string.btn_ok)
                                    .onPositive((dialog, which) -> {
                                        dialog.dismiss();
                                    }).show();

                        }

                        @Override
                        public void onFailure(Throwable t) {
                            progressBarReminder.setVisibility(View.INVISIBLE);
                            appContext.trackManagerEventPush("sinopse", "gravar-erro", exhibition.getTitle(), "");

                            MaterialDialog.Builder dialog2 = new MaterialDialog.Builder(SinopseActivity.this)
                                    .customView(R.layout.dialog_record_error, true)
                                    .positiveText(R.string.btn_ok)
                                    .onPositive((dialog, which) -> {
                                        dialog.dismiss();
                                    });

                            View view = dialog2.build().getCustomView();

                            TextView textViewError = (TextView) view.findViewById(R.id.textView2);

                            if (t.getMessage().equals("Essa gravação já está agendada")) {
                                String message = "Essa gravação já está agendada";
                                textViewError.setText(message);
                            }

                            dialog2.show();
                        }
                    });
                } else if (decodersHDMax.size() > 1) {
                    ActivityRoutes.getInstance().openDecodersActivity(this, Constants.RECORD_RESULT);
                } else if (decodersHDMax.isEmpty()) {
                    progressBarReminder.setVisibility(View.INVISIBLE);
                    new MaterialDialog.Builder(this)
                            .title("Atenção")
                            .content(R.string.dialog_record_error_notHDMAX_message)
                            .neutralText(R.string.btn_ok)
                            .onNeutral((dialog, which) -> {
                                dialog.dismiss();
                            }).show();
                }
            }
        } else {
            progressBarReminder.setVisibility(View.GONE);
        }
    }

    private void removeReminder() {
        progressBarReminder.setVisibility(View.VISIBLE);

        Reminder reminder = getRealm().where(Reminder.class).equalTo("exhibition.id", exhibition.getId()).findFirst();

        this.reminderClient.deleteReminder(sessionManager.getUser().getToken(), reminder.getId(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                Timber.e(String.valueOf(errorMessage), "onError: %s", errorMessage.getMessage());
            }

            @Override
            public void onSuccess(boolean success) {
                super.onSuccess(success);
                icReminder.setSelected(false);
                deleteReminderFromRealm();
                AlarmHelper.cancelNotification(getApplicationContext(), Long.valueOf(exhibition.getId()).intValue());

                progressBarReminder.setVisibility(View.GONE);

            }
        });
    }

    private void deleteReminderFromRealm() {
        super.getRealm().executeTransaction(realm1 -> {
            RealmObject reminder = realm1.where(Reminder.class).equalTo("exhibition.id", exhibition.getId()).findFirst();
            reminder.deleteFromRealm();

        });

    }

    private void createReminder() {
        progressBarReminder.setVisibility(View.VISIBLE);
        this.reminderClient.createReminder(sessionManager.getUser().getToken(), exhibition.getId(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {

            }

            @Override
            public void onSuccess(RealmObject response) {
                super.onSuccess(response);
                icReminder.setSelected(true);

                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(new Date());
                calendar.setTime(exhibition.getStartDate());
                calendar.add(Calendar.MINUTE, -15);

                scheduleNotification(calendar.getTimeInMillis(), Long.valueOf(exhibition.getId()).intValue());
                progressBarReminder.setVisibility(View.INVISIBLE);

            }
        });
    }

    private void scheduleNotification(long delay, int id) {

        String text = getString(R.string.reminder_text, exhibition.getTitle());


        NotificationHelper notificationHelper = NotificationHelper.getInstance();
        Intent sinopseIntent = SinopseActivity_.intent(this).idExhibition(exhibition.getId()).get();

        Notification notification = notificationHelper.sendExpandLayoutNotification(
                sinopseIntent, this.getString(R.string.reminder_intro), text);
        AlarmHelper.scheduleNotification(this, notification, id, delay);
    }

    private void setupExhibition(Exhibition exhibition) {
        gradientView.bringToFront();
        icBack.bringToFront();
        containerTitleExhibition.bringToFront();
        containerSubInfo.bringToFront();
        containerDateInfo.bringToFront();

        Picasso.with(this).load(exhibition.getTvShow().getPosterUrl()).into(exhibitionLogoUrl, new Callback() {
            @Override
            public void onSuccess() {
                loading.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onError() {
                loading.setVisibility(View.INVISIBLE);
                exhibitionLogoUrl.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_primary));

            }
        });

        Reminder reminder = getRealm().where(Reminder.class).equalTo("exhibition.id", exhibition.getId()).findFirst();
        if (reminder != null) {
            icReminder.setSelected(true);
        } else {
            icReminder.setSelected(false);
        }

        title.setText(exhibition.getTitle());
        genre.setText(exhibition.getTvShow().getGenres());
        duration.setText(exhibition.getTvShow().getDuration() + " minutos");
        configAgeRating(exhibition);

        Picasso.with(this).load(exhibition.getChannelLogoUrl()).resize(300, 300)
                .centerInside()
                .onlyScaleDown()
                .into(channelImage);

        monthAndYear.setText(exhibition.getFormatedMonthAndYear());
        numberDay.setText(exhibition.getFormatedDayOfMonth());


        Calendar c = Calendar.getInstance();
        c.setTime(exhibition.getStartDate());

        int day_of_week = c.get(Calendar.DAY_OF_WEEK);

        String weekDays[] = {"Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"};

        String day = weekDays[day_of_week - 1];

        dateAndHour.setText(day + exhibition.getFormatedDayOfWeekAndHour());

        channelName.setText(exhibition.getChannelName());
        channelNumber.setText(exhibition.getChannelNumber().toString());
        exhibitionDetail.setText(exhibition.getTvShow().getDescription());
        exhibitionDirector.setText(exhibition.getTvShow().getDirector() != null ? String.format("%s %s", getResources().getString(R.string.director), exhibition.getTvShow().getDirector()) : "");

        exhibitionCast.setText(exhibition.getTvShow().getCast() != null ? String.format("%s %s", getResources().getString(R.string.cast), exhibition.getTvShow().getCast()) : "");

    }

    protected void configAgeRating(Exhibition exhibition) {
        containerAgeRating.setBackgroundResource(R.drawable.exhibition_genre_layout_corner);

        GradientDrawable drawable = (GradientDrawable) containerAgeRating.getBackground();

        String esbrRating = exhibition.getTvShow().getAgeRating();
        String color = "";

        if (esbrRating.equals("5")) {
            color = "#16A440";

        } else if (esbrRating.equals("10")) {
            color = "#19C1FF";

        } else if (esbrRating.equals("12")) {
            color = "#FFC30A";

        } else if (esbrRating.equals("14")) {
            color = "#FD4F06";

        } else if (esbrRating.equals("16")) {
            color = "#FB0006";

        } else if (esbrRating.equals("18")) {
            color = "#000000";

        }
        drawable.setColor(Color.parseColor(color));

        if (esbrRating.equals("5")) {
            ageRating.setText("L");
        } else {
            ageRating.setText(esbrRating);
        }
    }

    @Click(R.id.ic_back)
    protected void backClicked() {
        onBackPressed();
    }

}
