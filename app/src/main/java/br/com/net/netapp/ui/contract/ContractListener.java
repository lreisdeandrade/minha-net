package br.com.net.netapp.ui.contract;

import br.com.net.netapp.service.mobilebackend.models.Contract;

/**
 * Created by Leandro on 25/07/17.
 */

public interface ContractListener {

    void onContractClicked(Contract contract);
}
