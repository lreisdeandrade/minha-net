package br.com.net.netapp.ui.decoder;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Decoder;

/**
 * Created by Leandro on 25/07/17.
 */

@EViewGroup(R.layout.decoder_view)
public class DecoderView extends RelativeLayout {

    @ViewById
    TextView decoderCode, point, local;

    @ViewById
    FrameLayout frameReceptor;

    private Decoder decoder;
    private Context context;

    private DecoderListener listener;


    public DecoderView(Context context, Decoder decoder, DecoderListener decoderListener) {
        super(context);
        this.context = context;
        this.decoder = decoder;
        this.listener = decoderListener;

    }

    @AfterViews
    protected void setupView() {

        decoderCode.setText(decoder.getCode());
        point.setText(decoder.getType());
        local.setText(decoder.getLocalization());

    }

    @Click(R.id.frameReceptor)
    protected void frameClicked() {
        
        listener.onDecoderSelected(decoder);

    }

}
