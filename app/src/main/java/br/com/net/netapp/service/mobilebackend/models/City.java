package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 06/09/17.
 */

@RealmClass
public class City extends RealmObject {

    @SerializedName("name")
    @PrimaryKey
    @Expose
    private String name;
    @SerializedName("externalIds")
    @Expose
    private ExternalIds externalIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExternalIds getExternalIds() {
        return externalIds;
    }

    public void setExternalIds(ExternalIds externalIds) {
        this.externalIds = externalIds;
    }

    public static RealmList<City> getRealmlistDefault() {
        Realm realm = Realm.getDefaultInstance();

        List<City> citiesDefault = new ArrayList<>();
        City city = new City();
        city.setName("São Paulo");

        ExternalIds externalIds = new ExternalIds();
        externalIds.setSolrId(1);
        city.setExternalIds(externalIds);
        citiesDefault.add(city);

        return new RealmList<City>(citiesDefault.toArray(new City[citiesDefault.size()]));

    }

    @Override
    public boolean equals(Object cidade) {
        if (cidade == null) {
            return false;
        } else if (!(cidade instanceof City)) {
            return false;
        } else if (!this.name.equals(((City) cidade).getName())) {
            return false;
        }
        return true;
    }

}