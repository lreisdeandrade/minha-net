package br.com.net.netapp.service.mobilebackend.clients.tv.channel;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.ChannelDetail;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Leandro on 23/05/17.
 */

public interface IChannelClient {

    @GET("epg/channels/{channelId}")
    Call<ResponseData<ChannelDetail>> loadChannelDetail(@Path("channelId") int channelId, @QueryMap HashMap<String, String> queryString);
}
