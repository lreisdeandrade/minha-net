package br.com.net.netapp.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.event.UserTrySignoutEvent;
import br.com.net.netapp.event.WorkorderScheduledEvent;
import br.com.net.netapp.fragment.BookmarkFragment_;
import br.com.net.netapp.fragment.EpgFragment_;
import br.com.net.netapp.fragment.HighlightsFragment_;
import br.com.net.netapp.fragment.LicenseAgreementFragment_;
import br.com.net.netapp.fragment.ReminderFragment_;
import br.com.net.netapp.fragment.SettingsFragment_;
import br.com.net.netapp.helper.NotificationHandler;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.User;
import br.com.net.netapp.pref.NETAppPref_;
import br.com.net.netapp.service.aws.AWSCallBack;
import br.com.net.netapp.service.aws.AWSManager;
import br.com.net.netapp.service.aws.AWSResponse;
import br.com.net.netapp.service.aws.AwsDevice;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.auth.AuthClient;
import br.com.net.netapp.service.mobilebackend.clients.account.workorder.WorkOrderClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import io.realm.RealmList;

@EActivity(R.layout.activity_main)
public class MainActivity extends SearchActivity {

    private static final String TAG = "MainActivity";
    @ViewById
    NavigationView navigationView;

    @ViewById
    DrawerLayout drawerLayout;

    @ViewById
    Toolbar toolbar;

    @ViewById
    FrameLayout frame;

    @Bean
    SessionManager sessionManager;

    @Pref
    NETAppPref_ netAppPref;

    @App
    AppContext appContext;

    @Bean
    AwsDevice awsDevice;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_EPG = "epg";
    private static final String TAG_REMINDER = "reminder";
    private static final String TAG_BOOKMARK = "bookmark";
    private static final String TAG_LICENSE = "license";
    private static final String TAG_SETTINGS = "settings";
    private static final String TAG_SIGNOUT = "showLogoutDialog";

    private String[] activityTitlesEntertainment;
    private String[] activityTitlesCare;

    private Handler mHandler;
    private boolean shouldLoadHomeFragOnBackPress = true;

    public static int navItemIndex = 0;
    public static int navSectionIndex = 0;


    public static String CURRENT_TAG = TAG;

    private WorkOrderClient workOrderClient;
    private AuthClient authClient;
    private MaterialDialog dialogLogout;

    private br.com.net.netapp.service.mobilebackend.models.App app;

    @AfterViews
    protected void afterViews() {

        mHandler = new Handler();
        setSupportActionBar(toolbar);

        checkVersion();

        this.workOrderClient = new WorkOrderClient();
        this.authClient = new AuthClient();

        getGCMToken();

        activityTitlesEntertainment = getResources().getStringArray(R.array.nav_item_activity_titles_entertainment);
        activityTitlesCare = getResources().getStringArray(R.array.nav_item_activity_titles_care);

        setUpNavigationView();

//        navItemIndex = 0;
//        navSectionIndex = 0;

        CURRENT_TAG = TAG_HOME;
        loadHomeFragment();

        Intent intent = getIntent();

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            NotificationHandler notificationHandler = new NotificationHandler();
            notificationHandler.configDeepLinkFlow(this, intent);
        }

        configFlowMyTechnician();
        if (sessionManager.isActive() && BuildConfig.FLAVOR.equals("net")) {
            loadWorkOrders();
        }

        autoSignin();

    }

    private void autoSignin() {
        if (sessionManager.isActive()) {
            authClient.autoSignin(sessionManager.getUser().getToken(), new CallBackClient<User>() {
                @Override
                public void onSuccess(User result) {

                    getRealm().executeTransaction(realm -> {
                        User user = realm.where(User.class).findFirst();

                        result.setContractOperationCode(user.getContractOperationCode());
                        result.setContractSelectedCode(user.getContractSelectedCode());

                        realm.insertOrUpdate(result);
                    });
                }

                @Override
                public void onFailure(Throwable t) {

                    sessionManager.cleanDataBaseUserInfos();
                    sessionManager.deleteUserInSession();
                    ActivityRoutes.getInstance().openLoginActivity(MainActivity.this);
                }
            });
        }
    }

    protected void checkVersion() {

        Log.d(TAG, "checkVerion");
        if (!sessionManager.getAppSettings().getAndroidVersion().equals(BuildConfig.VERSION_NAME)) {
            new MaterialDialog.Builder(this)
                    .title("Atenção")
                    .content(R.string.update_message)
                    .positiveText(R.string.atualizar_agora)
                    .negativeText(R.string.lembrar_mais_tarde)
                    .onNegative((dialog, which) -> {
                        dialog.dismiss();
                        appContext.trackManagerEventPush("Atualize-seu-App", "clique", "Não", "");


                    })
                    .onPositive((dialog, which) -> {
                        appContext.trackManagerEventPush("Atualize-seu-App", "clique", "Sim", "");

                        final String appPackageName = BuildConfig.APPLICATION_ID;
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }

                        dialog.dismiss();
                    }).show();
        }
    }


    private void configFlowMyTechnician() {
        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            if (uri.toString().contains("meutecnico/timeline")) {
                MyTechnicianActivity_.intent(this).start();
            } else if (uri.toString().contains("epg/today")) {
                EpgActivity_.intent(this).start();
            }
        }

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String action = extras.getString("action");
            String operation = extras.getString("operation");

            if (!operation.equals("technical-cancel") && !operation.equals("technical-finish")) {
                if (action != null && action.contains("http")) {
                    MyTechnicianActivity_.intent(this).url(action).start();
                } else {
                    MyTechnicianActivity_.intent(this).start();
                }
            }
        }
    }

    @Background
    protected void getGCMToken() {
        String token = "";
        try {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(appContext.getApplicationContext());
            InstanceID instanceID = InstanceID.getInstance(appContext.getApplicationContext());
            token = instanceID.getToken(BuildConfig.PROJECT_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            netAppPref.gcmToken().put(token);
            Log.d(TAG, "Gcm token : " + token);

            if (!netAppPref.awsDeviceRegistered().exists()) {
                Log.d(TAG, "vai atualizou");

                registerDeviceAws();
            } else {
                Log.d(TAG, "nao atualizou");
            }
        } catch (Exception e) {
            Log.e(TAG, "GoogleCloudMessaging error: " + e.getLocalizedMessage());
        }
    }

    @UiThread
    protected void registerDeviceAws() {
        Log.d(TAG, "registrando Device ");


        AWSManager.getInstance().registerDeviceAWS(sessionManager.generateDeviceInfo(), new AWSCallBack<AWSResponse>() {
            @Override
            public void onSuccess(AWSResponse result) {

                if (result != null) {
                    Log.d(TAG, result.toString());
                    netAppPref.awsDeviceRegistered().put(true);
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "on failure ");

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWorkOrderScheduled(WorkorderScheduledEvent event) {
        Log.d(TAG, "onWorkOrderScheduled");
        showSnackBarWorkOrder(event.getWorkOrder());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLogoutDismissWorkOrder(UserSignoutEvent event) {
        Log.d(TAG, "onWorkOrderDismiss");
        dismissSnackBarWorkOrder();
        drawNavigationHeaderSignOut();

        updateDeviceAws();

        dialogLogout.dismiss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void userTryLogout(UserTrySignoutEvent event) {

        dialogLogout = new MaterialDialog.Builder(this)
                .title("Atenção")
                .content("Finalizando sua sessão")
                .progress(true, 0)
                .show();

    }

    private void updateDeviceAws() {
        AWSManager.getInstance().updateDeviceAWS(sessionManager.generateDeviceInfo(), new AWSCallBack<AWSResponse>() {
            @Override
            public void onSuccess(AWSResponse result) {

            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showVtIfExists();
    }

    private void loadWorkOrders() {
        this.workOrderClient.getWorkOrders(sessionManager.getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);

                List<WorkOrder> workOrders = (List<WorkOrder>) response;

                WorkOrder workOrder = null;
                for (int i = workOrders.size() - 1; i >= 0; i--) {
                    if (!workOrders.get(i).getScheduleDate().toString().contains("9999")) {
                        workOrder = workOrders.get(i);
                        break;
                    }
                }

                if (workOrder != null) {
                    showSnackBarWorkOrder(workOrder);
                } else {
//                    meuTecnicoPrefs.clear();
                }
            }

            @Override
            public void onError(ErrorMessage errorMessage) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        drawNavigationHeaderMenu();
    }

    private void drawNavigationHeaderSignin() {
        Button btnSignin = (Button) navigationView.getHeaderView(0).findViewById(R.id.btn_signin);
        btnSignin.setVisibility(View.GONE);
        User user = sessionManager.getUser();
        TextView txtTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.title_welcome);
        txtTitle.setVisibility(View.VISIBLE);

        txtTitle.setText(String.format("Bem vindo, %s", user.getName()));
        TextView txtSubTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.subtitle_welcome);
        txtSubTitle.setText(user.getEmail());
        txtSubTitle.setVisibility(View.INVISIBLE);
        Menu menu = navigationView.getMenu();
        MenuItem menuItemSignout = menu.findItem(R.id.nav_signout);
        menuItemSignout.setVisible(true);
        drawerLayout.closeDrawers();

    }

    private void drawNavigationHeaderSignOut() {
        Button btnSignin = (Button) navigationView.getHeaderView(0).findViewById(R.id.btn_signin);
        btnSignin.setVisibility(View.VISIBLE);
        TextView txtTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.title_welcome);
        txtTitle.setVisibility(View.INVISIBLE);

        TextView txtSubTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.subtitle_welcome);
        txtSubTitle.setText("Faça seu Login para mais acessos.");
        txtSubTitle.setVisibility(View.VISIBLE);


        Menu menu = navigationView.getMenu();
        MenuItem menuItemSignout = menu.findItem(R.id.nav_signout);
        menuItemSignout.setVisible(false);
    }

    public void drawNavigationHeaderMenu() {
        if (sessionManager.isActive()) {
            drawNavigationHeaderSignin();
        } else {
            drawNavigationHeaderSignOut();
        }
    }

    private void loadHomeFragment() {

        selectNavMenu();

        setToolbarTitle();


        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawerLayout.closeDrawers();

            return;
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                    getSupportFragmentManager().popBackStack();
                }

                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        }, 350);


        drawerLayout.closeDrawers();

        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navSectionIndex) {
            case 0:
                switch (navItemIndex) {
                    case 0:
                        // home
                        HighlightsFragment_ homeFragment = new HighlightsFragment_();
                        return homeFragment;
                    case 1:
                        // grade
                        EpgFragment_ epgFragment = new EpgFragment_();
                        return epgFragment;
                    case 2:
                        ReminderFragment_ reminderFragment = new ReminderFragment_();
                        return reminderFragment;
                    case 3:
                        BookmarkFragment_ bookmarkFragment = new BookmarkFragment_();
                        return bookmarkFragment;
                }
            case 1:
                switch (navItemIndex) {
                    case 0:
                        LicenseAgreementFragment_ licenseAgreementFragment = new LicenseAgreementFragment_();
                        return licenseAgreementFragment;
                    case 1:
                        SettingsFragment_ settingsFragment = new SettingsFragment_();
                        return settingsFragment;
                }
            default:
                HighlightsFragment_ highlightsFragment = new HighlightsFragment_();
                return highlightsFragment;
        }
    }

    private void setToolbarTitle() {
        if (getSupportActionBar() != null) {
            switch (navSectionIndex) {
                case 0:
                    getSupportActionBar().setTitle(activityTitlesEntertainment[navItemIndex]);
                    break;
                case 1:
                    getSupportActionBar().setTitle(activityTitlesCare[navItemIndex]);
                    break;
                default:
                    break;
            }
        }
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navSectionIndex).getSubMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                navigationView.getMenu().getItem(navSectionIndex).getSubMenu()
                        .getItem(navItemIndex).setChecked(false);

                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        navSectionIndex = 0;
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        appContext.trackManagerEventPush("menu-lateral", "Home", "meu-claro-app", "");
                        break;
                    case R.id.nav_epg:
                        navSectionIndex = 0;
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_EPG;
                        appContext.trackManagerEventPush("menu-lateral", "EPG", "meu-claro-app", "");
                        break;
                    case R.id.nav_reminder:
                        navSectionIndex = 0;
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_REMINDER;
                        appContext.trackManagerEventPush("menu-lateral", "lembrete", "meu-claro-app", "");
                        break;
                    case R.id.nav_favorite:
                        navSectionIndex = 0;
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_BOOKMARK;
                        appContext.trackManagerEventPush("menu-lateral", "favoritos", "meu-claro-app", "");
                        break;
                    case R.id.nav_license:
                        navSectionIndex = 1;
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_LICENSE;
                        appContext.trackManagerEventPush("menu-lateral", "termos-politicas", "mais-opcoes", "");
                        break;
                    case R.id.nav_settings:
                        navSectionIndex = 1;
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_SETTINGS;
                        appContext.trackManagerEventPush("menu-lateral", "configurações", "mais-opcoes", "");
                        break;
                    case R.id.nav_signout:
                        CURRENT_TAG = TAG_SIGNOUT;
                        appContext.trackManagerEventPush("menu-lateral", "sair", "usuario", "");
                        sessionManager.showLogoutDialog();
                        menuItem.setChecked(false);
                        drawerLayout.closeDrawers();
                        return false;
                    default:
                        CURRENT_TAG = TAG_HOME;
                        navItemIndex = 0;
                        navSectionIndex = 0;

                }
                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navSectionIndex > 0 || (navSectionIndex == 0 && navItemIndex != 0)) {

                navigationView.getMenu().getItem(navSectionIndex).getSubMenu()
                        .getItem(navItemIndex).setChecked(false);

                navItemIndex = 0;
                navSectionIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();

    }

    @Override
    public void onLongItemClick(View view, Object item, int position) {

    }
}