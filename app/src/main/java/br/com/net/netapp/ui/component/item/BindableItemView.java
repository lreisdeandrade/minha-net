package br.com.net.netapp.ui.component.item;

/**
 * Created by thiago on 05/08/16.
 */
public interface BindableItemView<T> {

    void bind(T object);
}
