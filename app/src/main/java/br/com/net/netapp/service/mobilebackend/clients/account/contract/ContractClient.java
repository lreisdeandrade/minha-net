package br.com.net.netapp.service.mobilebackend.clients.account.contract;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 25/07/17.
 */

public class ContractClient extends BaseClient {
    private String TAG = "EpgClient";
    private IContractClient contractClient;

    public ContractClient() {
        super(BuildConfig.BASE_URL_ACCOUNT);

        this.contractClient = this.retrofit.create(IContractClient.class);
    }

    public void loadContracts(String xAcessToken, final RealmCallbackClient callbackClient) {


        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("includes", "details,decoders");
//        queryStringMap.put("filters", "hdmax");
        this.contractClient.loadContracts(xAcessToken, queryStringMap).enqueue(new Callback<ResponseDataCollection<Contract>>() {
            @Override
            public void onResponse(Call<ResponseDataCollection<Contract>> call, Response<ResponseDataCollection<Contract>> response) {
                if (response.isSuccessful()) {
                    callbackClient.onSuccess(response.body().getDataInfo());
                } else {
                    ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                    callbackClient.onError(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<ResponseDataCollection<Contract>> call, Throwable t) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                callbackClient.onError(errorMessage);
            }
        });
    }
}
