package br.com.net.netapp.ui.component.epg;

import br.com.net.netapp.ui.component.epg.domain.EPGChannel;
import br.com.net.netapp.ui.component.epg.domain.EPGEvent;

/**
 * Created by Kristoffer on 15-05-25.
 */
public interface EPGClickListener {

    void onChannelClicked(int channelPosition, EPGChannel epgChannel);

    void onEventClicked(int channelPosition, int programPosition, EPGEvent epgEvent);

    void onResetButtonClicked();

}
