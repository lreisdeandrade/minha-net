package br.com.net.netapp.ui.search;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.activity.SinopseActivity_;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import timber.log.Timber;

/**
 * Created by Leandro on 05/06/17.
 */

@EFragment(R.layout.fragment_search)
public class ExhibitionSearchFragment extends Fragment implements
        CustomRecyclerListener<ExhibitionSearchRecyclerItem> {

    @FragmentArg("items")
    ArrayList<Exhibition> items;

    @ViewById
    FrameLayout fragmentContainerLayout;

    private CustomRecycler<ExhibitionSearchRecyclerItem> recycler;
    CustomRecyclerListener listener;
    private ArrayList<ExhibitionSearchRecyclerItem> recyclerItems;

    @AfterViews
    protected void afterViews() {
        recycler = new CustomRecycler<>(getContext(), this);
        fragmentContainerLayout.addView(recycler);
        if (getActivity() instanceof CustomRecyclerListener) {
            this.listener = ((CustomRecyclerListener) getActivity());
            add(items);
            recycler.setViewLoaded(true);
        } else {
            Timber.e("ExhibitionsSearchFragment", "afterViews: %s", "CustomRecyclerListener");
        }
    }


    public void add(List<Exhibition> items) {
        recyclerItems = new ArrayList<>();
        for (Exhibition exhibition : items) {
            recyclerItems.add(new ExhibitionSearchRecyclerItem(getContext(), exhibition));
        }
        recycler.add(recyclerItems);
    }

    public void removeAll() {
        recycler.removeAll();
    }

    @Override
    public void onItemClick(View view, ExhibitionSearchRecyclerItem item, int position) {
        AppContext.getInstance().trackManagerEventPush("busca", "programa", item.getModel().getTitle(), null);
        listener.onItemClick(view, item, position);
        SinopseActivity_.intent(this).idExhibition(item.getModel().getId()).start();
    }

    @Override
    public void onLongItemClick(View view, ExhibitionSearchRecyclerItem item, int position) {

    }

    public CustomRecycler getRecycler() {
        return recycler;
    }
}