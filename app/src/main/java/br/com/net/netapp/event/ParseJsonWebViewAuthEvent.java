package br.com.net.netapp.event;

import br.com.net.netapp.model.User;

/**
 * Created by Leandro on 24/04/17.
 */

public class ParseJsonWebViewAuthEvent {

    private String section;
    private User user;


    public ParseJsonWebViewAuthEvent(String section, User user) {
        this.section = section;
        this.user = user;
    }

    public String getSection() {
        return section;
    }

    public User getUser() {
        return user;
    }
}
