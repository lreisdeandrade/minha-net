package br.com.net.netapp.service.mobilebackend.clients.account.workorder;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 10/08/17.
 */

public class WorkOrderClient extends BaseClient {
    private String TAG = "EpgClient";
    private IWorkOrderClient iWorkOrderClient;

    public WorkOrderClient() {
        super(BuildConfig.BASE_URL_CARE);

        this.iWorkOrderClient = this.retrofit.create(IWorkOrderClient.class);
    }

    public void getWorkOrders(String xAcessToken, final RealmCallbackClient callbackClient) {

        this.iWorkOrderClient.getWorkOrders(xAcessToken).enqueue(new Callback<ResponseDataCollection<WorkOrder>>() {
            @Override
            public void onResponse(Call<ResponseDataCollection<WorkOrder>> call,
                                   Response<ResponseDataCollection<WorkOrder>> response) {
                if (response.isSuccessful()) {
                    callbackClient.onSuccess(response.body().getDataInfo());
                }
            }

            @Override
            public void onFailure(Call<ResponseDataCollection<WorkOrder>> call, Throwable t) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                callbackClient.onError(errorMessage);
            }
        });
    }

}