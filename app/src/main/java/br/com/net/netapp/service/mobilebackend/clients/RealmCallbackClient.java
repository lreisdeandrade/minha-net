package br.com.net.netapp.service.mobilebackend.clients;

import java.util.List;

import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.ChannelDetail;
import br.com.net.netapp.service.mobilebackend.models.Epg;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.service.mobilebackend.models.Highlight;
import br.com.net.netapp.service.mobilebackend.models.Screen;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

public abstract class RealmCallbackClient<T extends RealmObject> {
    private String TAG = "CalbackRequestMobileBackend";
    private Realm realm;

    public RealmCallbackClient() {
        this.realm = Realm.getDefaultInstance();
    }

    public void onSuccess(boolean success) {

    }

    public void onSuccess(final T response) {

        realm.executeTransaction(realm1 -> {

            if (response instanceof Exhibition) {

                Exhibition exhibition = (Exhibition) response;

                exhibition.setChannelId(((Exhibition) response).getChannel().getId());
                exhibition.setChannelName(((Exhibition) response).getChannel().getName());
                exhibition.setChannelNumber(((Exhibition) response).getChannel().getNumber());
                exhibition.setChannelCategoryName(((Exhibition) response).getChannel().getCategoryName());
                exhibition.setChannelLogoUrl(((Exhibition) response).getChannel().getLogoUrl());
                exhibition.setGenre(((Exhibition) response).getTvShow().getGenres());

                realm1.insertOrUpdate(exhibition);

            } else if (response instanceof ChannelDetail) {

                ChannelDetail channelDetail = (ChannelDetail) response;

                for (Exhibition exhibition : channelDetail.getExhibitions()) {

                    Exhibition e = realm1.where(Exhibition.class).equalTo("id", exhibition.getId()).findFirst();

                    if (e != null) {
                        exhibition.setTvShow(e.getTvShow());
                        exhibition.setChannelName(e.getChannelName());
                        exhibition.setChannelLogoUrl(e.getChannelLogoUrl());
                        exhibition.setChannelNumber(e.getChannelNumber());
                        exhibition.setChannelCategoryName(e.getChannelCategoryName());
                        exhibition.setChannelId(e.getChannelId());
                    }
                }

                realm1.insertOrUpdate(response);
            } else if (response instanceof Screen) {

                Screen screen = realm1.where(Screen.class).findFirst();
                if (screen != null) {
                    List<Highlight> highlightResponse = ((Screen) response).getHighlights();
                    List<Highlight> highlightRealm = screen.getHighlights();
                    boolean changeHighlights = highlightRealm.equals(highlightResponse);

                    if (!changeHighlights) {
                        screen.getHighlights().deleteAllFromRealm();
                        screen.deleteFromRealm();

                        realm1.insertOrUpdate(response);
                    }
                } else {
                    realm1.insertOrUpdate(response);
                }

            } else if (response instanceof Epg) {

                Realm.getDefaultModule();
                realm1.insertOrUpdate(response);

            } else if (response instanceof App) {
                App appRealm = realm1.where(App.class).findFirst();
                if (appRealm != null) {

                    App appResponse = (App) response;

                    appResponse.setEnabledTouchId(appRealm.getEnabledTouchId());

                    appResponse.getSettings().setSelectedStateId(appRealm.getSettings().getSelectedStateId());
                    appResponse.getSettings().setSelectedCityId(appRealm.getSettings().getSelectedCityId());
                    appResponse.getSettings().setSelectedCityName(appRealm.getSettings().getSelectedCityName());
                    appResponse.getSettings().setSelectedStateName(appRealm.getSettings().getSelectedStateName());

                    realm1.insertOrUpdate(appResponse);

                } else {
                    realm1.insertOrUpdate(response);
                }
            } else {
                if (response != null) {
                    realm1.insertOrUpdate(response);
                }
            }
        });
    }

    public void onSuccess(final RealmList<T> response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.insertOrUpdate(response);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromSaveInRealm("Realm.Transaction.OnError", error);

                RealmCallbackClient.this.onError(errorMessage);
            }
        });
    }


    public abstract void onError(ErrorMessage errorMessage);
}
