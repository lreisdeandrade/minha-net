package br.com.net.netapp.ui.login;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EView;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;

/**
 * Created by Leandro on 02/06/17.
 */
@EView
public class LoginButton extends Button implements View.OnClickListener {

    @App
    AppContext appContext;

    private static final String TAG = "LoginButton";

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (Connectivity.create(getContext()).isAvailable()) {

            if (v.getTag() != null) {
                appContext.trackManagerEventPush(v.getTag().toString(), "logar", "tentou", null);

            }

            ActivityRoutes.getInstance().openLoginActivity(getContext());
        } else {
            new MaterialDialog.Builder(getContext())
                    .title("Atenção")
                    .content(R.string.no_connection)
                    .neutralText(R.string.btn_ok)
                    .onNeutral((dialog, which) -> {
                        dialog.dismiss();
                    }).show();
        }
    }
}