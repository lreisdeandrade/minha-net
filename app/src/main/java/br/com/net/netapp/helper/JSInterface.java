package br.com.net.netapp.helper;

import android.webkit.JavascriptInterface;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.EventBus;

import br.com.net.netapp.event.ParseJsonWebViewAuthEvent;
import br.com.net.netapp.event.StartFormSubmitEvent;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import timber.log.Timber;

/**
 * Created by Leandro on 24/04/17.
 */

@EBean
public class JSInterface {

    @Bean
    SessionManager sessionManager;


    private static final String TAG = "JSInterface";

    @JavascriptInterface
    public void startFormSubmit() {
        Timber.d("startFormSubmit: acessando");
        EventBus.getDefault().post(new StartFormSubmitEvent());
    }


    @JavascriptInterface
    public void parseJsonSignIn(String json) {
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(json);

        JsonObject dataObject = jsonObject.getAsJsonObject("data");
        JsonObject erroObject = jsonObject.getAsJsonObject("error");

        if (erroObject != null) {
            String code = String.valueOf(erroObject.get("code")).replaceAll("\"", "");

            if (code.equals("mb-403-user-denied-permissions")) {
                EventBus.getDefault().post(new ParseJsonWebViewAuthEvent("user_denied_permissions", null));
            }else{
                EventBus.getDefault().post(new ParseJsonWebViewAuthEvent("mb_404", null));
            }

        } else {

            Gson gson = new GsonBuilder().create();
            User user = gson.fromJson(dataObject, User.class);

            EventBus.getDefault().post(new ParseJsonWebViewAuthEvent("signin", user));
        }
    }

    @JavascriptInterface
    public void parseJsonSignOut(String json) {

        JsonObject jsonObject = (JsonObject) new JsonParser().parse(json);
        JsonObject dataObject = jsonObject.getAsJsonObject("data");
        JsonObject erroObject = jsonObject.getAsJsonObject("error");


        if (erroObject != null) {
            Gson gson = new GsonBuilder().create();
            ErrorMessage error = gson.fromJson(erroObject, ErrorMessage.class);
            if (error.getCode().equals("mb-403-invalid-access-token")) {
                EventBus.getDefault().post(new ParseJsonWebViewAuthEvent("showLogoutDialog", null));
            }

        } else {
            Boolean sessionClosed = (dataObject.getAsJsonPrimitive("sessionClosed")).isBoolean();
            if (sessionClosed) {
                EventBus.getDefault().post(new ParseJsonWebViewAuthEvent("showLogoutDialog", null));
            }
        }
    }
}