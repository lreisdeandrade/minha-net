package br.com.net.netapp.service.mobilebackend.clients;

/**
 * Created by Leandro on 05/06/17.
 */

public interface CallBackClient<T> {
    void onSuccess(T result);

    void onFailure(Throwable t);
}
