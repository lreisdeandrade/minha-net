package br.com.net.netapp.service.mobilebackend.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Exhibition extends RealmObject implements Parcelable {

    public static final SimpleDateFormat HORA_FORMAT = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat MONTH_YEAR_FORMAT = new SimpleDateFormat("MMMM y");
    public static final SimpleDateFormat DAY_FORMAT = new SimpleDateFormat("dd");
    public static final SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat(" à's' HH:mm");
    public static final SimpleDateFormat DAY_MONTH_AND_START_HOUR = new SimpleDateFormat("EEE, dd 'de' MMM à's' HH:mm");


    @PrimaryKey
    private String id;
    private String title;
    private Date startDate;
    private Date endDate;

    @SerializedName("genres")
    private String genre;

    private Integer channelId;
    private String channelName;
    private Integer channelNumber;
    private String channelCategoryName;
    private String channelLogoUrl;

    @Ignore
    private ChannelDTO channel;
    private TvShow tvShow;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Integer getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(Integer channelNumber) {
        this.channelNumber = channelNumber;
    }

    public String getChannelCategoryName() {
        return channelCategoryName;
    }

    public void setChannelCategoryName(String channelCategoryName) {
        this.channelCategoryName = channelCategoryName;
    }

    public String getChannelLogoUrl() {
        return channelLogoUrl;
    }

    public void setChannelLogoUrl(String channelLogoUrl) {
        this.channelLogoUrl = channelLogoUrl;
    }

    public String getFormattedDhInicioDhFim() {
        return String.format("%s - %s", HORA_FORMAT.format(this.startDate), HORA_FORMAT.format(this.endDate));

    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public ChannelDTO getChannel() {
        return channel;
    }

    public void setChannel(ChannelDTO channel) {
        this.channel = channel;
    }

    public TvShow getTvShow() {
        return tvShow;
    }

    public void setTvShow(TvShow tvShow) {
        this.tvShow = tvShow;
    }

    public String getFormatedMonthAndYear() {
        return MONTH_YEAR_FORMAT.format(this.startDate);
    }

    public String getFormatedDayOfMonth() {
        return DAY_FORMAT.format(this.startDate);

    }

    public String getFormatedDayOfWeekAndHour() {
        return HOUR_FORMAT.format(this.startDate);

    }
    public String getFormatedDayMonthAndHour() {
        return DAY_MONTH_AND_START_HOUR.format(this.startDate);

    }

    public Boolean isRunning() {
        return this.getStartDate().before(new Date()) && this.getEndDate().after(new Date());
    }

    public int getProgress() {
        Date now = new Date() ;
        double tempoTotal = getEndDate().getTime() - getStartDate().getTime() ;
        double tempoRestante = getEndDate().getTime() - now.getTime() ;

        return (int) ((1 - (tempoRestante/tempoTotal)) * 100) ;
    }

    public Boolean canScheduleLocalNotification() {
        Date dtAgora = new Date();
        long diffDh = ((this.getStartDate().getTime() - dtAgora.getTime()) / (1000 * 60));
        return (diffDh > 15);
    }

    public Boolean canRecordExhibition() {
        Date now = new Date();

        return now.before(this.getStartDate());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeLong(this.startDate != null ? this.startDate.getTime() : -1);
        dest.writeLong(this.endDate != null ? this.endDate.getTime() : -1);
        dest.writeString(this.genre);
        dest.writeValue(this.channelId);
        dest.writeString(this.channelName);
        dest.writeValue(this.channelNumber);
        dest.writeString(this.channelCategoryName);
        dest.writeString(this.channelLogoUrl);
        dest.writeParcelable(this.channel, flags);
        dest.writeParcelable(this.tvShow, flags);
    }

    public Exhibition() {
    }

    protected Exhibition(Parcel in) {
        this.id = (String) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        long tmpStartDate = in.readLong();
        this.startDate = tmpStartDate == -1 ? null : new Date(tmpStartDate);
        long tmpEndDate = in.readLong();
        this.endDate = tmpEndDate == -1 ? null : new Date(tmpEndDate);
        this.genre = in.readString();
        this.channelId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.channelName = in.readString();
        this.channelNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.channelCategoryName = in.readString();
        this.channelLogoUrl = in.readString();
        this.channel = in.readParcelable(ChannelDTO.class.getClassLoader());
        this.tvShow = in.readParcelable(TvShow.class.getClassLoader());
    }

    public static final Parcelable.Creator<Exhibition> CREATOR = new Parcelable.Creator<Exhibition>() {
        @Override
        public Exhibition createFromParcel(Parcel source) {
            return new Exhibition(source);
        }

        @Override
        public Exhibition[] newArray(int size) {
            return new Exhibition[size];
        }
    };
}
