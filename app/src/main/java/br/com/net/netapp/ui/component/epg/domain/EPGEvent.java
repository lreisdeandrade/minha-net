package br.com.net.netapp.ui.component.epg.domain;

import br.com.net.netapp.service.mobilebackend.models.Exhibition;

/**
 * Created by Leandro on 04/05/17.
 */

public class EPGEvent {

    private final Exhibition exhibition;

    public EPGEvent(Exhibition solrExibicao) {
        this.exhibition = solrExibicao;
    }

    public Exhibition getExhibition(){
        return this.exhibition;
    }

    public long getStart() {
        return this.exhibition.getStartDate().getTime();
    }

    public long getEnd() {
        return this.exhibition.getEndDate().getTime();
    }

    public String getTitle() {
        return this.exhibition.getTitle();
    }
    public String getHour() {
        return this.exhibition.getFormattedDhInicioDhFim();
    }

    public boolean isCurrent() {
        long now = System.currentTimeMillis();
        return now >= this.getStart() && now <= this.getEnd();
    }
}
