package br.com.net.netapp.adapter;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.event.LoadFiltersEvent;
import br.com.net.netapp.event.OnChangesFilterChanged;
import br.com.net.netapp.model.Filter;
import br.com.net.netapp.service.mobilebackend.models.Category;
import br.com.netcombo.trackmanager.events.Event;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by elourenco on 10/05/17.
 */

@EBean
public class EpgChannelCategoryAdapter extends BaseAdapter {

    @RootContext
    Context context;
    private RealmResults<Category> items;
    private Realm realm;

    private HashMap<Integer, Boolean> originFilter;
    private HashMap<Integer, Boolean> changeFilter;

    @AfterInject
    public void afterInject() {
        realm = Realm.getDefaultInstance();
        originFilter = new HashMap<>();
        changeFilter = new HashMap<>();

        List<Category> categories = realm.where(Category.class).findAllSorted("name", Sort.ASCENDING);

        if(categories != null){
            EventBus.getDefault().post(new LoadFiltersEvent());
        }
        for (Category category : categories) {

            originFilter.put(category.getId(), Filter.has(category));
        }


        this.items = realm.where(Category.class).findAll().sort("name");
        this.items.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Category>>() {
            @Override
            public void onChange(RealmResults<Category> collection, OrderedCollectionChangeSet changeSet) {

                notifyDataSetChanged();
                EventBus.getDefault().post(new LoadFiltersEvent());
                if (Filter.categoriesList().size() > 0) {
                    for (Category categoriaFilter : Filter.categoriesList()) {
                        changeFilter.put(categoriaFilter.getId(), true);
                    }
                }
            }
        });
    }

    public void onDestroy() {

        if (!realm.isClosed()) {

            realm.close();
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // if (convertView == null) {
        convertView = LayoutInflater.from(context).inflate(R.layout.adapter_epg_category_item, parent, false);
        // }

        final Category category = (Category) getItem(position);
        if (category != null) {
            TextView textViewItemName = (TextView)
                    convertView.findViewById(R.id.name);

            SwitchCompat switchIdCategories = (SwitchCompat)
                    convertView.findViewById(R.id.switchIdCategories);

            textViewItemName.setText(category.getName());

            Boolean isChecked = false;

            if (changeFilter.containsKey(category.getId())) {
                isChecked = changeFilter.get(category.getId());
            }

            switchIdCategories.setChecked(isChecked);

            switchIdCategories.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (originFilter.containsKey(category.getId())) {
                        if (originFilter.get(category.getId()) == isChecked) {
                            changeFilter.remove(category.getId());
                        } else {
                            changeFilter.put(category.getId(), isChecked);
                        }
                    } else {
                        changeFilter.put(category.getId(), isChecked);
                    }

                    EventBus.getDefault().post(new OnChangesFilterChanged(changeFilter.size()));

                }
            });
        }

        return convertView;
    }

    public HashMap<Integer, Boolean> getOriginFilter() {
        return originFilter;
    }

    public void setOriginFilter(HashMap<Integer, Boolean> originFilter) {
        this.originFilter = originFilter;
    }

    public HashMap<Integer, Boolean> getChangeFilter() {
        return changeFilter;
    }

    public void setChangeFilter(HashMap<Integer, Boolean> changeFilter) {
        this.changeFilter = changeFilter;
    }
}
