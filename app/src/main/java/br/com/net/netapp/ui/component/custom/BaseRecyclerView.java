package br.com.net.netapp.ui.component.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by thiago on 25/08/16.
 */
public class BaseRecyclerView extends RecyclerView {

    private View mEmptyView;

    private AdapterDataObserver mDataObserver = new AdapterDataObserver() {

        @Override
        public void onChanged() {

            super.onChanged();

            updateEmptyView();
        }
    };

    public BaseRecyclerView(Context context) {
        super(context);
    }

    public BaseRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public View getmEmptyView() {
        return mEmptyView;
    }

    public void setmEmptyView(View mEmptyView) {
        this.mEmptyView = mEmptyView;
    }

    @Override
    public void setAdapter(Adapter adapter) {

        if (!adapter.hasObservers()) {

            adapter.registerAdapterDataObserver(mDataObserver);

            super.setAdapter(adapter);

            updateEmptyView();

        }
    }

    private void updateEmptyView() {

        if (mEmptyView != null && getAdapter() != null) {

            boolean showEmptyView = getAdapter().getItemCount() == 0;

            mEmptyView.setVisibility(showEmptyView ? VISIBLE : GONE);

            setVisibility(showEmptyView ? GONE : VISIBLE);
        }
    }

}
