package br.com.net.netapp.ui;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import timber.log.Timber;

/**
 * Created by gibranlyra on 12/06/17.
 */

public class AlarmHelper {

    public static void scheduleNotification(Context context, Notification notification,
                                            int notificationId, long milliSecondsDelay) {

        Timber.d("scheduleNotification: %s", notificationId );

        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, milliSecondsDelay, pendingIntent);
    }

    public static void cancelNotification(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationPublisher.class);

        PendingIntent sender = PendingIntent.getBroadcast(context, notificationId, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
