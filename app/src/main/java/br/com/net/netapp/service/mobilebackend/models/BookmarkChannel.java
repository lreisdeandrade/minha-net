package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 27/06/17.
 */

@RealmClass
public class BookmarkChannel extends RealmObject {

    @SerializedName("categoryName")
    private String categoryName;

    @SerializedName("categoryId")
    private int categoryId;

    @SerializedName("logoUrl")
    private String logoUrl;

    @SerializedName("number")
    private int number;

    @SerializedName("name")
    private String name;

    @SerializedName("exhibition")
    private Exhibition exhibition;

    @PrimaryKey
    private int id;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Exhibition getExhibition() {
        return exhibition;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BookmarkChannel{" +
                "categoryName='" + categoryName + '\'' +
                ", categoryId=" + categoryId +
                ", logoUrl='" + logoUrl + '\'' +
                ", number=" + number +
                ", name='" + name + '\'' +
                ", exhibition=" + exhibition +
                ", id=" + id +
                '}';
    }
}
