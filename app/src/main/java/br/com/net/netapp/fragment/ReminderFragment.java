package br.com.net.netapp.fragment;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.activity.SinopseActivity_;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.reminder.ReminderClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import br.com.net.netapp.ui.reminder.ReminderRecyclerItem;
import io.realm.RealmList;
import timber.log.Timber;

/**
 * Created by Leandro on 08/06/17.
 */

@EFragment(R.layout.fragment_reminder)
public class ReminderFragment extends BaseFragment implements
        CustomRecyclerListener<ReminderRecyclerItem> {

    private static final String TAG = "ReminderFragment";
    @ViewById
    FrameLayout fragmentContainerLayout;

    @ViewById
    LinearLayout containerReminderLogin;

    @Bean
    SessionManager sessionManager;

    CustomRecyclerListener listener;

    private CustomRecycler<ReminderRecyclerItem> recycler;

    private ArrayList<ReminderRecyclerItem> recyclerItems;

    private List<Reminder> reminders;

    ReminderClient reminderClient;


    @AfterViews
    protected void afterviews() {
        reminderClient = new ReminderClient();


        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        if (getActivity() instanceof CustomRecyclerListener) {
            this.listener = ((CustomRecyclerListener) getActivity());

        } else {
            throw new RuntimeException("You must implements CustomRecyclerListener");
        }

        setupScreen();

    }

    protected void setupScreen() {
        recycler = new CustomRecycler<>(getContext(), this);
        fragmentContainerLayout.addView(recycler);
        recycler.setViewLoaded(true);
        recycler.setNoContentText(getResources().getString(R.string.empty_reminder));
    }

    @Override
    public void onResume() {
        super.onResume();

        isActiveRecycler(sessionManager.isActive());

    }

    protected void isActiveRecycler(Boolean isActive) {
        if (isActive) {
            loadReminders();
            fragmentContainerLayout.setVisibility(View.VISIBLE);
            containerReminderLogin.setVisibility(View.GONE);
        } else {
            containerReminderLogin.setVisibility(View.VISIBLE);
            fragmentContainerLayout.setVisibility(View.GONE);
        }
    }

    public void add() {
        recyclerItems = new ArrayList<>();

        for (Reminder reminder : reminders) {
            recyclerItems.add(new ReminderRecyclerItem(getContext(), reminder));
        }

        if (recycler != null) {
            recycler.add(recyclerItems);

        } else {
            recycler = new CustomRecycler<>(getContext(), this);
            recycler.add(recyclerItems);

        }
    }

    protected void loadReminders() {

        reminderClient.loadReminders(sessionManager.getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {

            }

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);
                reminders = (List<Reminder>) response;

                add();
            }

        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLogout(UserSignoutEvent event) {
        isActiveRecycler(sessionManager.isActive());
    }

    @Override
    public void onItemClick(View view, ReminderRecyclerItem item, int position) {

        SinopseActivity_.intent(this).idExhibition(item.getModel().getExhibition().getId()).start();

    }

    @Override
    public void onLongItemClick(View view, ReminderRecyclerItem item, int position) {
        new MaterialDialog.Builder(getActivity())
                .title("Lembretes")
                .positiveText(R.string.sim)
                .negativeText(R.string.nao)
                .content(R.string.remove_reminder).onPositive((dialog, which) -> {
            try {

                reminderClient.deleteReminder(sessionManager.getUser().getToken(), item.getModel().getId(), new RealmCallbackClient() {
                    @Override
                    public void onError(ErrorMessage errorMessage) {

                    }

                    @Override
                    public void onSuccess(boolean success) {
                        super.onSuccess(success);
                        deleteFromRealm(item.getModel().getId());
                    }
                });
                recycler.remove(position);

            } catch (Exception e) {

                Timber.e(e, "onLongItemClick: %s", e.getMessage());
                Toast.makeText(getContext(), "Não foi possível remover o lembrete", Toast.LENGTH_SHORT).show();
            }
        }).onNegative((dialog, which) -> {
            dialog.dismiss();
        })

                .show();
    }

    private void deleteFromRealm(String reminderId) {
        getRealm().executeTransactionAsync(realm1 -> {
            Reminder reminder = realm1.where(Reminder.class).equalTo("id", reminderId).findFirst();
            reminder.deleteFromRealm();
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }
}