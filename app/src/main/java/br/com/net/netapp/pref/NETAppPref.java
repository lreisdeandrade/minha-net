package br.com.net.netapp.pref;

import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Leandro on 12/09/17.
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface NETAppPref {

    String gcmToken();

    boolean awsDeviceRegistered();


}
