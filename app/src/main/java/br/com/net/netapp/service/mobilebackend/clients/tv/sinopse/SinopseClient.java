package br.com.net.netapp.service.mobilebackend.clients.tv.sinopse;

/**
 * Created by Leandro on 17/05/17.
 */

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SinopseClient extends BaseClient {
    private String TAG = "EpgClient";
    private ISinopseClient sinopseClient;

    public SinopseClient() {
        super(BuildConfig.BASE_URL_TV);

        this.sinopseClient = this.retrofit.create(ISinopseClient.class);
    }

    public void loadSinopse(String idExhibition, final RealmCallbackClient callbackClient) {

        this.sinopseClient.loadExhibitionDetail(idExhibition)
                .enqueue(new Callback<ResponseData<Exhibition>>() {
                    @Override
                    public void onResponse(Call<ResponseData<Exhibition>> call, Response<ResponseData<Exhibition>> response) {
                        if (response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().data);
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<Exhibition>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }

    public void recordExhibition(String xAccessToken, String idExhibition, String decoderCode, String contractCode, String operationCode, final CallBackClient<ResponseData> callback) {

        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("equipmentId", decoderCode);
        queryStringMap.put("contractCode", contractCode);
        queryStringMap.put("operationCode", operationCode);

        this.sinopseClient.recordExhibition(xAccessToken, idExhibition, queryStringMap)
                .enqueue(new Callback<ResponseData<Exhibition>>() {
                    @Override
                    public void onResponse(Call<ResponseData<Exhibition>> call, Response<ResponseData<Exhibition>> response) {
                        if (response.isSuccessful()) {
                            callback.onSuccess(response.body());
                        } else {
                            if (response.code() == 400) {
                                callback.onFailure(new Exception("Essa gravação já está agendada"));
                            } else if (response.code() == 500) {
                                callback.onFailure(new Exception("Ocorreu um erro, por favor tente novamente mais tarde"));

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<Exhibition>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callback.onFailure(new Throwable("erro"));
                    }
                });
    }
}
