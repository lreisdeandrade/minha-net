package br.com.net.netapp.service.mobilebackend.clients.tv.search;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.Data;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Leandro on 05/06/17.
 */

public interface ISearchClient {

    @GET("search")
    Call<ResponseData<Data>> searchItems(@QueryMap HashMap<String, String> queryString);
}
