package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

public class ResponseError {
    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("requestId")
    private String requestId;

    @SerializedName("errorMessage")
    private ErrorMessage errorMessage;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }
}
