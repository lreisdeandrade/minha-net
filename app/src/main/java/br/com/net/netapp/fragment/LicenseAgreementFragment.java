package br.com.net.netapp.fragment;

import android.content.DialogInterface;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;

/**
 * Created by thiagomagalhaes on 08/06/17.
 */

@EFragment(R.layout.fragment_license_agreement)
public class LicenseAgreementFragment extends BaseFragment {

    @ViewById
    WebView webview;

    @ViewById
    RelativeLayout loading;

    @AfterViews
    void afterViews() {

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                if(loading != null){
                    loading.animate().alpha(0.0f);
                    loading.setVisibility(View.INVISIBLE);
                }
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                loading.animate().alpha(0.0f);
                loading.setVisibility(View.INVISIBLE);

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Deseja continuar ?");
                builder.setPositiveButton("sim",
                        (dialog, which) -> handler.proceed());
                builder.setNegativeButton("não",
                        (dialog, which) -> {
                            handler.cancel();

                            Toast.makeText(getActivity(),
                                    "Ação não concluida!",
                                    Toast.LENGTH_SHORT).show();

                        });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                loading.animate().alpha(0.0f);
                loading.setVisibility(View.INVISIBLE);
            }
        });
        webview.loadUrl(BuildConfig.LICENSE_AGREEMENT_URL);
    }
}
