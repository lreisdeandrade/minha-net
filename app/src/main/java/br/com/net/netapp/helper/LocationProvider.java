package br.com.net.netapp.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import static android.location.LocationProvider.OUT_OF_SERVICE;
import static android.location.LocationProvider.TEMPORARILY_UNAVAILABLE;

public class LocationProvider implements LocationListener {

    private LocationProviderListener listener;

    private LocationManager locationManager;

    private Context context;

    public LocationProvider(Context context) {

        this.context = context;


    }

    public boolean getLocation(LocationProviderListener listener) {

        this.listener = listener;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        String bestProvider = getBestProvider(locationManager);

        if (bestProvider != null && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager.requestLocationUpdates(bestProvider, 0, 0, this);

            Location location = locationManager.getLastKnownLocation(bestProvider);

            if (location != null) {

                listener.onSuccess(location);

                locationManager.removeUpdates(this);

            } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

                locationManager.removeUpdates(this);
                listener.onFailure();
            }else{
                listener.onFailure();
            }

        } else {

            return false;
        }

        return true;
    }

    private String getBestProvider(LocationManager locationManager) {

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        return locationManager.getBestProvider(criteria, true);
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location == null) {

            listener.onFailure();

        } else {

            listener.onSuccess(location);
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates(this);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

        if ((status == OUT_OF_SERVICE) || (status == TEMPORARILY_UNAVAILABLE)) {

            listener.onFailure();

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.removeUpdates(this);
            }
        }
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}