package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 25/07/17.
 */

@RealmClass
public class Contract extends RealmObject {


    @SerializedName("code")
    @Expose
    @PrimaryKey
    private String code;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("operationCode")
    @Expose
    private String operationCode;

    @SerializedName("saleDate")
    @Expose
    private String saleDate;

    @SerializedName("dayExpiration")
    @Expose
    private String dayExpiration;

    @SerializedName("billingType")
    @Expose
    private String billingType;

    @SerializedName("address")
    @Expose
    private Address address;

    @SerializedName("decoders")
    @Expose
    private RealmList<Decoder> decoders;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getDayExpiration() {
        return dayExpiration;
    }

    public void setDayExpiration(String dayExpiration) {
        this.dayExpiration = dayExpiration;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public RealmList<Decoder> getDecoders() {
        return decoders;
    }

    public void setDecoders(RealmList<Decoder> decoders) {
        this.decoders = decoders;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getFormattedOperationAndCodeContract() {
        if (this.getCode() != null) {
            return String.format("%s/%s", this.getOperationCode(), this.getCode());

        } else {
            return "Selecione seu contrato";
        }
    }

    @Override
    public String toString() {
        return "{" +
                "code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", operationCode='" + operationCode + '\'' +
                ", saleDate='" + saleDate + '\'' +
                ", dayExpiration='" + dayExpiration + '\'' +
                ", billingType='" + billingType + '\'' +
                ", address=" + address +
                ", decoders=" + decoders +
                '}';
    }
}
