package br.com.net.netapp.service.mobilebackend.clients.tv.channel;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ChannelDetail;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 23/05/17.
 */

public class ChannelClient extends BaseClient {
    private String TAG = "EpgClient";
    private IChannelClient channelClient;

    public ChannelClient() {
        super(BuildConfig.BASE_URL_TV);

        this.channelClient = this.retrofit.create(IChannelClient.class);
    }

    public void loadChannelDetail(int channelId, String cityId, final RealmCallbackClient callbackClient) {
        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("cityId", cityId);

        this.channelClient.loadChannelDetail(channelId, queryStringMap).enqueue(new Callback<ResponseData<ChannelDetail>>() {
                    @Override
                    public void onResponse(Call<ResponseData<ChannelDetail>> call, Response<ResponseData<ChannelDetail>> response) {
                        if(response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().getData());
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<ChannelDetail>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }
}