package br.com.net.netapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.app.AppClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import io.realm.RealmObject;

/**
 * Created by Leandro on 05/09/17.
 */

public class SplashScreenActivity extends BaseActivity {

    private AppClient appClient ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appClient = new AppClient();

        App app = getRealm().where(App.class).findFirst();

        if (app != null && app.getSettings().getSelectedCityId() != null) {
            MainActivity_.intent(this).start();
        } else {
            RegionSelectActivity_.intent(this).isFromSplash(true).start();
        }
        finish();
    }

}
