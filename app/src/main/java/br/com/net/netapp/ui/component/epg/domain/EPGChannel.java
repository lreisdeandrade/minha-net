package br.com.net.netapp.ui.component.epg.domain;

import br.com.net.netapp.service.mobilebackend.models.Channel;

/**
 * Created by Leandro on 04/05/17.
 */

public class EPGChannel implements Comparable{

    private final Channel channel;

    public EPGChannel(Channel channel) {
        this.channel = channel;
    }

    public Channel getSolrCanal() {
        return channel;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }
}

