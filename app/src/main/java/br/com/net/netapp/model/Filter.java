package br.com.net.netapp.model;

import java.util.List;

import br.com.net.netapp.service.mobilebackend.models.Category;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by elourenco on 10/05/17.
 */

public class Filter extends RealmObject {

    private RealmList<Category> categories;


    public RealmList<Category> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<Category> categories) {
        this.categories = categories;
    }

    private static Filter getInstance() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Filter> query = realm.where(Filter.class).findAll();

        Filter filter;

        if (query.size() == 0) {

            filter = new Filter();

            realm.beginTransaction();

            realm.insert(filter);

            realm.commitTransaction();

        } else {

            filter = query.first();
        }

        realm.close();

        return filter;
    }

    public static boolean has(Integer id) {
        Filter filter = getInstance();
        Realm realm = Realm.getDefaultInstance();
        Category c = realm.where(Category.class).equalTo("id", id).findFirst();
        return filter.getCategories().contains(c);
    }

    public static boolean has(Category categories) {
        Filter filter = getInstance();
        return filter.getCategories().contains(categories);
    }

    public static List<Category> categoriesList() {
        Filter filter = getInstance();
        return filter.getCategories();
    }

    public static void addCategory(Category category) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        categoriesList().add(category);
        realm.commitTransaction();
        realm.close();
    }

    public static void removeCategory(Category category) {
        Realm realm = Realm.getDefaultInstance();
        Filter filter = getInstance();
        realm.beginTransaction();
        filter.getCategories().remove(filter.getCategories().indexOf(category));
        realm.commitTransaction();
        realm.close();
    }

}
