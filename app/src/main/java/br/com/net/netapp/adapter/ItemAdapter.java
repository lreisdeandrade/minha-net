package br.com.net.netapp.adapter;

import android.widget.ListAdapter;

import java.util.List;

import br.com.net.netapp.service.mobilebackend.models.ScreenItem;

/**
 * Created by elourenco on 20/04/17.
 */

public interface ItemAdapter extends ListAdapter {

    void appendItems(List<ScreenItem> newItems);

    void setItems(List<ScreenItem> moreItems);
}
