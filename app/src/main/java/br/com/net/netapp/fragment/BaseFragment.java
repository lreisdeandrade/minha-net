package br.com.net.netapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.events.EventContentView;
import io.realm.Realm;

/**
 * Created by Leandro on 01/05/17.
 */

public abstract class BaseFragment extends Fragment {

    public Realm realm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        realm = Realm.getDefaultInstance();

        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();

    }

    public void trackManagerEventPush(String category, String action, String label, String itemId) {
        Event event = new EventContentView(category, action, label, itemId);
        TrackManager.getInstance().trackEvent(event);
    }

    public Realm getRealm() {
        return realm;
    }
}
