package br.com.net.netapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.net.netapp.activity.ChannelActivity_;
import br.com.net.netapp.activity.ContractsActivity_;
import br.com.net.netapp.activity.DecodersActivity;
import br.com.net.netapp.activity.DecodersActivity_;
import br.com.net.netapp.activity.LoginActivity_;
import br.com.net.netapp.activity.MainActivity_;
import br.com.net.netapp.activity.RegionSelectActivity_;
import br.com.net.netapp.activity.SinopseActivity_;
import br.com.net.netapp.activity.WebViewActivity_;

/**
 * Created by Leandro on 02/06/17.
 */

public class ActivityRoutes {
    private static ActivityRoutes instance;

    public static ActivityRoutes getInstance() {
        if (instance == null) {
            instance = new ActivityRoutes();
        }
        return instance;
    }

    public void openDecodersActivity(Context context, int requestCode) {

        DecodersActivity_.intent(context).startForResult(requestCode);

    }

    public void openRegionActivity(Context context) {

        RegionSelectActivity_.intent(context).isFromSplash(false).start();

    }

    public void openChannelActivity(Context context, int channelId) {

        ChannelActivity_.intent(context).id(channelId).start();

    }

    public void openSinopsysActivity(Context context, String idExhibition) {

        SinopseActivity_.intent(context).idExhibition(idExhibition).start();

    }

    public void openLoginActivity(Context context) {

        LoginActivity_.intent(context).start();

    }

    public void openWebViewActivity(Context context, String url, String title) {

        WebViewActivity_.intent(context).url(url).title(title).start();

    }

    public void openContractsActivity(Context context, int requestCode) {
        if (context instanceof Activity) {
            ContractsActivity_.intent(context).startForResult(requestCode);
        }
    }

    public void openLoginActivity(Context context, int requestCode) {
        Intent intent = LoginActivity_.intent(context).get();
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent, requestCode);
        } else {
            context.startActivity(intent);
        }
    }

    public void openLoginActivityFromFragment(Context context, Fragment fragment, int requesCode) {
        Intent intent = new Intent(context, LoginActivity_.class);
        fragment.startActivityForResult(intent, requesCode);
    }

}