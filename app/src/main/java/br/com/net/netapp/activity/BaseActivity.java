package br.com.net.netapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

    private Realm realm;
    private Disposable internetDisposable;
    private CoordinatorLayout coordinatorLayout;
    Snackbar snack;
    Snackbar snackWorkOrder;
    private static ArrayList<Snackbar> snackbars = new ArrayList<Snackbar>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate: CLass Name %s", this.getClass());
        realm = Realm.getDefaultInstance();

    }

    public void initCoordinator() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        if (coordinatorLayout != null) {
            Timber.d("initCoordinator: Initialized");
        } else {
            Timber.d("initCoordinator: NOT INITIALIZED");
        }
    }

    private void initIsConnectedObservable() {
        internetDisposable = AppContext.getInstance().getConnectionObservable()
                .subscribe(isConnected -> {
                    if (!isConnected) {
                        showSnackBarWithoutConnection("Sem conexão de internet");
                    } else {
                        dismissSnackBar();
                    }
                });
    }

    public void showSnackBarWithoutConnection(String messageText) {

        snack = Snackbar.make(findViewById(R.id.coordinatorLayout), messageText,
                Snackbar.LENGTH_INDEFINITE);
        View view = snack.getView();
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.color_primary));
        snack.show();

    }

    public void showSnackBarWorkOrder(WorkOrder workOrder) {

        snackWorkOrder = Snackbar.make(findViewById(R.id.coordinatorLayout), "", Snackbar.LENGTH_INDEFINITE);

        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackWorkOrder.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

        LayoutInflater mInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View snackView = mInflater.inflate(R.layout.snack_bar_vt, null);
        snackWorkOrder.setDuration(Snackbar.LENGTH_INDEFINITE);

        TextView txtHorario = (TextView) snackView.findViewById(R.id.txtHorario);
        TextView txtReminder = (TextView) snackView.findViewById(R.id.txtReminder);

        txtHorario.setText(workOrder.getVTFormatedTime());
        txtReminder.setText(workOrder.getVTExtendedTime());

        snackWorkOrder.getView().setOnClickListener(v -> {
            MyTechnicianActivity_.intent(this).start();
//            snackWorkOrder.dismiss();
        });

        layout.addView(snackView, 0);
        colorSnackBar(snackWorkOrder, this.getResources().getColor(R.color.yellow));

        snackWorkOrder.show();
        snackbars.add(snackWorkOrder);

    }

    public void showVtIfExists() {

        List<WorkOrder> workOrders = getRealm().where(WorkOrder.class).findAll();

        WorkOrder workOrder = null;
        if (workOrders != null && !workOrders.isEmpty()) {

            for (int i = workOrders.size() - 1; i >= 0; i--) {
                if (!workOrders.get(i).getScheduleDate().toString().contains("9999")) {
                    workOrder = workOrders.get(i);
                    break;
                }
            }
            if (workOrder != null) {
                showSnackBarWorkOrder(workOrder);
            }
        }

    }

    private static View getSnackBarLayout(Snackbar snackbar) {
        if (snackbar != null) {
            return snackbar.getView();
        }
        return null;
    }

    private static Snackbar colorSnackBar(Snackbar snackbar, int colorId) {
        View snackBarView = getSnackBarLayout(snackbar);
        if (snackBarView != null) {
            snackBarView.setBackgroundColor(colorId);
        }

        return snackbar;
    }

    public void dismissSnackBar() {

        if (snack != null) {
            snack.dismiss();
        }
    }

    public void dismissSnackBarWorkOrder() {

        if (this.snackbars != null && !this.snackbars.isEmpty()) {
            for (Snackbar snackbar : this.snackbars) {
                snackbar.dismiss();
            }

            this.snackbars.clear();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Connectivity.create(this).isAvailable()) {
            showSnackBarWithoutConnection("Sem conexão");
        }

        initIsConnectedObservable();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
        safelyDispose(internetDisposable);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    private void safelyDispose(Disposable... disposables) {
        for (Disposable subscription : disposables) {
            if (subscription != null && !subscription.isDisposed()) {
                subscription.dispose();
            }
        }
    }

    public Realm getRealm() {
        return realm;
    }
}
