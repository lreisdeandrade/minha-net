package br.com.net.netapp.event;

import br.com.net.netapp.service.mobilebackend.models.WorkOrder;

/**
 * Created by Leandro on 15/09/17.
 */

public class WorkorderScheduledEvent {

    protected WorkOrder workOrder;

    public WorkorderScheduledEvent(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }
}

