package br.com.net.netapp.service.mobilebackend.clients.tv.reminder;

import br.com.net.netapp.service.mobilebackend.models.Reminder;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by rafael.mendes on 08/06/17.
 */

public interface IReminderClient {
    @GET("epg/reminders")
    Call<ResponseDataCollection<Reminder>> loadReminders(@Header("x-access-token") String xAccessToken);

    @POST("epg/reminders")
    @FormUrlEncoded
    Call<ResponseData<Reminder>> createReminder(@Header("x-access-token") String xAccessToken,
                                                @Field("exhibitionId") String exhibitionId);

    //TODO: VERIFICAR TIPO DE RETORNO (ResponseBody??)
    @DELETE("epg/reminders/{id}")
    Call<ResponseBody> deleteReminder(@Header("x-access-token") String xAccessToken,
                                      @Path("id") String reminderId);

}
