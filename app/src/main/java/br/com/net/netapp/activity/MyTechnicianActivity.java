package br.com.net.netapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.manager.SessionManager;

/**
 * Created by Leandro on 14/09/17.
 */

@EActivity(R.layout.activity_my_technician)
public class MyTechnicianActivity extends BaseActivity {

    private static String TAG = "MeuTecnicoActivity";

    @ViewById
    WebView webView;

    @ViewById
    ImageView btnClose;

    @Extra("url")
    String url;

    @Bean
    SessionManager sessionManager;


    @AfterViews
    protected void setupView() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String action = extras.getString("action");
            if (action != null && action.contains("http")) {
                url = action;
            }
        }

        if (sessionManager.isActive()) {
            setupWebView();
        } else {
            ActivityRoutes.getInstance().openLoginActivity(this, Constants.LOGIN_TECHINICIAN_RESULT);
        }
    }

    protected void setupWebView() {
        Log.d(TAG, "InitWebView");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(TAG, url);
                Uri uri = Uri.parse(url);

                if ("meutecnico".equals(uri.getScheme())) {
                    String action = uri.getQueryParameter("action");
                    if ("addcalendario".equals(action)) {

                        String beginTime = uri.getQueryParameter("beginTime");
                        String endTime = uri.getQueryParameter("endTime");
                        String notes = uri.getQueryParameter("notes");
                        String title = uri.getQueryParameter("title");


                        Intent intent = new Intent(Intent.ACTION_EDIT);
                        intent.setType("vnd.android.cursor.item/event");
                        intent.putExtra("beginTime", Long.parseLong(beginTime));
                        intent.putExtra("endTime", Long.parseLong(endTime));
                        intent.putExtra("title", title);
                        intent.putExtra("description", notes);
                        startActivity(intent);

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d(TAG, "PAGE FINISHED URL: " + url);
                super.onPageFinished(webView, url);
            }

            @Override
            public void onReceivedSslError(WebView view,
                                           final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MyTechnicianActivity.this);
                builder.setMessage("Deseja continuar com o login ?");
                builder.setPositiveButton("sim",
                        (dialog, which) -> handler.proceed());
                builder.setNegativeButton("não",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                handler.cancel();
                                finish();
                                Toast.makeText(getApplicationContext(),
                                        "Login não efetuado!",
                                        Toast.LENGTH_SHORT).show();

                            }
                        });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO implementar mensagem de erro
            }
        });

        if (url != null && url.contains("http")) {
            webView.loadUrl(url);
        } else {

            url = String.format(BuildConfig.TECH_URL, sessionManager.getUser().getDocumentId());
            webView.loadUrl(String.format("%s", url));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setupWebView();
//        LocalNotificationBroadcastReceiver.setActivity(this);
//        NetworkChangeReceiver.setActivity(this);
//        RemoteNotificationBroadcastReceiver.setActivity(this);
//        LocalBroadcastManager.getInstance(this).registerReceiver(activityUtils.notificationReceiver,
//                new IntentFilter(PushListenerService.ACTION_SNS_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(activityUtils.notificationReceiver);

    }

    @Click(R.id.btn_close)
    protected void closeClicked() {
        finish();
    }
}