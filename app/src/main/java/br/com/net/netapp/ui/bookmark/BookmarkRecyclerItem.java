package br.com.net.netapp.ui.bookmark;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 27/06/17.
 */

public class BookmarkRecyclerItem extends CustomRecyclerItem<BookmarkChannel,
        BookmarkRecyclerItem, BookmarkRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new BookmarkRecyclerItem.ItemFactory();
    private Context context;

    public BookmarkRecyclerItem(Context context, BookmarkChannel item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.favorite_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bookmark_recycler_item;
    }

    @Override
    public void bindView(BookmarkRecyclerItem.ViewHolder viewHolder) {
        super.bindView(viewHolder);
        BookmarkChannel viewModel = getModel();
        if (viewModel.getExhibition().getTvShow() != null) {
            Picasso.with(context).load(viewModel.getExhibition().getTvShow().getPosterUrl()).into(viewHolder.bookmarkExhibitionLogo);


            Picasso.with(context).load(viewModel.getExhibition().getTvShow().getPosterUrl()).into(viewHolder.bookmarkExhibitionLogo, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                    viewHolder.bookmarkExhibitionLogo.setBackgroundColor(ContextCompat.getColor(context, R.color.color_primary));


                }
            });
             viewHolder.bookmarkExhibitionLogo.setBackgroundColor(ContextCompat.getColor(context, R.color.color_primary));
        }

        Picasso.with(context).load(viewModel.getLogoUrl()).into(viewHolder.bookmarkChannelLogo);

        viewHolder.gradientView.bringToFront();

        viewHolder.bookmarkChannelName.setText(viewModel.getName());
        viewHolder.bookmarkChannelNumber.setText(String.valueOf(viewModel.getNumber()));
        viewHolder.bookmarkExhibitionTitle.setText(viewModel.getExhibition().getTitle());
        viewHolder.bookmarkExhibitionDate.setText(viewModel.getExhibition().getFormattedDhInicioDhFim());

        viewHolder.bookmarkProgressbar.setProgress(viewModel.getExhibition().getProgress());

        viewHolder.containerExhibition.bringToFront();
    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<BookmarkRecyclerItem.ViewHolder> {
        public BookmarkRecyclerItem.ViewHolder create(View v) {
            return new BookmarkRecyclerItem.ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends BookmarkRecyclerItem.ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView bookmarkChannelName;
        TextView bookmarkChannelNumber;
        ImageView bookmarkChannelLogo;

        TextView bookmarkExhibitionTitle;
        TextView bookmarkExhibitionDate;

        ProgressBar bookmarkProgressbar;
        ImageView bookmarkExhibitionLogo;

        View gradientView;

        LinearLayout containerExhibition;

        public ViewHolder(View view) {
            super(view);
            this.bookmarkChannelName = (TextView) view.findViewById(R.id.bookmark_channel_name);
            this.bookmarkChannelNumber = (TextView) view.findViewById(R.id.bookmark_channel_number);

            this.bookmarkChannelLogo = (ImageView) view.findViewById(R.id.bookmark_channel_logo);

            this.bookmarkExhibitionTitle = (TextView) view.findViewById(R.id.bookmark_exhibition_title);
            this.bookmarkExhibitionDate = (TextView) view.findViewById(R.id.bookmark_exhibition_date);

            this.bookmarkProgressbar = (ProgressBar) view.findViewById(R.id.bookmark_progressbar);

            this.bookmarkExhibitionLogo = (ImageView) view.findViewById(R.id.bookmark_exhibition_logo);
            this.gradientView = view.findViewById(R.id.gradient_view);

            this.containerExhibition = (LinearLayout) view.findViewById(R.id.container_exhibition);
        }
    }
}