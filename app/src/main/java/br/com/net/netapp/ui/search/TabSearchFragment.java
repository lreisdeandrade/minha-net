package br.com.net.netapp.ui.search;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import br.com.net.netapp.R;
import br.com.net.netapp.activity.SearchActivity;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.fragment.BaseFragment;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.search.SearchClient;
import br.com.net.netapp.service.mobilebackend.models.ChannelDTO;
import br.com.net.netapp.service.mobilebackend.models.Data;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.ui.component.RxSearch;
import br.com.net.netapp.ui.component.pageAdapter.ViewPagerAdapter;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static android.view.View.VISIBLE;

@EFragment(R.layout.amx_search_view)
public class TabSearchFragment extends BaseFragment {

    @ViewById
    ViewPager viewpager;

    @ViewById
    TabLayout searchTabs;

    @ViewById
    LinearLayout tabsContainer;

    @ViewById
    TextView textViewErrorMessage;

    @ViewById
    RelativeLayout loadingComponent;

    @ViewById
    ProgressBar progressComponent;

    @ViewById
    TextView hintSearchTextView;

    @Bean
    SessionManager sessionManager;

    protected SearchClient searchClient;
    private ViewPagerAdapter viewPagerAdapter;

    ExhibitionSearchFragment exhibitionSearchFragment;
    ChannelSearchFragment channelSearchFragment;
    private ArrayList<Exhibition> exhibitions;
    private ArrayList<ChannelDTO> channels;


    @FragmentArg("testArg")
    ArrayList<String> highLightChannels;

    private Context context;
    private Observable<String> searchTrigger;
    private SearchView searchView;

    @AfterViews
    public void afterViews() {
        context = getContext();
        setHasOptionsMenu(true);
        searchClient = new SearchClient();
        init();
    }

    private void init() {
        SearchActivity activity = (SearchActivity) context;
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        searchView = activity.getSearchItem();
        exhibitions = new ArrayList<>();
        channels = new ArrayList<>();

        exhibitionSearchFragment = ExhibitionSearchFragment_.builder().items(exhibitions).build();
        channelSearchFragment = ChannelSearchFragment_.builder().channels(channels).build();
        viewPagerAdapter.addFragment(channelSearchFragment, "Canais");
        viewPagerAdapter.addFragment(exhibitionSearchFragment, "Programas");

        viewpager.setAdapter(viewPagerAdapter);
        searchTabs.setupWithViewPager(viewpager);
        searchTrigger =
                RxSearch.fromSearchView(searchView)
                        .debounce(700, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread());
        searchTrigger.subscribe(query -> {
            if (searchView.getQuery().length() != 0) {
                prepareToLoad();
                hintSearchTextView.setVisibility(View.GONE);
                textViewErrorMessage.setVisibility(View.GONE);
                progressComponent.setVisibility(VISIBLE);
                exhibitionSearchFragment.getRecycler().setNoContentText(String.format(
                        getString(R.string.search_result_empty_text), query));
                channelSearchFragment.getRecycler().setNoContentText(String.format(
                        getString(R.string.search_result_empty_text), query));
                makeSearch(query);
            }
        }, e -> {
            Timber.e(e, "init: %s", e.getMessage());
            showError(e);
        }, () -> {
        });

    }

    private void makeSearch(String query) {
        hideKeyboard();
        AppContext.getInstance().trackManagerEventPush("Busca", "Termo", query, null);
        searchClient.search(query, String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new CallBackClient() {
            @Override
            public void onSuccess(Object result) {
                if (searchTrigger != null) {
                    populateTabs((Data) result);
                }
            }

            @Override
            public void onFailure(Throwable e) {
                Timber.e(e, "onFailure: %s", e.getMessage());
                hideKeyboard();
            }
        });
    }

    private void showError(Throwable e) {

        progressComponent.setVisibility(View.INVISIBLE);
        textViewErrorMessage.setVisibility(VISIBLE);
        textViewErrorMessage.setText("Erro ao realizar busca");
    }

    private void prepareToLoad() {
        loadingComponent.setVisibility(VISIBLE);
        exhibitionSearchFragment.removeAll();
        channelSearchFragment.removeAll();
    }

    protected void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    private void populateTabs(Data data) {

        exhibitions = data.getExhibitions();
        exhibitionSearchFragment.add(exhibitions);

        channels = data.getChannels();
        channelSearchFragment.add(channels);

        loadingComponent.setVisibility(View.GONE);
        tabsContainer.setVisibility(VISIBLE);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        SearchActivity searchActivity = (SearchActivity) this.context;
        searchActivity.addSearchButton();
        searchActivity.getMenu().getItem(0).expandActionView();

    }

    @Override
    public void onDestroyView() {
        searchTrigger = null;
        super.onDestroyView();
    }
}
