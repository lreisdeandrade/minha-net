package br.com.net.netapp.service.mobilebackend.clients.tv.bookmark;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.BookmarkChannel;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 27/06/17.
 */

public class BookmarkClient extends BaseClient {

    private String TAG = "ReminderClient";
    private IBookmarkClient iFavoriteClient;

    public BookmarkClient() {
        super(BuildConfig.BASE_URL_TV);

        this.iFavoriteClient = this.retrofit.create(IBookmarkClient.class);
    }

    public void createChannelBookmark(String xAcessToken, int channelId, String cityId, final RealmCallbackClient callbackClient) {

        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("channelId", String.valueOf(channelId));
        queryStringMap.put("cityId", cityId);

        this.iFavoriteClient.createBookmark(xAcessToken, queryStringMap)
                .enqueue(new Callback<ResponseData<BookmarkChannel>>() {
                    @Override
                    public void onResponse(Call<ResponseData<BookmarkChannel>> call,
                                           Response<ResponseData<BookmarkChannel>> response) {

                        if(response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().data);
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<BookmarkChannel>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }
//
    public void deleteBookmark(String xAccessToken, int channelId, final RealmCallbackClient callbackClient) {
        this.iFavoriteClient.deleteBookmark(xAccessToken,channelId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()) {
                            //TODO: Verificar se o tratamento correto é mesmo esse,
                            // com envio de um boolean
                            callbackClient.onSuccess(true);
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }

    public void loadBookmarks(String xAcessToken , String cityId , final RealmCallbackClient callbackClient) {

        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("cityId", cityId);

        this.iFavoriteClient.loadBookmarks(xAcessToken, queryStringMap).enqueue(new Callback<ResponseDataCollection<BookmarkChannel>>() {
            @Override
            public void onResponse(Call<ResponseDataCollection<BookmarkChannel>> call,
                                   Response<ResponseDataCollection<BookmarkChannel>> response) {
                if(response.isSuccessful()) {
                    callbackClient.onSuccess(response.body().getDataInfo());
                } else {
                    ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                    callbackClient.onError(errorMessage);
                }
            }

            public void onFailure(Call<ResponseDataCollection<BookmarkChannel>> call, Throwable t) {
                ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                callbackClient.onError(errorMessage);
            }
        });
    }

}
