package br.com.net.netapp.service.mobilebackend.clients;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.helper.DateDeserializer;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseError;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import io.realm.RealmObject;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class BaseClient {

    protected Retrofit retrofit;

    public BaseClient(String urlBase) {
        this.retrofit = this.createRetrofit(urlBase);
    }


    private Retrofit createRetrofit(String urlBase) {
        HttpLoggingInterceptor interceptorLogging = new HttpLoggingInterceptor();
        interceptorLogging.setLevel(HttpLoggingInterceptor.Level.BODY);

        Interceptor interceptor = chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .addHeader("x-api-key", BuildConfig.API_KEY)
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        };

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptorLogging)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                }).registerTypeAdapter(Date.class, new DateDeserializer())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urlBase)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    protected ErrorMessage getErrorMessage(String tag, ResponseBody responseBody) {
        ErrorMessage errorMessage;

        try {
            ResponseError responseError =
                    (ResponseError) retrofit.responseBodyConverter(ResponseError.class, ResponseError.class.getAnnotations())
                            .convert(responseBody);

            if (responseError.getStatusCode() == 404) {
                errorMessage = responseError.getErrorMessage();
                errorMessage.setMessage("Conteúdo não encontrado");
            } else {
                errorMessage = BuilderErrorMessage.errorConnectToServer(tag);
            }
        } catch (IOException e) {
            Timber.e(e.getMessage());

            errorMessage = BuilderErrorMessage.errorConnectToServer(tag);
        }
        return errorMessage;
    }
}
