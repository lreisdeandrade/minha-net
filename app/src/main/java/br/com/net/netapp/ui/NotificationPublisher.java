package br.com.net.netapp.ui;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import timber.log.Timber;

/**
 * Created by gibranlyra on 12/06/17.
 */

public class NotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION = "notification";


    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        NotificationHelper.getInstance().sendNotification(notification);


        Timber.d("onReceive: ");
    }
}
