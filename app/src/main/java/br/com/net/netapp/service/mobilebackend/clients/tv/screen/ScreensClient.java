package br.com.net.netapp.service.mobilebackend.clients.tv.screen;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.Screen;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ScreensClient extends BaseClient {

    private static final String TAG = "ScreensClient";
    private IScreensClient screensClient;

    public ScreensClient() {
        super(BuildConfig.BASE_URL_TV);

        this.screensClient = this.retrofit.create(IScreensClient.class);
    }

    public void getHighLights(String cityId, final RealmCallbackClient<Screen> callbackClient) {

        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("cityId", cityId);

        screensClient.loadScreen(queryStringMap)
                .enqueue(new Callback<ResponseData<Screen>>() {
                    @Override
                    public void onResponse(Call<ResponseData<Screen>> call, Response<ResponseData<Screen>> response) {
                        // TODO: Tratar erros

                        if (response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().getData());
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<Screen>> call, Throwable t) {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setCode("app-android-load-screen");
                        errorMessage.setMessage("Erro ao conectar no servidor");
                        Timber.e(t, "onFailure: %s", t.getMessage());
                        callbackClient.onError(errorMessage);
                    }
                });
    }
}
