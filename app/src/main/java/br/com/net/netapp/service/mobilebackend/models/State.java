package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 06/09/17.
 */

@RealmClass
public class State extends RealmObject {

    @SerializedName("name")
    @Expose
    private String name;

    @PrimaryKey
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("cities")
    @Expose
    private RealmList<City> cities = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RealmList<City> getCities() {
        return cities;
    }

    public void setCities(RealmList<City> cities) {
        this.cities = cities;
    }


    public static void createDefault() {
        Realm realm = Realm.getDefaultInstance();


        realm.executeTransaction(realm1 -> {
            State state = new State();

            state.setCode("SP");
            state.setName("São Paulo");
            state.setCities(City.getRealmlistDefault());
            realm1.insertOrUpdate(state);

        });

    }

    public static RealmList<City> getRealmlistDefault() {

        Realm realm = Realm.getDefaultInstance();

        List<State> statesDefault = new ArrayList<>();
        State state = new State();

        state.setCode("SP");
        state.setName("São Paulo");
        state.setCities(City.getRealmlistDefault());
        realm.insertOrUpdate(state);

        return new RealmList<City>(statesDefault.toArray(new City[statesDefault.size()]));

    }

    @Override
    public boolean equals(Object state) {
        if (state == null) {
            return false;
        } else if (!(state instanceof State)) {
            return false;
        } else if (!this.name.equals(((State) state).getName())) {
            return false;
        }
        return true;
    }

}