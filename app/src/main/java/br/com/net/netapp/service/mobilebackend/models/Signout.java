package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Leandro on 19/09/17.
 */
public class Signout {

    @SerializedName("sessionClosed")
    @Expose
    private Boolean sessionClosed;

    public Boolean getSessionClosed() {
        return sessionClosed;
    }

    public void setSessionClosed(Boolean sessionClosed) {
        this.sessionClosed = sessionClosed;
    }

    @Override
    public String toString() {
        return "Signout{" +
                "sessionClosed=" + sessionClosed +
                '}';
    }
}