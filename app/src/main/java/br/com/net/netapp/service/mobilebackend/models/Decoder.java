package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 26/07/17.
 */

@RealmClass
public class Decoder extends RealmObject{

    @SerializedName("code")
    @Expose
    @PrimaryKey
    private String code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("isHDMax")
    @Expose
    private Boolean isHDMax;
    @SerializedName("technology")
    @Expose
    private String technology;
    @SerializedName("localization")
    @Expose
    private String localization;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsHDMax() {
        return isHDMax;
    }

    public void setIsHDMax(Boolean isHDMax) {
        this.isHDMax = isHDMax;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

}