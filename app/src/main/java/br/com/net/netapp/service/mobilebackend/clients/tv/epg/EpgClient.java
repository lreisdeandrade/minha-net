package br.com.net.netapp.service.mobilebackend.clients.tv.epg;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.Category;
import br.com.net.netapp.service.mobilebackend.models.Epg;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class EpgClient extends BaseClient {
    private String TAG = "EpgClient";
    private IEpgClient epgClient;

    public EpgClient() {
        super(BuildConfig.BASE_URL_TV);

        this.epgClient = this.retrofit.create(IEpgClient.class);
    }

    public void loadEpg(String cityId, DateTime date, final RealmCallbackClient callbackClient) {
        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("cityId", cityId);
        long dateUnix = date.getMillis() / 1000;
        queryStringMap.put("date", Long.toString(dateUnix));

        this.epgClient.loadEPG(queryStringMap)
                .enqueue(new Callback<ResponseData<Epg>>() {
                    @Override
                    public void onResponse(Call<ResponseData<Epg>> call, Response<ResponseData<Epg>> response) {
                        if(response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().data);
                        } else {
                            Timber.e("onResponse: %s", response.raw()                                                                                                               );
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<Epg>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }

    public void loadEpgChannelCategories(String cityId, final RealmCallbackClient callbackClient) {
        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("cityId", cityId);

        this.epgClient.loadEPGChannelCategories(queryStringMap)
                .enqueue(new Callback<ResponseDataCollection<Category>>() {
                    @Override
                    public void onResponse(Call<ResponseDataCollection<Category>> call, Response<ResponseDataCollection<Category>> response) {
                        if(response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().getDataInfo());
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());

                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataCollection<Category>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }
}
