package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 06/09/17.
 */

@RealmClass
public class Settings extends RealmObject {

    @SerializedName("homeUrlDefault")
    @Expose
    private String homeUrlDefault;

    @SerializedName("solrDefaultCityId")
    @Expose
    @PrimaryKey
    private Integer solrDefaultCityId;

    @Expose
    private Integer selectedCityId;

    @Expose
    private String selectedCityName;

    @Expose
    private int selectedStateId;

    @Expose
    private String selectedStateName;

    @Expose
    private String selectedUFName;


    public String getHomeUrlDefault() {
        return homeUrlDefault;
    }

    public void setHomeUrlDefault(String homeUrlDefault) {
        this.homeUrlDefault = homeUrlDefault;
    }

    public Integer getSolrDefaultCityId() {
        return solrDefaultCityId;
    }

    public void setSolrDefaultCityId(Integer solrDefaultCityId) {
        this.solrDefaultCityId = solrDefaultCityId;
    }

    public Integer getSelectedCityId() {
        return selectedCityId;
    }

    public void setSelectedCityId(Integer selectedCityId) {
        this.selectedCityId = selectedCityId;
    }

    public String getSelectedCityName() {
        return selectedCityName;
    }

    public void setSelectedCityName(String selectedCityName) {
        this.selectedCityName = selectedCityName;
    }

    public int getSelectedStateId() {
        return selectedStateId;
    }

    public void setSelectedStateId(int selectedStateId) {
        this.selectedStateId = selectedStateId;
    }

    public String getSelectedStateName() {
        return selectedStateName;
    }

    public void setSelectedStateName(String selectedStateName) {
        this.selectedStateName = selectedStateName;
    }

    public String getSelectedUFName() {
        return selectedUFName;
    }

    public void setSelectedUFName(String selectedUFName) {
        this.selectedUFName = selectedUFName;
    }
}