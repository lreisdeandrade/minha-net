package br.com.net.netapp.activity;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.event.FingerPrintAuthCanceledEvent;
import br.com.net.netapp.event.FingerPrintAuthSuccessEvent;
import br.com.net.netapp.event.WorkorderScheduledEvent;
import br.com.net.netapp.helper.FingerprintHandler;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.Profile;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.aws.AWSCallBack;
import br.com.net.netapp.service.aws.AWSManager;
import br.com.net.netapp.service.aws.AWSResponse;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.auth.AuthClient;
import br.com.net.netapp.service.mobilebackend.clients.account.contract.ContractClient;
import br.com.net.netapp.service.mobilebackend.clients.account.workorder.WorkOrderClient;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.WorkOrder;
import io.realm.RealmList;

/**
 * Created by Leandro on 24/04/17.
 */

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";

    @ViewById
    TextView txtTituloAppBar, errorLabel, forgotLabel;

    @ViewById
    Toolbar toolbar;

    @ViewById
    EditText edtPassword, edtUser;

    @ViewById
    ProgressBar loginProgressBar;

    AuthClient authClient;

    @ViewById
    Button btnLogin, btnRegister;

    @Bean
    SessionManager sessionManager;

    WorkOrderClient workOrderClient;
    ContractClient contractClient;

    @App
    AppContext appContext;
    private Profile profile;

    FingerprintHandler fingerprintHandler;

    @AfterViews
    protected void setupView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        configAppBar();

        authClient = new AuthClient();
        contractClient = new ContractClient();

        workOrderClient = new WorkOrderClient();

        fingerprintHandler = new FingerprintHandler(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            fingerprintHandler = new FingerprintHandler(this);
            if (sessionManager.getAppSettings() != null) {
                profile = getRealm().where(Profile.class).findFirst();
                if (profile != null) {
                    edtUser.setText(profile.getUserName());
                    edtPassword.setText(profile.getPassword());
                    if (fingerprintHandler.fingerPrintAvailable()) {
                        fingerprintHandler.startAuth();
                    } else {
                        saveTouchIdOption(false);
                    }
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            //Nothing
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void configAppBar() {
        setSupportActionBar(toolbar);
        txtTituloAppBar.setVisibility(View.VISIBLE);
        txtTituloAppBar.setText("Minha NET");

    }

    @Click(R.id.btn_login)
    protected void loginClicked() {
        errorLabel.setVisibility(View.INVISIBLE);
        btnRegister.setEnabled(false);
        forgotLabel.setEnabled(false);

        edtPassword.setBackgroundResource(R.drawable.edittext_shape);
        edtUser.setBackgroundResource(R.drawable.edittext_shape);

        edtUser.setEnabled(false);
        edtPassword.setEnabled(false);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (edtPassword.getText().toString().matches("") || edtUser.getText().toString().matches("")) {
            showDialogError("Preencha os campos");
        } else {

            authClient.signin(edtUser.getText().toString(), edtPassword.getText().toString(), new CallBackClient<User>() {
                @Override
                public void onSuccess(User user) {

                    sessionManager.createUserInSession(user);

                    if (fingerprintHandler.fingerPrintAvailable() && profile != null && !profile.getDocumentId().equals(user.getDocumentId())) {

                        new MaterialDialog.Builder(LoginActivity.this)
                                .title("Atenção")
                                .content("Deseja associar esse usuário com o touch id ?")
                                .positiveText(R.string.sim)
                                .negativeText(R.string.nao)
                                .onNegative((dialog, which) -> {
                                    updateAndLoadInfoUser(user);
                                })
                                .onPositive((dialog, which) -> {
                                    getRealm().executeTransaction(realm -> {
                                        realm.where(Profile.class).findAll().deleteAllFromRealm();
                                        dialog.dismiss();
                                        updateAndLoadInfoUser(user);
                                    });
                                    saveProfile(user);
                                    saveTouchIdOption(true);

                                }).show();

                    } else if (fingerprintHandler.fingerPrintAvailable() && profile == null) {
                        new MaterialDialog.Builder(LoginActivity.this)
                                .title("Atenção")
                                .content("Usar impressão digital ?")
                                .positiveText(R.string.sim)
                                .negativeText(R.string.nao)
                                .onNegative((dialog, which) -> {

                                    showDialogInfoTouchId(user);
                                    saveTouchIdOption(false);

                                })
                                .onPositive((dialog, which) -> {

                                    dialog.dismiss();

                                    saveProfile(user);
                                    saveTouchIdOption(true);
                                    updateAndLoadInfoUser(user);


                                }).show();
                    } else if (fingerprintHandler.fingerPrintAvailable() && profile != null && profile.getDocumentId().equals(user.getDocumentId())) {

                        saveProfile(user);
                        updateAndLoadInfoUser(user);

                    } else {
                        saveTouchIdOption(false);
                        updateAndLoadInfoUser(user);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    if (t.getMessage().equals("erro: usuário ou senha inválido")) {
                        showDialogError("Usuário ou senha inválidos");
                    } else if (t.getMessage().equals("erro: Ocorreu um erro")) {
                        showDialogError("Ocorreu um erro, tente novamente mais tarde.");
                    }
                }
            });

            loginProgressBar.setVisibility(View.VISIBLE);
            btnLogin.setEnabled(false);
        }
    }

    private void showDialogInfoTouchId(User user) {
        new MaterialDialog.Builder(this)
                .title("Atenção")
                .cancelable(false)
                .content("Você pode habilitar impressão digital em configurações")
                .neutralText(R.string.btn_ok)
                .onNeutral((dialog, which) -> {
                    dialog.dismiss();
                    updateAndLoadInfoUser(user);

                }).show();
    }

    private void saveTouchIdOption(Boolean option) {
        Log.d(TAG, "save touchid " + option.toString());
        getRealm().executeTransaction(realm2 -> {

            br.com.net.netapp.service.mobilebackend.models.App app =
                    realm2.where(br.com.net.netapp.service.mobilebackend.models.App.class).findFirst();

            app.setEnabledTouchId(option);

            realm2.insertOrUpdate(app);
        });

    }

    private void saveProfile(User user) {
        Profile profile = new Profile();
        profile.setDocumentId(user.getDocumentId());
        profile.setName(user.getName());
        profile.setEmail(user.getEmail());
        profile.setUserName(edtUser.getText().toString());
        profile.setPassword(edtPassword.getText().toString());

        getRealm().executeTransaction(realm -> {
            realm.insertOrUpdate(profile);
        });
    }

    private void updateAndLoadInfoUser(User user) {

        loadContracts(user);
        updateDeviceAws();
        loadWorkOrders();
        setResult(Activity.RESULT_OK);
    }

    protected void loadContracts(User user) {
        contractClient.loadContracts(sessionManager.getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                Log.d(TAG, errorMessage.toString());
                sessionManager.logout();
                showDialogError(errorMessage.getMessage());

            }

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);

                edtUser.setEnabled(true);
                edtPassword.setEnabled(true);

                if (response.isEmpty()) {
                    finish();
                } else if (response.size() > 1) {
                    ContractsActivity_.intent(LoginActivity.this).start();
                    finish();
                } else {
                    saveContract(response);
                }
            }
        });

    }

    protected void saveContract(RealmList<Contract> contracts) {

        getRealm().executeTransaction(realm1 -> {

            User user = realm1.where(User.class).findFirst();

            user.setContractSelectedCode(contracts.get(0).getCode());
            user.setContractOperationCode(contracts.get(0).getOperationCode());

            realm1.insertOrUpdate(user);

            appContext.trackManagerEventPush("configuração", "selecionar-contrato",
                    String.format("%s/%s", contracts.get(0).getOperationCode(),
                            contracts.get(0).getCode()), "");

        });
        finish();

    }

    private void loadWorkOrders() {

        WorkOrderClient workOrderClient = new WorkOrderClient();
        workOrderClient.getWorkOrders(sessionManager.getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);

                List<WorkOrder> workOrders = (List<WorkOrder>) response;
                WorkOrder workOrder = null;
                for (int i = workOrders.size() - 1; i >= 0; i--) {
                    if (!workOrders.get(i).getScheduleDate().contains("9999")) {
                        workOrder = workOrders.get(i);
                        break;
                    }
                }
                if (workOrder != null) {
                    EventBus.getDefault().post(new WorkorderScheduledEvent(workOrder));
                }
            }

            @Override
            public void onError(ErrorMessage errorMessage) {
            }
        });
    }

    private void updateDeviceAws() {
        AWSManager.getInstance().updateDeviceAWS(sessionManager.generateDeviceInfo(), new AWSCallBack<AWSResponse>() {
            @Override
            public void onSuccess(AWSResponse result) {

            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    protected void showDialogError(String message) {

        edtPassword.setBackgroundResource(R.drawable.edittext_error_shape);
        edtUser.setBackgroundResource(R.drawable.edittext_error_shape);
        errorLabel.setVisibility(View.VISIBLE);
        errorLabel.setText(message);
        edtUser.setEnabled(true);
        edtPassword.setEnabled(true);

        loginProgressBar.setVisibility(View.INVISIBLE);
        edtUser.setText("");
        edtPassword.setText("");

        btnLogin.setEnabled(true);
    }


    @Click(R.id.forgot_label)
    protected void forgotCliked() {
        ActivityRoutes.getInstance().openWebViewActivity(this, "https://servicos.netcombo.com.br/netPortalMobileWEB/index.portal?_nfpb=true&_pageLabel=mobile_net_virtua_wifi_esqueci_senha_esqueci_senha_page", "Esqueci a senha");
    }

    @Click(R.id.btn_register)
    protected void btnRegister() {
        ActivityRoutes.getInstance().openWebViewActivity(this, "http://servicos.netcombo.com.br/netPortalMobileWEB/index.portal?_nfpb=true&_pageLabel=mobile_net_virtua_wifi_cadastro_cadastro_page&wlp_mobile_net_virtua_wifi_cadastro_cadastro_page", "Faça seu Cadastro");
    }

    @Click(R.id.img_menu)
    protected void backClicked() {
        onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFingerPrintAuthSuccess(FingerPrintAuthSuccessEvent event) {
        Log.d(TAG, "onFingerPrintAuthSuccess");

        edtUser.setText(profile.getUserName());
        edtPassword.setText(profile.getPassword());
        saveTouchIdOption(true);
        loginClicked();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFingerPrintCanceled(FingerPrintAuthCanceledEvent event) {
        Log.d(TAG, "onFingerPrintCanceled");

        edtPassword.setText("");
        edtPassword.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

    }

}
