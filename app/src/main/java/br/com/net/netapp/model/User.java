package br.com.net.netapp.model;

import br.com.net.netapp.service.mobilebackend.models.Contract;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 24/04/17.
 */

@RealmClass
public class User extends RealmObject {

    @PrimaryKey
    protected String documentId;
    protected String name;
    protected String fullName;
    protected String token;
    protected String userName;
    protected String email;
    protected String contractSelectedCode;
    protected String contractOperationCode;
    protected String decoderSelectedCode;
    protected String birthDate;
    protected Boolean decoderIsHDMAX;
    protected RealmList<Contract> contract;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentID) {
        this.documentId = documentID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContractSelectedCode() {
        return contractSelectedCode;
    }

    public void setContractSelectedCode(String contractCode) {
        this.contractSelectedCode = contractCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean getDecoderIsHDMAX() {
        return decoderIsHDMAX;
    }

    public void setDecoderIsHDMAX(Boolean decoderIsHDMAX) {
        this.decoderIsHDMAX = decoderIsHDMAX;
    }

    public RealmList<Contract> getContract() {
        return contract;
    }

    public void setContract(RealmList<Contract> contract) {
        this.contract = contract;
    }

    public String getDecoderSelectedCode() {
        return decoderSelectedCode;
    }

    public void setDecoderSelectedCode(String decoderSelectedCode) {
        this.decoderSelectedCode = decoderSelectedCode;
    }

    public String getContractOperationCode() {
        return contractOperationCode;
    }

    public void setContractOperationCode(String contractOperationCode) {
        this.contractOperationCode = contractOperationCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFormattedOperationAndCodeContract() {
        if (this.getContractSelectedCode() != null) {
            return String.format("%s%s", this.getContractOperationCode(), this.getContractSelectedCode());

        } else {
            return "Selecione seu contrato";
        }
    }


    @Override
    public String toString() {
        return "User{" +
                "documentId='" + documentId + '\'' +
                ", name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                ", token='" + token + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", contractSelectedCode='" + contractSelectedCode + '\'' +
                ", contractOperationCode='" + contractOperationCode + '\'' +
                ", decoderSelectedCode='" + decoderSelectedCode + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", decoderIsHDMAX=" + decoderIsHDMAX +
                ", contract=" + contract +
                '}';
    }
}
