package br.com.net.netapp.activity;

import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.contract.ContractClient;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import br.com.net.netapp.ui.contract.ContractRecyclerItem;
import io.realm.RealmList;

/**
 * Created by Leandro on 24/07/17.
 */

@EActivity(R.layout.activity_contracts)
public class ContractsActivity extends BaseActivity implements CustomRecyclerListener<ContractRecyclerItem> {

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView txtTituloAppBar;

    @ViewById
    FrameLayout fragmentContracts;

    @Bean
    SessionManager sessionManager;

    @App
    AppContext appContext;

    ContractClient contractClient;

    private CustomRecycler<ContractRecyclerItem> recycler;
    private ArrayList<ContractRecyclerItem> recyclerItems;
    private List<Contract> contracts;


    @AfterViews
    protected void afterviews() {

        contractClient = new ContractClient();

        this.setSupportActionBar(toolbar);
        txtTituloAppBar.setVisibility(View.VISIBLE);
        txtTituloAppBar.setText("Contratos");

        recycler = new CustomRecycler<>(this, this);
        fragmentContracts.addView(recycler);
        recycler.setViewLoaded(true);
        recycler.setNoContentText(getResources().getString(R.string.empty_contracts));

        showContracts();

    }

    protected void showContracts() {
        contracts = getRealm().where(Contract.class).findAll();

        if (contracts.isEmpty()) {
            loadContracts();
        } else {
            add();
        }

    }

    protected void loadContracts() {

        contractClient.loadContracts(sessionManager.getUser().getToken(), new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {

                add();

            }

            @Override
            public void onSuccess(RealmList response) {
                super.onSuccess(response);

                contracts = response;
                add();
            }
        });

    }

    public void add() {
        recyclerItems = new ArrayList<>();

        for (Contract contract : contracts) {
            recyclerItems.add(new ContractRecyclerItem(this, contract));
        }

        if (recycler != null) {
            recycler.add(recyclerItems);

        } else {
            recycler = new CustomRecycler<>(this, this);
            recycler.add(recyclerItems);

        }
    }

    @Click(R.id.img_menu)
    protected void backClicked() {
        onBackPressed();
    }

    @Override
    public void onItemClick(View view, ContractRecyclerItem item, int position) {

        getRealm().executeTransaction(realm1 -> {
            User user = sessionManager.getUser();
            user.setContractSelectedCode(item.getModel().getCode());
            user.setContractOperationCode(item.getModel().getOperationCode());

            realm1.insertOrUpdate(user);

            appContext.trackManagerEventPush("configuração", "selecionar-contrato",
                    String.format("%s/%s", item.getModel().getOperationCode(), item.getModel().getCode()), "");
        });

        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onLongItemClick(View view, ContractRecyclerItem item, int position) {

    }
}
