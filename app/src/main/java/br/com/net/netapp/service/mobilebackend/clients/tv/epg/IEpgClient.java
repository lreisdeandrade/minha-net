package br.com.net.netapp.service.mobilebackend.clients.tv.epg;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.Category;
import br.com.net.netapp.service.mobilebackend.models.Epg;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface IEpgClient {
    @GET("epg")
    Call<ResponseData<Epg>> loadEPG(@QueryMap HashMap<String, String> queryString);

    @GET("epg/channels/categories")
    Call<ResponseDataCollection<Category>> loadEPGChannelCategories(@QueryMap HashMap<String, String> queryString);
}
