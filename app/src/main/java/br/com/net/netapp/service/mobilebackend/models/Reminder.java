package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by rafael.mendes on 08/06/17.
 */

@RealmClass
public class Reminder extends RealmObject {

    @PrimaryKey
    @SerializedName("_id")
    private String id;
    private Date date;

    private Exhibition exhibition;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Exhibition getExhibition() {
        return exhibition;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }
}