package br.com.net.netapp.ui.component.epg;

/**
 * Created by Leandro on 02/06/17.
 */

public interface SearchListener {

    void onSearchOpen();
    void onSearchClose();
}
