package br.com.net.netapp.service.mobilebackend.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Leandro on 17/05/17.
 */

@RealmClass
public class TvShow extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;
    private String description;
    private String genres;
    private String subGenres;
    private String duration;
    private String cast;
    private String director;
    private String ageRating;
    private String posterUrl;

    private String fullTitle;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getSubGenres() {
        return subGenres;
    }

    public void setSubGenres(String subGenres) {
        this.subGenres = subGenres;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getFullTitle() {
        return fullTitle;
    }

    public void setFullTitle(String fullTitle) {
        this.fullTitle = fullTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.description);
        dest.writeString(this.genres);
        dest.writeString(this.subGenres);
        dest.writeString(this.duration);
        dest.writeString(this.cast);
        dest.writeString(this.director);
        dest.writeString(this.ageRating);
        dest.writeString(this.posterUrl);
    }

    public TvShow() {
    }

    protected TvShow(Parcel in) {
        this.id = in.readLong();
        this.description = in.readString();
        this.genres = in.readString();
        this.subGenres = in.readString();
        this.duration = in.readString();
        this.cast = in.readString();
        this.director = in.readString();
        this.ageRating = in.readString();
        this.posterUrl = in.readString();
    }

    public static final Parcelable.Creator<TvShow> CREATOR = new Parcelable.Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel source) {
            return new TvShow(source);
        }

        @Override
        public TvShow[] newArray(int size) {
            return new TvShow[size];
        }
    };
}
