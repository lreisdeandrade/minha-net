package br.com.net.netapp.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

import java.util.Random;

import br.com.net.netapp.R;
import br.com.net.netapp.application.AppContext;

/**
 * Created by gibranlyra on 09/06/17.
 */

public class NotificationHelper {
    private int notificationColor;
    private static final int NOTIFICATION_ON_MS = 800;
    private static final int NOTIFICATION_OFF_MS = 400;
    private final NotificationManager notificationManager;
    private Bitmap icon;
    private int currentNotificationId;
    private NotificationCompat.Builder notificationBuilder;
    private static NotificationHelper instance;

    public static NotificationHelper getInstance() {
        if (instance == null) {
            instance = new NotificationHelper();
        }
        return instance;
    }

    private NotificationHelper() {
        notificationManager = (NotificationManager) AppContext.getInstance()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationColor = AppContext.getInstance().getResources().getColor(R.color.color_primary);
        icon = BitmapFactory.decodeResource(AppContext.getInstance().getResources(), R.mipmap.ic_launcher);
    }

    private Notification getNotification(Intent goToIntent) {
        Random random = new Random();

        PendingIntent contentIntent = PendingIntent.getActivity(AppContext.getInstance(), random.nextInt(1000),
                goToIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        return notification;
    }

    public void sendNotification(Notification notification) {
        currentNotificationId++;
        int notificationId = currentNotificationId;
        if (notificationId == Integer.MAX_VALUE - 1) {
            notificationId = 0;
        }
        notificationManager.notify(notificationId, notification);
    }

    public void clearAllNotifications() {
        if (notificationManager != null) {
            currentNotificationId = 0;
            notificationManager.cancelAll();
        }
    }

    public Notification sendSimpleNotification(Intent gotoIntent, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);;
        return getNotification(gotoIntent);
    }

    public Notification sendExpandLayoutNotification(Intent goToIntent, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);;
        return getNotification(goToIntent);
    }

    public Notification sendNotificationWithActionButton(Intent goToIntent, Intent buttonIntent, String buttonText,
                                                         @DrawableRes int drawable, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);;
        buttonIntent.setAction(buttonText);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(AppContext.getInstance(), 1, buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(drawable, buttonText, pendingIntent);
        return getNotification(goToIntent);
    }

    public Notification sendNotificationWithTwoActions(Intent goToIntent, Intent firstButton, String firstButtonText,
                                                       @DrawableRes int firstButtonDrawable, Intent secondButton,
                                                       String secondButtonText,
                                                       @DrawableRes int secondButtonDrawable, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);
        firstButton.setAction(firstButtonText);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(AppContext.getInstance(), 1, firstButton, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(firstButtonDrawable, firstButtonText, pendingIntent);
        secondButton.setAction(secondButtonText);
        PendingIntent pendingIntent2 = PendingIntent.getActivity(AppContext.getInstance(), 2, secondButton,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(secondButtonDrawable, secondButtonText, pendingIntent2);
        return getNotification(goToIntent);
    }

    public Notification sendMaxPriorityNotification(Intent goToIntent, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setPriority(Notification.PRIORITY_MAX)
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);;
        return getNotification(goToIntent);
    }

    public Notification sendMinPriorityNotification(Intent goToIntent, String title, String text) {
        notificationBuilder = new NotificationCompat.Builder(AppContext.getInstance())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setPriority(Notification.PRIORITY_MIN)
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setLights(notificationColor, NOTIFICATION_ON_MS, NOTIFICATION_OFF_MS);;
        return getNotification(goToIntent);
    }
}
