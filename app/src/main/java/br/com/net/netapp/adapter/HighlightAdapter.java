package br.com.net.netapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.ScreenItem;

/**
 * Created by elourenco on 20/04/17.
 */

public class HighlightAdapter extends ArrayAdapter<ScreenItem> implements ItemAdapter {

    private final LayoutInflater layoutInflater;


    public HighlightAdapter(Context context, List<ScreenItem> items) {
        super(context, 0, items);
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v;

        ScreenItem item = getItem(position);

        if (convertView == null) {
            v = layoutInflater.inflate(R.layout.destaque_item, parent, false);
        } else {
            v = convertView;
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.image_view);
        View viewTvShowIndicator = v.findViewById(R.id.view_banner_tvshow);
        View gradientView = v.findViewById(R.id.gradient_view);
        TextView title = (TextView) v.findViewById(R.id.tv_show_title);
        TextView date = (TextView) v.findViewById(R.id.tv_show_date);


        if (item.getHighlight().getType().equals("TvShow")) {

            gradientView.setVisibility(View.VISIBLE);
            viewTvShowIndicator.setVisibility(View.VISIBLE);

            gradientView.bringToFront();
            title.bringToFront();
            date.bringToFront();
            viewTvShowIndicator.bringToFront();

            title.setVisibility(View.VISIBLE);
            date.setVisibility(View.VISIBLE);

            title.setText(item.getHighlight().getTitle());

            String dateFormated = item.getHighlight()
                    .getDetails().getFormatedTvShowDate().substring(0,1)
                    .toUpperCase() + item.getHighlight().getDetails().getFormatedTvShowDate().substring(1);
            date.setText(dateFormated);

        }

        String urlImage = String.format("http://api-backend.clarobrasil.mobi/files/v1/image/process?source=%s", item.getHighlight().getImageUrl());
        Picasso.with(getContext()).load(urlImage).fit().into(imageView);

        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? 1 : 0;
    }

    @Override
    public void appendItems(List<ScreenItem> newItems) {
        addAll(newItems);
        notifyDataSetChanged();
    }

    @Override
    public void setItems(List<ScreenItem> moreItems) {
        clear();
        appendItems(moreItems);
    }

}