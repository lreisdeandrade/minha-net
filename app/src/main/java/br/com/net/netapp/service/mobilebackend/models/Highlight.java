package br.com.net.netapp.service.mobilebackend.models;

import java.text.SimpleDateFormat;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import timber.log.Timber;

@RealmClass
public class Highlight extends RealmObject {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EE, dd 'de' MMMM à's' HH:mm");

    private Float proportion;
    private String imageUrl;
    private Integer width;
    private HighlightDetail details;
    private String title;
    private String type;
    @PrimaryKey
    private Integer order;
    private String landingUrl;
    private String googlePlayUrl;
    private String exhibitionId;
    private String deepLink;
    private Boolean needAuthenticated;

    private RealmList<Channel> channels;

    public static SimpleDateFormat getDateFormat() {
        return DATE_FORMAT;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Float getProportion() {
        return proportion;
    }

    public void setProportion(Float proportion) {
        this.proportion = proportion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLandingUrl() {
        return landingUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public String getGooglePlayUrl() {
        return googlePlayUrl;
    }

    public void setGooglePlayUrl(String googlePlayUrl) {
        this.googlePlayUrl = googlePlayUrl;
    }

    public HighlightDetail getDetails() {
        return details;
    }

    public void setDetails(HighlightDetail details) {
        this.details = details;
    }

    public RealmList<Channel> getChannels() {
        return channels;
    }

    public void setChannels(RealmList<Channel> channels) {
        this.channels = channels;
    }

    public String getExhibitionId() {
        return exhibitionId;
    }

    public void setExhibitionId(String exhibitionId) {
        this.exhibitionId = exhibitionId;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public Boolean getNeedAuthenticated() {
        return needAuthenticated;
    }

    public void setNeedAuthenticated(Boolean needAuthenticated) {
        this.needAuthenticated = needAuthenticated;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result;
        if (obj == null) {
            Timber.d("equals: FALSE ");
            return false;
        }
        if (obj instanceof Highlight) {
            Highlight highlight = (Highlight) obj;
            Timber.d("equals: SemiTrue ");
            result = title.equals(highlight.title) && order.equals(highlight.getOrder()) && proportion.equals(highlight.getProportion());

        } else {
            Timber.d("equals: FALSE ");
            return false;
        }
        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        return result;
    }


    @Override
    public String toString() {
        return "Highlight{" +
                "proportion=" + proportion +
                ", imageUrl='" + imageUrl + '\'' +
                ", width=" + width +
                ", details=" + details +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", order=" + order +
                ", landingUrl='" + landingUrl + '\'' +
                ", googlePlayUrl='" + googlePlayUrl + '\'' +
                ", exhibitionId=" + exhibitionId +
                ", deepLink='" + deepLink + '\'' +
                ", needAuthenticated=" + needAuthenticated +
                ", channels=" + channels +
                '}';
    }
}
