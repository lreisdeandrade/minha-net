package br.com.net.netapp.ui.component.customRecycler;

import android.view.View;

/**
 * Created by Leandro on 05/06/17.
 */

public interface CustomRecyclerListener<T>   {
    void onItemClick(View view, T item, int position);
    void onLongItemClick(View view, T item, int position);
}
