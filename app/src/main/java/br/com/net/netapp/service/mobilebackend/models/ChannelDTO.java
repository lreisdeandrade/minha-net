package br.com.net.netapp.service.mobilebackend.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ChannelDTO implements Parcelable {

    private Integer id;
    private String name;
    private String categoryName;
    private Integer number;
    private String logoUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.categoryName);
        dest.writeValue(this.number);
        dest.writeString(this.logoUrl);
    }

    public ChannelDTO() {
    }

    protected ChannelDTO(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.categoryName = in.readString();
        this.number = (Integer) in.readValue(Integer.class.getClassLoader());
        this.logoUrl = in.readString();
    }

    public static final Parcelable.Creator<ChannelDTO> CREATOR = new Parcelable.Creator<ChannelDTO>() {
        @Override
        public ChannelDTO createFromParcel(Parcel source) {
            return new ChannelDTO(source);
        }

        @Override
        public ChannelDTO[] newArray(int size) {
            return new ChannelDTO[size];
        }
    };
}
