package br.com.net.netapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.Decoder;
import br.com.net.netapp.ui.component.customRecycler.CustomRecycler;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerListener;
import br.com.net.netapp.ui.decoder.DecoderRecyclerItem;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Leandro on 25/09/17.
 */

@EActivity(R.layout.activity_decoders)
public class DecodersActivity extends BaseActivity implements
        CustomRecyclerListener<DecoderRecyclerItem> {

    private static final String TAG = "DecodersActivity";

    @ViewById
    TextView contractAddress, contractSelected, labelDecoders;

    @ViewById
    CardView contractCardView;

    @ViewById
    FrameLayout decodersContainer;

    @Bean
    SessionManager sessionManager;

    private CustomRecycler<DecoderRecyclerItem> recycler;

    private ArrayList<DecoderRecyclerItem> recyclerItems;
    private User user;
    private Contract contract;


    @AfterViews
    protected void afterViews() {

        recycler = new CustomRecycler<>(this, this);
        decodersContainer.addView(recycler);
        recycler.setViewLoaded(true);
        recycler.setNoContentText(getResources().getString(R.string.empty_decoders));

    }

    protected void configContractSelected() {

        user = getRealm().where(User.class).findFirst();

        contract = getRealm().where(Contract.class).equalTo("code", user.getContractSelectedCode()).findFirst();

        contractAddress.setText(contract.getAddress().getFormattedContractAddress());
        contractSelected.setText(contract.getCode());

        configDecodersByContract();
    }

    protected void configDecodersByContract() {

        Contract contract = getRealm().where(Contract.class).equalTo("code", user.getContractSelectedCode()).findFirst();

        add(contract);
    }

    public void add(Contract contract) {
        recyclerItems = new ArrayList<>();

        for (Decoder decoder : contract.getDecoders()) {
            recyclerItems.add(new DecoderRecyclerItem(this, decoder));
        }

        if (recyclerItems.isEmpty()) {
            labelDecoders.setVisibility(View.GONE);
        } else {
            labelDecoders.setVisibility(View.VISIBLE);

        }
        if (recycler != null) {
            recycler.add(recyclerItems);

        } else {
            recycler = new CustomRecycler<>(this, this);
            recycler.add(recyclerItems);
        }
    }

    @Click(R.id.contract_card_view)
    protected void contractCardClicked() {
        ActivityRoutes.getInstance().openContractsActivity(this, 1);
    }

    @Override
    public void onResume() {
        super.onResume();
        configContractSelected();
    }

    @Override
    public void onItemClick(View view, DecoderRecyclerItem item, int position) {

//        getRealm().executeTransaction(realm -> {
//            user.setDecoderSelectedCode(item.getModel().getCode());
//            user.setDecoderIsHDMAX(item.getModel().getIsHDMax());
//            realm.insertOrUpdate(user);
//        });

        Intent intent = new Intent();
        intent.putExtra("decoderCode", item.getModel().getCode());
        setResult(Activity.RESULT_OK, intent);

        finish();
    }

    @Override
    public void onLongItemClick(View view, DecoderRecyclerItem item, int position) {
        Log.d(TAG, "longclick");

    }

    @Click(R.id.img_menu)
    protected void backClicked() {
        onBackPressed();
    }

}
