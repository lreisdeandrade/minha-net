package br.com.net.netapp.service.mobilebackend.models;

/**
 * Created by Leandro on 26/07/17.
 */

public class InvoiceSubmission {

    public String id;
    public String format;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "InvoiceSubmission{" +
                "id='" + id + '\'' +
                ", format='" + format + '\'' +
                '}';
    }
}
