package br.com.net.netapp.fragment;

import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.activity.ContractsActivity_;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.helper.FingerprintHandler;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.model.Profile;
import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.clients.CallBackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.auth.AuthClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.State;
import io.realm.RealmResults;

/**
 * Created by Leandro on 31/08/17.
 */

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends BaseFragment implements Switch.OnCheckedChangeListener {

    private static final String TAG = "SettingsFragment";
    @ViewById
    CardView regionCardView, contractCardView, fingerprintCardview;

    @ViewById
    TextView citySelected, contractSelected, contractAddress, versionNumber;

    @ViewById
    ImageView contractIcArrowIndicator;

    @ViewById
    Switch switchFingerprint;

    @Bean
    SessionManager sessionManager;

    @ViewById
    RelativeLayout loading;

    AuthClient authClient;
    private User user;

    private FingerprintHandler fingerprintHandler;

    @AfterViews
    void afterViews() {

        setupAppVersion();

        authClient = new AuthClient();
        fingerprintHandler = new FingerprintHandler(getContext());

    }

    private void setupAppVersion() {
        versionNumber.setText(BuildConfig.VERSION_NAME);
    }

    private void confirmUserPassword() {
        new MaterialDialog.Builder(getActivity())
                .title(user.getFullName())
                .cancelable(false)
                .content("Confirme sua senha: ")
                .inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
                .input("Digite sua senha", null, (dialog, input) -> {

                }).onPositive((dialog, which) ->
                makeLogin(user.getUserName(), dialog.getInputEditText().getText().toString())).show();
    }

    private void makeLogin(String userName, String password) {
        loading.setVisibility(View.VISIBLE);
        authClient.signin(userName, password, new CallBackClient<User>() {
            @Override
            public void onSuccess(User result) {
                loading.setVisibility(View.GONE);

                getRealm().executeTransaction(realm -> {
                    User user = realm.where(User.class).findFirst();
                    Profile profile = new Profile();

                    result.setContractOperationCode(user.getContractOperationCode());
                    result.setContractSelectedCode(user.getContractSelectedCode());
                    realm.insertOrUpdate(result);

                    profile.setDocumentId(user.getDocumentId());
                    profile.setName(user.getName());
                    profile.setEmail(user.getEmail());
                    profile.setUserName(user.getUserName());
                    profile.setPassword(password);
                    realm.insertOrUpdate(profile);

                    br.com.net.netapp.service.mobilebackend.models.App app =
                            realm.where(br.com.net.netapp.service.mobilebackend.models.App.class).findFirst();
                    app.setEnabledTouchId(true);
                    realm.insertOrUpdate(app);


                });
            }

            @Override
            public void onFailure(Throwable t) {
                loading.setVisibility(View.GONE);
                new MaterialDialog.Builder(getActivity())
                        .title("Atenção")
                        .content("Usuário ou senha inválidos")
                        .cancelable(false)
                        .neutralText(R.string.btn_ok)
                        .onNeutral((dialog, which) -> {
                            switchFingerprint.setChecked(false);
                            dialog.dismiss();
                        }).show();

            }
        });
    }

    public void onLogoutDismissWorkOrder(UserSignoutEvent event) {

    }


    protected void removeProfileFromRealm() {
        getRealm().executeTransaction(realm1 -> {
            RealmResults<Profile> profile = realm1.where(Profile.class).findAll();
            profile.deleteAllFromRealm();
        });

        getRealm().executeTransaction(realm12 -> {
            App app = realm12.where(App.class).findFirst();
            app.setEnabledTouchId(false);
            realm12.insertOrUpdate(app);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        user = getRealm().where(User.class).findFirst();

        configStateAndCity();
        configContractAndDecoder();


        configFingerPrint();

    }

    protected void configFingerPrint() {
        if (sessionManager.isActive() && fingerprintHandler.fingerPrintAvailable()) {
            switchFingerprint.setOnCheckedChangeListener(null);
            switchFingerprint.setChecked(sessionManager.getAppSettings().getEnabledTouchId());
            switchFingerprint.setOnCheckedChangeListener(this);
            fingerprintCardview.setVisibility(View.VISIBLE);
        } else {
            fingerprintCardview.setVisibility(View.GONE);
        }
    }

    protected void configContractAndDecoder() {
        RealmResults<Contract> contracts = getRealm().where(Contract.class).findAll();
        if (contracts.size() > 1) {
            contractCardView.setClickable(true);
            contractIcArrowIndicator.setVisibility(View.VISIBLE);

        } else if (contracts.size() == 0) {
            contractCardView.setClickable(true);
            contractIcArrowIndicator.setVisibility(View.VISIBLE);
            contractSelected.setText("Selecione seu contrato");
        } else {
            contractCardView.setClickable(false);
            contractIcArrowIndicator.setVisibility(View.INVISIBLE);
        }

        User user = getRealm().where(User.class).findFirst();
        Contract contract = getRealm().where(Contract.class).findFirst();

        if (user != null) {
            if (contract != null) {
                contractSelected.setText(sessionManager.getContractSelected(user.getContractSelectedCode()).getFormattedOperationAndCodeContract());
                contractAddress.setText(sessionManager.getContractSelected(user.getContractSelectedCode()).getAddress().getFormattedContractAddress());
            }
            contractCardView.setVisibility(View.VISIBLE);

        } else {
            contractCardView.setVisibility(View.GONE);

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLogout(UserSignoutEvent event) {
        Log.d(TAG, "userLogoutInEvent");
        contractCardView.setVisibility(View.GONE);
        fingerprintCardview.setVisibility(View.GONE);

    }

    protected void configStateAndCity() {
        App app = getRealm().where(App.class).findFirst();

        State state = getRealm().where(State.class).equalTo("name", app.getSettings().getSelectedStateName()).findFirst();

        citySelected.setText(String.format("%s - %s ", app.getSettings().getSelectedCityName(), state.getCode()));
    }


    @Click(R.id.region_card_view)
    protected void cardRegionClicked() {
        ActivityRoutes.getInstance().openRegionActivity(getActivity());
    }

    @Click(R.id.contract_card_view)
    protected void cardContractClicked() {

        ContractsActivity_.intent(this).start();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            confirmUserPassword();
        } else {
            removeProfileFromRealm();
        }
    }
}