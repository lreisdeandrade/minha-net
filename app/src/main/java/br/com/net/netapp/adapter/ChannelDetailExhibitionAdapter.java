package br.com.net.netapp.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.ui.component.item.ExhibitionItemView;
import br.com.net.netapp.ui.component.item.ExhibitionItemView_;
import io.realm.RealmQuery;

/**
 * Created by thiagomagalhaes on 19/05/17.
 */

@EBean
public class ChannelDetailExhibitionAdapter extends RecyclerViewAdapterBase<Exhibition, ExhibitionItemView> {

    @RootContext
    Context context;

    public void init(RealmQuery<Exhibition> query) {

        items = query.findAllSorted("startDate");
    }

    @Override
    protected ExhibitionItemView onCreateItemView(ViewGroup parent, int viewType) {

        return ExhibitionItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ExhibitionItemView> holder, int position) {

        holder.getView().bind(items.get(position));
        getCurrentItemPosition();

    }

    public int getCurrentItemPosition(){

        int index = 0;

        for (Exhibition exhition: items) {

            if(exhition.isRunning()){

                index = items.indexOf(exhition);
            }
        }

        return index;
    }

}
