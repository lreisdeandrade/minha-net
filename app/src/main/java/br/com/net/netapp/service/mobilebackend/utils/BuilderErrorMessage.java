package br.com.net.netapp.service.mobilebackend.utils;

import android.util.Log;

import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import timber.log.Timber;

import static timber.log.Timber.e;

/**
 * Created by jonatas.saraiva on 09/05/17.
 */

public class BuilderErrorMessage {
    public static ErrorMessage fromRequest(String tag, Throwable t) {
        Timber.e(t.getMessage());

        return BuilderErrorMessage.errorConnectToServer(tag);
    }

    public static ErrorMessage fromSaveInRealm(String tag, Throwable t) {
        Timber.e(t.getMessage());

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(tag);
        errorMessage.setMessage("Erro ao salvar os dados");

        return errorMessage;
    }

    public static ErrorMessage errorConnectToServer(String tag) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(tag);
        errorMessage.setMessage("Erro ao conectar no servidor");

        return errorMessage;
    }
}
