package br.com.net.netapp.service.mobilebackend.clients.app;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.models.State;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Leandro on 06/09/17.
 */

public interface IAppClient {

    @GET("apps")
    Call<ResponseData<App>> loadAppSettings();


}