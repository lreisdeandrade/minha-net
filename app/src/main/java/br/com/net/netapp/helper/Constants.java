package br.com.net.netapp.helper;

/**
 * Created by Leandro on 07/07/17.
 */

public class Constants {

    public static final Integer MAX_RETRY_ATTEMPS = 3;

    public static final int LOGIN_REMINDER_RESULT = 1;
    public static final int LOGIN_RECORD_RESULT = 2;
    public static final int LOGIN_BOOKMARK_RESULT = 3;

    public static final int RECORD_RESULT = 3;
    public static final int LOGIN_RESULT = 5;

    public static final int LOGIN_TECHINICIAN_RESULT = 4;

    public static final String DISPLACEMENT = "DISPLACEMENT";
    public static final String CREATE = "CREATE";
    public static final String TECHNICAL_FEEDBACK = "technical-feedback";
    public static final String TECHNICAL_CHAT = "technical-chat";
    public static final String TECHNICAL_CANCEL = "technical-cancel";
    public static final String TECHNICAL_RESCHEDULING = "technical-rescheduling";
    public static final String TECHNICAL_FINISH = "technical-finish";


}
