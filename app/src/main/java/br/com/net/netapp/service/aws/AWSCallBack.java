package br.com.net.netapp.service.aws;

/**
 * Created by leandro on 10/01/17.
 */

public interface AWSCallBack<T> {

    void onSuccess(T result);

    void onFailure(Throwable t);

}


