package br.com.net.netapp.ui.component.epg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Scroller;

import com.google.common.collect.Maps;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import br.com.net.netapp.R;
import br.com.net.netapp.ui.component.epg.domain.EPGEvent;
import br.com.net.netapp.ui.component.epg.misc.EPGUtil;
import timber.log.Timber;

/**
 * Created by Leandro on 04/05/17.
 */

public class EpgView extends ViewGroup {

    public final String TAG = getClass().getSimpleName();

    public static int DAYS_BACK_MILLIS;
    public static int DAYS_FORWARD_MILLIS;     // 24 hours
    public static final int TIME_LABEL_SPACING_MILLIS = 30 * 60 * 1000;        // 30 minutes

    public int HOURS_IN_VIEWPORT_MILLIS;

    private final Rect clipRect;
    private final Rect drawingRect;
    private final Rect measuringRect;
    private final Paint paint;
    private final Scroller scroller;
    private final GestureDetector gestureDetector;

    private final int channelLayoutMargin;
    private final int channelLayoutPadding;
    private final int channelLayoutHeight;
    private final int channelLayoutWidth;
    private final int channelLayoutBackground;
    private final int eventLayoutBackground;
    private final int eventLayoutBackgroundCurrent;
    private final int eventLayoutTextColor;
    private final int eventLayoutTextSize;
    private final int timeBarLineWidth;
    private final int timeBarLineColor;
    private final int timeBarHeight;
    private final int timeDayIndicatorSize;
    private final int timeBarDayindicatorBackground;
    private final int timeBarTextSize;
    private final int eventLayoutTextHorarioSize;
    private final int timeDayIndicatorNameSize;

    private final int resetButtonSize;
    private final int resetButtonMargin;
    private final Bitmap resetButtonIcon;

    private final int epgBackground;
    private final Map<String, Bitmap> channelImageCache;
    private final Map<String, Target> channelImageTargetCache;

    private EPGClickListener clickListener;
    private int maxHorizontalScroll;
    private int maxVerticalScroll;
    private long millisPerPixel;
    private long timeOffset;
    private long timeLowerBoundary;
    private long timeUpperBoundary;

    private DateTime baseLineDate ;


    private EPGData epgData = null;

    public EpgView(Context context) {
        this(context, null);
    }

    public EpgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setWillNotDraw(false);

        baseLineDate = LocalDateTime.now().toDateTime() ;

        eventLayoutTextHorarioSize = getResources().getDimensionPixelSize(R.dimen.epg_event_layout_horario_text);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            HOURS_IN_VIEWPORT_MILLIS = 2 * 60 * 60 * 1000;
            // do something
        } else {
            HOURS_IN_VIEWPORT_MILLIS = 1 * 60 * 60 * 1000;
            // do something else
        }

        resetBoundaries();

        drawingRect = new Rect();
        clipRect = new Rect();
        measuringRect = new Rect();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        gestureDetector = new GestureDetector(context, new OnGestureListener());
        channelImageCache = Maps.newHashMap();
        channelImageTargetCache = Maps.newHashMap();

        // Adding some friction that makes the epg less flappy.
        scroller = new Scroller(context);
//        scroller.setFriction(0.1f);

        scroller.setFriction(0.02f);

        epgBackground = getResources().getColor(R.color.epg_background);

        channelLayoutMargin = getResources().getDimensionPixelSize(R.dimen.epg_channel_layout_margin);
        channelLayoutPadding = getResources().getDimensionPixelSize(R.dimen.epg_channel_layout_padding);
        channelLayoutHeight = getResources().getDimensionPixelSize(R.dimen.epg_channel_layout_height);
        channelLayoutWidth = getResources().getDimensionPixelSize(R.dimen.epg_channel_layout_width);
        channelLayoutBackground = getResources().getColor(R.color.epg_channel_layout_background);

        eventLayoutBackground = getResources().getColor(R.color.epg_event_layout_background);
        eventLayoutBackgroundCurrent = getResources().getColor(R.color.epg_item_current);
        eventLayoutTextColor = getResources().getColor(R.color.epg_event_layout_text);
        eventLayoutTextSize = getResources().getDimensionPixelSize(R.dimen.epg_event_layout_text);

        timeBarHeight = getResources().getDimensionPixelSize(R.dimen.epg_time_bar_height);
        timeBarTextSize = getResources().getDimensionPixelSize(R.dimen.epg_time_bar_text);
        timeBarLineWidth = getResources().getDimensionPixelSize(R.dimen.epg_time_bar_line_width);
        timeBarLineColor = getResources().getColor(R.color.epg_time_bar);
        timeDayIndicatorSize = getResources().getDimensionPixelSize(R.dimen.epg_day_indicator_size);
        timeBarDayindicatorBackground = getResources().getColor(R.color.color_primary);
        timeDayIndicatorNameSize = getResources().getDimensionPixelSize(R.dimen.epg_day_indicator_name_size);


        resetButtonSize = getResources().getDimensionPixelSize(R.dimen.epg_reset_button_size);
        resetButtonMargin = getResources().getDimensionPixelSize(R.dimen.epg_reset_button_margin);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.outWidth = resetButtonSize;
        options.outHeight = resetButtonSize;
        resetButtonIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_now, options);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (epgData != null && epgData.hasData()) {
            timeLowerBoundary = getTimeFrom(getScrollX());
            timeUpperBoundary = getTimeFrom(getScrollX() + getWidth());

            Rect drawingRect = this.drawingRect;
            drawingRect.left = getScrollX();
            drawingRect.top = getScrollY();
            drawingRect.right = drawingRect.left + getWidth();
            drawingRect.bottom = drawingRect.top + getHeight();

            drawChannelListItems(canvas, drawingRect);
            drawEvents(canvas, drawingRect);
            drawTimebar(canvas, drawingRect);
            drawTimeLine(canvas, drawingRect);
            drawResetButton(canvas, drawingRect);

            // If scroller is scrolling/animating do scroll. This applies when doing a fling.
            if (scroller.computeScrollOffset()) {
                scrollTo(scroller.getCurrX(), scroller.getCurrY());

            }
        }
    }

    private DateTime startOfDay(DateTime baseDate) {
        Calendar startOfDay = Calendar.getInstance();
        startOfDay.setTime(baseDate.toDate());
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.SECOND, 0);
        startOfDay.set(Calendar.MILLISECOND, 0);
        return new DateTime(startOfDay.getTime()) ;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        recalculateAndRedraw(false,false);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    private void drawResetButton(Canvas canvas, Rect drawingRect) {
        // Show button when scrolled 1/3 of screen width from current time
        final long threshold = getWidth() / 3;
        if ((Math.abs(getXPositionStart() - getScrollX()) > threshold) || this.baseLineDate.getMillis() > (new DateTime()).getMillis()) {
            drawingRect = calculateResetButtonHitArea();
            paint.setColor(getResources().getColor(R.color.yellow));
            canvas.drawCircle(drawingRect.right - (resetButtonSize / 2),
                    drawingRect.bottom - (resetButtonSize / 2),
                    Math.min(drawingRect.width(), drawingRect.height()) / 2,
                    paint);

            drawingRect.left += resetButtonMargin;
            drawingRect.right -= resetButtonMargin;
            drawingRect.top += resetButtonMargin;
            drawingRect.bottom -= resetButtonMargin;
            canvas.drawBitmap(resetButtonIcon, null, drawingRect, paint);
        }
    }

    private void drawTimebarBottomStroke(Canvas canvas, Rect drawingRect) {
        drawingRect.left = getScrollX();
        drawingRect.top = getScrollY() + timeBarHeight;
        drawingRect.right = drawingRect.left + getWidth();
        drawingRect.bottom = drawingRect.top + channelLayoutMargin;

        // Bottom stroke
        paint.setColor(epgBackground);
        canvas.drawRect(drawingRect, paint);
    }

    private void drawTimebar(Canvas canvas, Rect drawingRect) {
        drawingRect.left = getScrollX();
        drawingRect.top = getScrollY();
        drawingRect.right = drawingRect.left + getWidth();
        drawingRect.bottom = drawingRect.top + timeBarHeight;

        clipRect.left = getScrollX() - channelLayoutWidth - channelLayoutMargin;
        clipRect.top = getScrollY();
        clipRect.right = getScrollX() + getWidth();
        clipRect.bottom = clipRect.top + timeBarHeight;

        canvas.save();
        canvas.clipRect(clipRect);

        // Background
        paint.setColor(getResources().getColor(R.color.color_primary));
        canvas.drawRect(drawingRect, paint);

        // Time stamps
        paint.setColor(getResources().getColor(R.color.white));
        paint.setTextSize(timeBarTextSize);

        for (int i = 0; i < (HOURS_IN_VIEWPORT_MILLIS / TIME_LABEL_SPACING_MILLIS) + 1; i++) {
            // Get time and round to nearest half hour
            final long time = TIME_LABEL_SPACING_MILLIS *
                    (((timeLowerBoundary + (TIME_LABEL_SPACING_MILLIS * i)) +
                            (TIME_LABEL_SPACING_MILLIS / 2)) / TIME_LABEL_SPACING_MILLIS);

            canvas.drawText(EPGUtil.getShortTime(time),
                    getXFrom(time),
                    drawingRect.top + (((drawingRect.bottom - drawingRect.top) / 2) + (timeBarTextSize / 2)), paint);
        }

        canvas.restore();

       // drawTimebarDayIndicator(canvas, drawingRect);
        drawTimebarBottomStroke(canvas, drawingRect);
    }

    private void drawTimebarDayIndicator(Canvas canvas, Rect drawingRect) {
        drawingRect.left = getScrollX();
        drawingRect.top = getScrollY();
        drawingRect.right = drawingRect.left + channelLayoutWidth + channelLayoutMargin;
        drawingRect.bottom = drawingRect.top + timeBarHeight;

        // Background
        paint.setColor(timeBarDayindicatorBackground);
        canvas.drawRect(drawingRect, paint);

        // Text
        paint.setColor(getResources().getColor(R.color.white));
        paint.setTextSize(timeDayIndicatorNameSize);
        paint.setTextAlign(Paint.Align.CENTER);

        canvas.drawText(EPGUtil.getWeekdayName(timeLowerBoundary),
                drawingRect.left + ((drawingRect.right - drawingRect.left) / 2),
                drawingRect.top + (((drawingRect.bottom - drawingRect.top) / 2) - (timeBarTextSize /2)), paint);

        paint.setTextSize(timeDayIndicatorSize);

        paint.setColor(getResources().getColor(R.color.white));
        canvas.drawText(EPGUtil.getDayInMonth(timeLowerBoundary),
                drawingRect.left + ((drawingRect.right - drawingRect.left) / 2),
                drawingRect.top + (((drawingRect.bottom - drawingRect.top) / 2) + (timeDayIndicatorSize /2 + 10)), paint);

        paint.setTextAlign(Paint.Align.LEFT);
    }

    private void drawTimeLine(Canvas canvas, Rect drawingRect) {
        long now = System.currentTimeMillis();

        if (shouldDrawTimeLine(now)) {
            drawingRect.left = getXFrom(now);
            drawingRect.top = getScrollY() + timeBarHeight;
            drawingRect.right = drawingRect.left + timeBarLineWidth;
            drawingRect.bottom = drawingRect.top + getHeight();

            paint.setColor(getResources().getColor(R.color.epg_line_current));
            canvas.drawRect(drawingRect, paint);
        }

    }

    private void drawEvents(Canvas canvas, Rect drawingRect) {
        final int firstPos = getFirstVisibleChannelPosition();
        final int lastPos = getLastVisibleChannelPosition();

        for (int pos = firstPos; pos <= lastPos; pos++) {

            // Set clip rectangle
            clipRect.left = getScrollX() + channelLayoutWidth + channelLayoutMargin;
            clipRect.top = getTopFrom(pos);
            clipRect.right = getScrollX() + getWidth();
            clipRect.bottom = clipRect.top + channelLayoutHeight;

            canvas.save();
            canvas.clipRect(clipRect);

            // Draw each event
            boolean foundFirst = false;

            List<EPGEvent> epgEvents = epgData.getEvents(pos);

            for (EPGEvent event : epgEvents) {
                if (isEventVisible(event.getStart(), event.getEnd())) {
                    drawEvent(canvas, pos, event, drawingRect);
                    foundFirst = true;
                } else if (foundFirst) {
                    break;
                }
            }

            canvas.restore();
        }

    }

    private void drawEvent(final Canvas canvas, final int channelPosition, final EPGEvent event, final Rect drawingRect) {

        setEventDrawingRectangle(channelPosition, event.getStart(), event.getEnd(), drawingRect);

        // Background
        paint.setColor(event.isCurrent() ? eventLayoutBackgroundCurrent : eventLayoutBackground);
        canvas.drawRect(drawingRect, paint);

        // Add left and right inner padding
        drawingRect.left += channelLayoutPadding;
        drawingRect.right -= channelLayoutPadding;

        // Text
        paint.setColor(eventLayoutTextColor);
        paint.setTextSize(eventLayoutTextSize);

        // Move drawing.top so text will be centered (text is drawn bottom>up)
        paint.getTextBounds(event.getTitle(), 0, event.getTitle().length(), measuringRect);
        drawingRect.top += (((drawingRect.bottom - drawingRect.top) / 2) - (measuringRect.height()/2));
        drawingRect.left += 10;

        String title = event.getTitle();
        title = title.substring(0, paint.breakText(title, true, drawingRect.right - drawingRect.left, null));

        canvas.drawText(title, drawingRect.left, drawingRect.top, paint);

        paint.setTextSize(eventLayoutTextHorarioSize);
        String hour = event.getHour();
        hour = hour.substring(0, paint.breakText(hour, true, drawingRect.right - drawingRect.left, null));
        canvas.drawText(hour, drawingRect.left, drawingRect.bottom - measuringRect.height(), paint);

    }

    private void setEventDrawingRectangle(final int channelPosition, final long start, final long end, final Rect drawingRect) {
        drawingRect.left = getXFrom(start);
        drawingRect.top = getTopFrom(channelPosition);
        drawingRect.right = getXFrom(end) - channelLayoutMargin;
        drawingRect.bottom = drawingRect.top + channelLayoutHeight;
    }

    private void drawChannelListItems(Canvas canvas, Rect drawingRect) {
        // Background
        measuringRect.left = getScrollX();
        measuringRect.top = getScrollY();
        measuringRect.right = drawingRect.left + channelLayoutWidth;
        measuringRect.bottom = measuringRect.top + getHeight();

        paint.setColor(channelLayoutBackground);
        canvas.drawRect(measuringRect, paint);

        final int firstPos = getFirstVisibleChannelPosition();
        final int lastPos = getLastVisibleChannelPosition();

        for (int pos = firstPos; pos <= lastPos; pos++) {
            drawChannelItem(canvas, pos, drawingRect);
        }
    }

    private void drawChannelItem(final Canvas canvas, int position, Rect drawingRect) {
        drawingRect.left = getScrollX();
        drawingRect.top = getTopFrom(position);
        drawingRect.right = drawingRect.left + channelLayoutWidth;
        drawingRect.bottom = drawingRect.top + channelLayoutHeight;

//        Paint paint = new Paint();
//        paint.setColor(Color.BLACK);
//        canvas.drawLine(0, 0, 20, 20, paint);


        // Loading channel image into target for
        final String imageURL = epgData.getChannel(position).getSolrCanal().getLogoUrl();

        if (channelImageCache.containsKey(imageURL)) {
            Bitmap image = channelImageCache.get(imageURL);
            drawingRect = getDrawingRectForChannelImage(drawingRect, image);
            canvas.drawBitmap(image, null, drawingRect, null);
        } else {
            final int smallestSide = Math.min(channelLayoutHeight, channelLayoutWidth);

            if (!channelImageTargetCache.containsKey(imageURL)) {
                channelImageTargetCache.put(imageURL, new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        channelImageCache.put(imageURL, bitmap);
                        redraw();
                        channelImageTargetCache.remove(imageURL);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

                EPGUtil.loadImageInto(getContext(), imageURL, smallestSide, smallestSide, channelImageTargetCache.get(imageURL));
            }

        }
    }

    private Rect getDrawingRectForChannelImage(Rect drawingRect, Bitmap image) {
        drawingRect.left += channelLayoutPadding;
        drawingRect.top += channelLayoutPadding;
        drawingRect.right -= channelLayoutPadding;
        drawingRect.bottom -= channelLayoutPadding;

        final int imageWidth = image.getWidth();
        final int imageHeight = image.getHeight();
        final float imageRatio = imageHeight / (float) imageWidth;

        final int rectWidth = drawingRect.right - drawingRect.left;
        final int rectHeight = drawingRect.bottom - drawingRect.top;

        // Keep aspect ratio.
        if (imageWidth > imageHeight) {
            final int padding = (int) (rectHeight - (rectWidth * imageRatio)) / 2;
            drawingRect.top += padding;
            drawingRect.bottom -= padding;
        } else if (imageWidth <= imageHeight) {
            final int padding = (int) (rectWidth - (rectHeight / imageRatio)) / 2;
            drawingRect.left += padding;
            drawingRect.right -= padding;
        }

        return drawingRect;
    }

    private boolean shouldDrawTimeLine(long now) {
        return now >= timeLowerBoundary && now < timeUpperBoundary;
    }

    private boolean isEventVisible(final long start, final long end) {
        return (start >= timeLowerBoundary && start <= timeUpperBoundary)
                || (end >= timeLowerBoundary && end <= timeUpperBoundary)
                || (start <= timeLowerBoundary && end >= timeUpperBoundary);
    }

    private long calculatedBaseLine() {
        return this.startOfDay(this.baseLineDate).getMillis() ;
    }

    private int getFirstVisibleChannelPosition() {
        final int y = getScrollY();

        int position = (y - channelLayoutMargin - timeBarHeight)
                / (channelLayoutHeight + channelLayoutMargin);

        if (position < 0) {
            position = 0;
        }
        return position;
    }

    private int getLastVisibleChannelPosition() {
        final int y = getScrollY();
        final int totalChannelCount = epgData.getChannelCount();
        final int screenHeight = getHeight();
        int position = (y + screenHeight + timeBarHeight - channelLayoutMargin)
                / (channelLayoutHeight + channelLayoutMargin);

        if (position > totalChannelCount - 1) {
            position = totalChannelCount - 1;
        }

        // Add one extra row if we don't fill screen with current..
        return (y + screenHeight) > (position * channelLayoutHeight) && position < totalChannelCount - 1 ? position + 1 : position;
    }

    private void calculateMaxHorizontalScroll() {
        maxHorizontalScroll = (int) ((DAYS_BACK_MILLIS + DAYS_FORWARD_MILLIS - HOURS_IN_VIEWPORT_MILLIS) / millisPerPixel);
    }

    private void calculateMaxVerticalScroll() {
        final int maxVerticalScroll = getTopFrom(epgData.getChannelCount() - 1) + channelLayoutHeight;
        this.maxVerticalScroll = maxVerticalScroll < getHeight() ? 0 : maxVerticalScroll - getHeight();
    }

    private int getXFrom(long time) {
        return (int) ((time - timeOffset) / millisPerPixel) + channelLayoutMargin
                + channelLayoutWidth + channelLayoutMargin;
    }

    private int getTopFrom(int position) {
        int y = position * (channelLayoutHeight + channelLayoutMargin)
                + channelLayoutMargin + timeBarHeight;
        return y;
    }

    private long getTimeFrom(int x) {
        return (x * millisPerPixel) + timeOffset;
    }

    private long calculateMillisPerPixel() {
        return HOURS_IN_VIEWPORT_MILLIS / (getResources().getDisplayMetrics().widthPixels - channelLayoutWidth - channelLayoutMargin);
    }

    private int getXPositionStart() {
        return getXFrom(this.baseLineDate.getMillis() - (HOURS_IN_VIEWPORT_MILLIS / 2));
    }

    private void resetBoundaries() {
        DateTime todayStart = new DateTime().withTimeAtStartOfDay();
        DateTime todayEnd = new DateTime().millisOfDay().withMaximumValue();

        DateTime now = new DateTime();
        long difMillisBack = getDateDiff(todayStart.toDate(), now.toDate(),TimeUnit.MILLISECONDS);
        long difMillisForward = getDateDiff(now.toDate(), todayEnd.toDate(),TimeUnit.MILLISECONDS);

        DAYS_BACK_MILLIS = (int) difMillisBack;
        DAYS_FORWARD_MILLIS = (int) difMillisForward;

        millisPerPixel = calculateMillisPerPixel();
        timeOffset = calculatedBaseLine();
        timeLowerBoundary = getTimeFrom(0);
        timeUpperBoundary = getTimeFrom(getWidth());
    }

    private Rect calculateChannelsHitArea() {
        measuringRect.top = timeBarHeight;
        int visibleChannelsHeight = epgData.getChannelCount() * (channelLayoutHeight + channelLayoutMargin);
        measuringRect.bottom = visibleChannelsHeight < getHeight() ? visibleChannelsHeight : getHeight();
        measuringRect.left = 0;
        measuringRect.right = channelLayoutWidth;
        return measuringRect;
    }

    private Rect calculateProgramsHitArea() {
        measuringRect.top = timeBarHeight;
        int visibleChannelsHeight = epgData.getChannelCount() * (channelLayoutHeight + channelLayoutMargin);
        measuringRect.bottom = visibleChannelsHeight < getHeight() ? visibleChannelsHeight : getHeight();
        measuringRect.left = channelLayoutWidth;
        measuringRect.right = getWidth();
        return measuringRect;
    }

    private Rect calculateResetButtonHitArea() {
        measuringRect.left = getScrollX() + getWidth() - resetButtonSize - resetButtonMargin;
        measuringRect.top = getScrollY() + getHeight() - resetButtonSize - resetButtonMargin;
        measuringRect.right = measuringRect.left + resetButtonSize;
        measuringRect.bottom = measuringRect.top + resetButtonSize;
        return measuringRect;
    }

    private int getChannelPosition(int y) {
        y -= timeBarHeight;
        int channelPosition = (y + channelLayoutMargin)
                / (channelLayoutHeight + channelLayoutMargin);

        return epgData.getChannelCount() == 0 ? -1 : channelPosition;
    }

    private int getProgramPosition(int channelPosition, long time) {
        List<EPGEvent> events = epgData.getEvents(channelPosition);

        if (events != null) {

            for (int eventPos = 0; eventPos < events.size(); eventPos++) {
                EPGEvent event = events.get(eventPos);

                if (event.getStart() <= time && event.getEnd() >= time) {
                    return eventPos;
                }
            }
        }
        return -1;
    }

    /**
     * Add click listener to the EPG.
     * @param epgClickListener to add.
     */
    public void setEPGClickListener(EPGClickListener epgClickListener) {
        clickListener = epgClickListener;
    }

    /**
     * Add data to EPG. This must be set for EPG to able to draw something.
     * @param epgData pass in any implementation of EPGData.
     */
    public void setEPGData(EPGData epgData) {
        this.epgData = epgData;
    }

    /**
     * This will recalculate boundaries, maximal scroll and scroll to start position which is current time.
     * To be used on device rotation etc since the device height and width will change.
     * @param withAnimation true if scroll to current position should be animated.
     */
//    public void recalculateAndRedraw(boolean withAnimation) {
//        if (epgData != null && epgData.hasData()) {
//            resetBoundaries();
//
//            calculateMaxVerticalScroll();
//            calculateMaxHorizontalScroll();
//
//            scroller.startScroll(getScrollX(), getScrollY(),
//                    getXPositionStart() - getScrollX(),
//                    0, withAnimation ? 600 : 0);
//
//            redraw();
//        }
//    }

    public void recalculateAndRedraw(boolean withAnimation, boolean scrollToNow) {
        if (epgData != null && epgData.hasData()) {
            resetBoundaries();

            calculateMaxVerticalScroll();
            calculateMaxHorizontalScroll();

            if (scrollToNow) {
                scroller.startScroll(getScrollX(), getScrollY(),
                        getXPositionStart() - getScrollX(),
                        0, withAnimation ? 600 : 0);
            }
            redraw();
        }
    }
    /**
     * Does a invalidate() and requestLayout() which causes a redraw of screen.
     */
    public void redraw() {
        invalidate();
        requestLayout();
    }

    /**
     * Clears the local image cache for channel images. Can be used when leaving epg and you want to
     * free some memory. Images will be fetched again when loadingComponent EPG next time.
     */
    public void clearEPGImageCache() {
        channelImageCache.clear();
    }


    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (epgData == null || !epgData.hasData()) {
                return false;
            }
            if(epgData == null){
                return false;
            }

            // This is absolute coordinate on screen not taking scroll into account.
            int x = (int) e.getX();
            int y = (int) e.getY();

            Timber.d("x " + String.valueOf(x));
            Timber.d("y " + String.valueOf(y));

            // Adding scroll to clicked coordinate
            int scrollX = getScrollX() + x;
            int scrollY = getScrollY() + y;

            int channelPosition = getChannelPosition(scrollY);
            if (channelPosition != -1 && clickListener != null) {
                if (calculateResetButtonHitArea().contains(scrollX, scrollY)) {
                    // Reset button clicked
                    clickListener.onResetButtonClicked();
                } else if (calculateChannelsHitArea().contains(x, y)) {
                    // Channel area is clicked
                    clickListener.onChannelClicked(channelPosition, epgData.getChannel(channelPosition));
                } else if (calculateProgramsHitArea().contains(x, y)) {
                    // Event area is clicked
                    int programPosition = getProgramPosition(channelPosition, getTimeFrom(getScrollX() + x - calculateProgramsHitArea().left));
                    if (programPosition != -1) {
                        clickListener.onEventClicked(channelPosition, programPosition, epgData.getEvent(channelPosition, programPosition));
                    }
                }
            }

            return true;

        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            int dx = (int) distanceX;
            int dy = (int) distanceY;
            int x = getScrollX();
            int y = getScrollY();


            // Avoid over scrolling
            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (y + dy < 0) {
                dy = 0 - y;
            }
            if (x + dx > maxHorizontalScroll) {
                dx = maxHorizontalScroll - x;
            }
            if (y + dy > maxVerticalScroll) {
                dy = maxVerticalScroll - y;
            }

            scrollBy(dx, dy);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {
            scroller.fling(getScrollX(), getScrollY(), -(int) vX,
                    -(int) vY, 0, maxHorizontalScroll, 0, maxVerticalScroll);

            redraw();
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            if (!scroller.isFinished()) {
                scroller.forceFinished(true);
                return true;
            }
            return true;
        }
    }

    public DateTime getBaseLineDate() {
        return baseLineDate;
    }

    public void setBaseLineDate(DateTime baseLineDate) {
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date());

        Calendar baseLineCalendar = Calendar.getInstance();
        baseLineCalendar.setTime(baseLineDate.toDate());
        baseLineCalendar.set(Calendar.HOUR_OF_DAY, currentDate.get(Calendar.HOUR_OF_DAY));
        baseLineCalendar.set(Calendar.MINUTE, currentDate.get(Calendar.MINUTE));

        this.baseLineDate = new DateTime(baseLineCalendar.getTime());
    }
}
