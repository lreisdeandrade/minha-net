package br.com.net.netapp.application;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.helper.ObserverHelper;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.account.auth.AuthClient;
import br.com.net.netapp.service.mobilebackend.clients.app.AppClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.netcombo.trackmanager.TrackManager;
import br.com.netcombo.trackmanager.events.Event;
import br.com.netcombo.trackmanager.events.EventContentView;
import io.reactivex.Observable;
import timber.log.Timber;

@EApplication()
public class AppContext extends Application {

    @Bean
    BaseConfiguration baseConfiguration;

    private static AppContext instance;

    private boolean changeNetworkConnection;

    private Observable<Boolean> connectionObservable;
    private boolean isConnected = true;

    private AppClient appClient;

    @Override
    public void onCreate() {
        super.onCreate();

        appClient = new AppClient();

        MultiDex.install(this);
        instance = this;
        registerActivityLifecycleCallbacks();
        initializeTimber();
        baseConfiguration.setupMemoryLeakDetect(this, getApplicationContext());
        baseConfiguration.setupTrackManager(this);
        baseConfiguration.setupRealm(this);
        loadAppSettings();
        baseConfiguration.registerAWSMobileClientPushNotification(this);
        initializeConnectionObservable();
        checkConnectionState();


    }

    protected void loadAppSettings() {
        appClient.loadSettings(new RealmCallbackClient() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                App.createDefault();
            }
        });
    }


    private void checkConnectionState() {
        if (Connectivity.create(this).isAvailable()) {
            changeNetworkConnection = true;
        } else {
            changeNetworkConnection = false;
        }
    }

    private void initializeTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initializeConnectionObservable() {
        connectionObservable = ReactiveNetwork.observeInternetConnectivity(
                10000, 4000, "http://clients3.google.com/generate_204", 80, 4000)
                .filter(isConnected -> {
                    return isConnected != this.isConnected;
                })
                .flatMap(aBoolean1 -> {

                    isConnected = aBoolean1;
                    return Observable.just(aBoolean1);
                })
                .compose(ObserverHelper.getInstance().applySchedulers());
    }

    private void registerActivityLifecycleCallbacks() {

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(final Activity activity, final Bundle savedInstanceState) {
//                if (BuildConfig.BUILD_TYPE.equals("staging")) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                }
            }

            @Override
            public void onActivityStarted(final Activity activity) {

            }

            @Override
            public void onActivityResumed(final Activity activity) {

            }

            @Override
            public void onActivityPaused(final Activity activity) {

            }

            @Override
            public void onActivityStopped(final Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(final Activity activity, final Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(final Activity activity) {

            }
        });
    }

    public Observable<Boolean> getConnectionObservable() {
        return connectionObservable;
    }


    public void trackManagerEventPush(String category, String action, String label, String itemId) {
        Event event = new EventContentView(category, action, label, itemId);
        TrackManager.getInstance().trackEvent(event);
    }

    public static boolean isOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public void setChangeNetworkConnection(boolean changeNetworkConnection) {
        this.changeNetworkConnection = changeNetworkConnection;
    }

    public static synchronized AppContext getInstance() {
        return instance;
    }

}
