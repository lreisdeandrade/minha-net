package br.com.net.netapp.helper;

import android.os.Build;
import android.support.annotation.NonNull;
import android.webkit.WebView;

/**
 * Created by leandro on 03/02/17.
 */
public class JavaScriptInjector {

    public static void injectJavascript(String script, @NonNull WebView webView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            webView.evaluateJavascript(script, null);
        } else {
            webView.loadUrl("javascript:" + script + ";");
        }
    }
}