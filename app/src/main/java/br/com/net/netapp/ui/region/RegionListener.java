package br.com.net.netapp.ui.region;

/**
 * Created by Leandro on 06/09/17.
 */

public interface RegionListener {

    void onRegionChange();
}
