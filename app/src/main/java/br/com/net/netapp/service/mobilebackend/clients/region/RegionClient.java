package br.com.net.netapp.service.mobilebackend.clients.region;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import br.com.net.netapp.service.mobilebackend.models.State;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 06/09/17.
 */

public class RegionClient extends BaseClient {
    private String TAG = "RegionClient";
    private IRegionClient stateClient;

    public RegionClient() {
        super(BuildConfig.BASE_URL_TV);

        this.stateClient = this.retrofit.create(IRegionClient.class);
    }

    public void loadStates(final RealmCallbackClient callbackClient) {

        HashMap<String, String> queryStringMap = new LinkedHashMap<String, String>();

        queryStringMap.put("groupByState", "true");

        this.stateClient.loadStates(queryStringMap)
                .enqueue(new Callback<ResponseDataCollection<State>>() {
                    @Override
                    public void onResponse(Call<ResponseDataCollection<State>> call, Response<ResponseDataCollection<State>> response) {
                        if (response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().getDataInfo());
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataCollection<State>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }
}