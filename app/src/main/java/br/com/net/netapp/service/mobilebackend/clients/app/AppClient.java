package br.com.net.netapp.service.mobilebackend.clients.app;

import android.util.Log;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.service.mobilebackend.clients.BaseClient;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.models.App;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.utils.BuilderErrorMessage;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Leandro on 06/09/17.
 */

public class AppClient extends BaseClient {
    private String TAG = "RegionClient";
    private IAppClient appClient;

    public AppClient() {
        super(BuildConfig.BASE_URL_ACCOUNT);

        this.appClient = this.retrofit.create(IAppClient.class);

    }

    public void loadSettings(final RealmCallbackClient callbackClient) {

        this.appClient.loadAppSettings()
                .enqueue(new Callback<ResponseData<App>>() {
                    @Override
                    public void onResponse(Call<ResponseData<App>> call, Response<ResponseData<App>> response) {
                        if (response.isSuccessful()) {
                            callbackClient.onSuccess(response.body().getData());
                        } else {
                            ErrorMessage errorMessage = getErrorMessage(TAG, response.errorBody());
                            callbackClient.onError(errorMessage);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData<App>> call, Throwable t) {
                        ErrorMessage errorMessage = BuilderErrorMessage.fromRequest(TAG, t);
                        callbackClient.onError(errorMessage);
                    }
                });
    }
}