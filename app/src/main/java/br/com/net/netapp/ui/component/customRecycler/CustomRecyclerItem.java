package br.com.net.netapp.ui.component.customRecycler;

import android.support.v7.widget.RecyclerView;

import com.mikepenz.fastadapter.items.AbstractItem;

/**
 * Created by Leandro on 05/06/17.
 */

public abstract class CustomRecyclerItem<T, item extends AbstractItem<?, ?>,
        viewHolder extends RecyclerView.ViewHolder> extends AbstractItem<item, viewHolder> {

    private T item;

    public CustomRecyclerItem(T item) {
        this.item = item;
    }

    public T getModel() {
        return item;
    }
}

