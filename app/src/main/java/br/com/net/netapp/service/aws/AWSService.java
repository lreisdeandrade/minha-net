package br.com.net.netapp.service.aws;

import java.util.HashMap;

import br.com.net.netapp.BuildConfig;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by leandro on 10/01/17.
 */

public interface AWSService {

    @Headers({
            "x-api-key:" + BuildConfig.AWS_KEY,
    })
    @POST(BuildConfig.AWS_DEVICE)
    Call<AWSResponse> registerAWSDevice(@Body HashMap<String, String> device);

    @Headers({
            "x-api-key:" + BuildConfig.AWS_KEY,
    })
    @PUT(BuildConfig.AWS_DEVICE)
    Call<AWSResponse> updateAWSDevice(@Body HashMap<String, String> device);
}