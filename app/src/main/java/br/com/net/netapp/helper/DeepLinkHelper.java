package br.com.net.netapp.helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.net.netapp.activity.WebViewActivity_;

/**
 * Created by elourenco on 22/04/17.
 */

@EBean
public class DeepLinkHelper {

    @RootContext
    Context context;

    public void openDeepLink(String name, final String deeplink, String urlStore, String landingUrl) {

        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(deeplink));

        if (intent != null) {
            try {

                context.startActivity(intent);

            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                showDialogDeepLink(intent, name, landingUrl, deeplink, urlStore);
            }
        }

    }

    protected void showDialogDeepLink(final Intent intent, String appName, final String landingUrl, final String packageName, final String urlStore) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(appName);
        builder.setMessage(String.format("Para uma melhor experiência, baixe agora nosso aplicativo."));
        builder.setPositiveButton("Baixar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse(urlStore));

                        if (intent.resolveActivity(context.getPackageManager()) != null)
                            context.startActivity(intent);
                    }
                });
        builder.setNegativeButton("Fechar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        WebViewActivity_.intent(context).flags(Intent.FLAG_ACTIVITY_NEW_TASK).url(landingUrl).start();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }
}
