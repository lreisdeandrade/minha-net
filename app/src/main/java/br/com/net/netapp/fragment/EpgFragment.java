package br.com.net.netapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.activity.BaseActivity;
import br.com.net.netapp.activity.EpgFilterCategoryActivity_;
import br.com.net.netapp.activity.SearchActivity;
import br.com.net.netapp.adapter.CalendarAdapter;
import br.com.net.netapp.application.AppContext;
import br.com.net.netapp.event.UserSignoutEvent;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.ui.component.epg.EPGClickListener;
import br.com.net.netapp.ui.component.epg.EPGData;
import br.com.net.netapp.ui.component.epg.EpgView;
import br.com.net.netapp.ui.component.epg.domain.EPGChannel;
import br.com.net.netapp.ui.component.epg.domain.EPGEvent;
import br.com.net.netapp.ui.component.epg.misc.EPGDataImpl;
import br.com.net.netapp.model.Filter;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.epg.EpgClient;
import br.com.net.netapp.service.mobilebackend.models.Channel;
import br.com.net.netapp.service.mobilebackend.models.Epg;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

@EFragment(R.layout.fragment_epg)
public class EpgFragment extends BaseFragment {

    private EpgView epgGroupView;
    private EpgClient epgClient;

    ArrayList<String> daysOfCalendar;

    @FragmentArg("highLightChannels")
    ArrayList<String> highLightChannels;

    @ViewById
    RelativeLayout loading;

    @ViewById
    TextView textViewErrorMessage;

    @ViewById
    GridView gridViewCalendar;

    @ViewById
    ViewGroup container;

    CalendarAdapter calendarAdapter;

    @Bean
    SessionManager sessionManager;

    @ViewById
    ProgressBar progressComponent;

    private DateTime dateStartCurrent;
    private DateTime dateEndCurrent;

    private Context context;

    @AfterViews
    public void afterViews() {
        this.context = getContext();
        if (context instanceof BaseActivity) {
            realm = ((BaseActivity) context).getRealm();
        } else {
            throw new RuntimeException("The context must extend BaseActivity");
        }
        setHasOptionsMenu(true);
        init();

    }

    private void init() {

        SearchActivity context = (SearchActivity) getActivity();
        context.removeInstanceFragment();

        this.epgClient = new EpgClient();
        configCalendar();
        this.dateStartCurrent = new DateTime().withTimeAtStartOfDay();
        this.dateEndCurrent = new DateTime().millisOfDay().withMaximumValue();
        deletePassedEpg(this.dateStartCurrent);

    }

    private void buildEpgBetweenDates(final DateTime todayStart, DateTime todayEnd) {
        RealmObject epgRealm = realm.where(Epg.class)
                .between("startDate", todayStart.toDate(), todayEnd.toDate()).findFirst();

        if (epgRealm != null) {
            Epg epgModel = (Epg) epgRealm;
            buildEPG(epgModel, todayStart);

            this.epgClient.loadEpg(String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), todayStart, new RealmCallbackClient() {
                @Override
                public void onError(ErrorMessage errorMessage) {
                    Timber.d(errorMessage.getMessage());

                    if (progressComponent != null) {
                        progressComponent.setVisibility(View.INVISIBLE);
                        textViewErrorMessage.setVisibility(View.VISIBLE);
                        textViewErrorMessage.setText(errorMessage.getMessage());
                    }
                }

                @Override
                public void onSuccess(RealmObject response) {
                    super.onSuccess(response);
                    Epg epgModel = (Epg) response;
                    buildEPG(epgModel, todayStart);
                }
            });

        } else {
            this.epgClient.loadEpg(String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), todayStart, new RealmCallbackClient() {
                @Override
                public void onError(ErrorMessage errorMessage) {
                    Timber.d(errorMessage.getMessage());

                    if (progressComponent != null) {
                        progressComponent.setVisibility(View.INVISIBLE);
                        textViewErrorMessage.setVisibility(View.VISIBLE);
                        textViewErrorMessage.setText(errorMessage.getMessage());
                    }

                }

                @Override
                public void onSuccess(RealmObject response) {
                    super.onSuccess(response);
                    Epg epgModel = (Epg) response;
                    buildEPG(epgModel, todayStart);
                }
            });
        }
    }

    protected void configCalendar() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 7);

        Calendar currentCalendar = Calendar.getInstance();

        daysOfCalendar = new ArrayList<>();

        while (Integer.valueOf(simpleDateFormat.format(calendar.getTime())) >
                Integer.valueOf(simpleDateFormat.format(currentCalendar.getTime()))) {
            daysOfCalendar.add(simpleDateFormat.format(currentCalendar.getTime()));
            currentCalendar.add(Calendar.DATE, 1);
        }

        calendarAdapter = new CalendarAdapter(context);

        calendarAdapter.setDiasCalendario(daysOfCalendar);

        gridViewCalendar.setAdapter(calendarAdapter);
        calendarAdapter.notifyDataSetChanged();
        gridViewCalendar.setOnItemClickListener((parent, view, position, id) -> calendarioClicked(position));
    }

    protected void calendarioClicked(int position) {
        calendarAdapter.setPositionSelected(position);
        calendarAdapter.notifyDataSetChanged();
        Date dateSelected = null;
        try {
            dateSelected = new SimpleDateFormat("yyyyMMdd").parse(daysOfCalendar.get(position) + " 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.dateStartCurrent = new DateTime(dateSelected).withTimeAtStartOfDay();
        this.dateEndCurrent = new DateTime(dateSelected).millisOfDay().withMaximumValue();

        loading.setVisibility(View.VISIBLE);
        buildEpgBetweenDates(this.dateStartCurrent, this.dateEndCurrent);
        AppContext.getInstance().trackManagerEventPush("grade", "calendario", dateSelected.toString(), "");

    }


    protected void deletePassedEpg(final DateTime todayStart) {
        realm.executeTransaction(realm1 -> {
            RealmResults<Epg> listOfOldEpg = realm1.where(Epg.class)
                    .lessThan("endDate", todayStart.toDate()).findAll();

            for (Epg oldEpg : listOfOldEpg) {

                for (Channel channel : oldEpg.getChannels()) {

                    channel.getExhibitions().deleteAllFromRealm();
                }

                oldEpg.getChannels().deleteAllFromRealm();
            }

            listOfOldEpg.deleteAllFromRealm();
        });
        setupEPG();
        buildEpgBetweenDates(this.dateStartCurrent, this.dateEndCurrent);
    }

    private void buildEPG(Epg epgModel, DateTime selectedDate) {
        RealmList<Channel> channels = epgModel.getChannels();
        HashMap<EPGChannel, List<EPGEvent>> result = new LinkedHashMap<>();

        if (highLightChannels != null) {

            for (String channelId : highLightChannels) {
                for (Channel channel : channels) {
                    if (String.valueOf(channel.getId()).equals(channelId)) {
                        List<EPGEvent> listEvents = new ArrayList<>();

                        for (Exhibition exhibition : channel.getExhibitions()) {
                            listEvents.add(new EPGEvent(exhibition));
                        }
                        result.put(new EPGChannel(channel), listEvents);
                    }
                }
            }

        } else {

            for (Channel channel : channels) {

                if (Filter.categoriesList() != null && Filter.categoriesList().size() > 0) {
                    if (!Filter.has(channel.getCategoryId())) {
                        continue;
                    }
                }

                List<EPGEvent> listEvents = new ArrayList<>();

                for (Exhibition exhibition : channel.getExhibitions()) {
                    listEvents.add(new EPGEvent(exhibition));
                }

                result.put(new EPGChannel(channel), listEvents);
            }
        }


        EPGData epgData = new EPGDataImpl(result);
        epgGroupView.setEPGData(epgData);
        epgGroupView.setBaseLineDate(selectedDate);
        epgGroupView.scrollTo(epgGroupView.getScrollX(), 0);
        epgGroupView.recalculateAndRedraw(false, true);

        if (loading != null) {
            loading.animate().alpha(1.0f);
            loading.setVisibility(View.INVISIBLE);
        }

    }

    private float x1, x2, y1, y2, lockScrollX, lockScrollY;
    static final int MIN_DISTANCE = 50;

    protected void setupEPG() {
        epgGroupView = new EpgView(getActivity());
        container.addView(epgGroupView);
        epgGroupView.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x1 = event.getX();
                    y1 = event.getY();
                    lockScrollX = epgGroupView.getScrollX();
                    lockScrollY = epgGroupView.getScrollY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    x2 = event.getX();
                    y2 = event.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        });
        epgGroupView.setEPGClickListener(new EPGClickListener() {

            @Override
            public void onChannelClicked(int channelPosition, EPGChannel epgChannel) {
                AppContext.getInstance()
                        .trackManagerEventPush("grade", "canal", epgChannel.getSolrCanal().getName(), "");

                ActivityRoutes.getInstance().openChannelActivity(context, epgChannel.getSolrCanal().getId());
            }

            @Override
            public void onEventClicked(int channelPosition, int programPosition, EPGEvent epgEvent) {
                ActivityRoutes.getInstance().openSinopsysActivity(context, epgEvent.getExhibition().getId());

                AppContext.getInstance().trackManagerEventPush("grade", "programa", epgEvent.getTitle(), "");

            }

            @Override
            public void onResetButtonClicked() {
                DateTime todayStart = new DateTime().withTimeAtStartOfDay();
                DateTime todayEnd = new DateTime().millisOfDay().withMaximumValue();
                epgGroupView.setBaseLineDate(new DateTime());
                buildEpgBetweenDates(todayStart, todayEnd);
                epgGroupView.recalculateAndRedraw(false, true);
                calendarAdapter.setPositionSelected(0);
                calendarAdapter.notifyDataSetChanged();
            }
        });
    }

    private void addToolbarButtons() {
        SearchActivity searchActivity = (SearchActivity) context;
        searchActivity.addSearchButton();
        searchActivity.getMenu()
                .add(Menu.NONE, R.id.filter_item, Menu.FIRST, R.string.menu_item_epg_filter)
                .setIcon(R.drawable.ic_filter_list_white)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        addToolbarButtons();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_item:

                if (!loading.isShown()) {
                    Intent i = EpgFilterCategoryActivity_.intent(getActivity()).get();
                    this.startActivityForResult(i, 1);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            buildEpgBetweenDates(this.dateStartCurrent, this.dateEndCurrent);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().invalidateOptionsMenu();
    }
}
