package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

public class ResponseData<T> {
    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("requestId")
    private String requestId;

    @SerializedName("data")
    public T data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
