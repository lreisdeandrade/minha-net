package br.com.net.netapp.service.mobilebackend.clients.tv.screen;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.Screen;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface IScreensClient {
    @GET("screens/published")
    Call<ResponseData<Screen>> loadScreen(@QueryMap HashMap<String, String> queryString);
}
