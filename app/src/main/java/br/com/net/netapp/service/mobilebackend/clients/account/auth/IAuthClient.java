package br.com.net.netapp.service.mobilebackend.clients.account.auth;

import java.util.HashMap;

import br.com.net.netapp.model.User;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import br.com.net.netapp.service.mobilebackend.models.Signout;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by Leandro on 19/09/17.
 */

public interface IAuthClient {

    @POST("auth/sign-in")
    Call<ResponseData<User>> signin(@Body HashMap<String, String> userInfo);

    @DELETE("auth/sign-out")
    Call<ResponseData<Signout>> signout(@Header("x-access-token") String xAccessToken);

    @PUT("auth/refresh")
    Call<ResponseData<User>> autoSignin(@Header("x-access-token") String xAccessToken);
}
