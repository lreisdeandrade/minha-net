package br.com.net.netapp.service.aws;

import android.util.Log;

import java.util.HashMap;

import br.com.net.netapp.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by leandro on 10/01/17.
 */

public class AWSManager {

    private static final String TAG = "AWSManager";
    private static final String POOL_ID = "us-east-1:6c259a4f-3731-4f39-902e-aeef80e951d0";
    private static AWSManager instance;
    private Retrofit retrofit;
    private AWSService awsService;


    private AWSManager() {
        HttpLoggingInterceptor interceptor = new    HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        this.retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.AWS_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.awsService = retrofit.create(AWSService.class);
    }

    public static synchronized AWSManager getInstance() {
        if (instance == null) {
            instance = new AWSManager();
        }
        return instance;
    }

    public void registerDeviceAWS(HashMap<String, String> awsDevice, final AWSCallBack<AWSResponse> callback) {

        Log.d(TAG, "registrando Device ");
        Call<AWSResponse> awsCall = this.awsService.registerAWSDevice(awsDevice);

        awsCall.enqueue(new Callback<AWSResponse>() {
            @Override
            public void onResponse(Call<AWSResponse> call, Response<AWSResponse> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<AWSResponse> call, Throwable t) {
                Log.d(TAG, t.toString());

                callback.onFailure(t);
            }
        });
    }



    public void updateDeviceAWS(HashMap<String, String> deviceInfomap, final AWSCallBack<AWSResponse> callback) {

        Call<AWSResponse> awsCall = this.awsService.updateAWSDevice(deviceInfomap);

        awsCall.enqueue(new Callback<AWSResponse>() {
            @Override
            public void onResponse(Call<AWSResponse> call, Response<AWSResponse> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<AWSResponse> call, Throwable t) {
                Log.d(TAG, t.toString());

                callback.onFailure(t);
            }
        });
    }
}