package br.com.net.netapp.fragment;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.net.netapp.ActivityRoutes;
import br.com.net.netapp.R;
import br.com.net.netapp.activity.EpgActivity_;
import br.com.net.netapp.activity.SinopseActivity_;
import br.com.net.netapp.activity.WebViewActivity_;
import br.com.net.netapp.adapter.HighlightAdapter;
import br.com.net.netapp.adapter.ItemAdapter;
import br.com.net.netapp.helper.Constants;
import br.com.net.netapp.helper.DeepLinkHelper;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.service.mobilebackend.clients.RealmCallbackClient;
import br.com.net.netapp.service.mobilebackend.clients.tv.screen.ScreensClient;
import br.com.net.netapp.service.mobilebackend.models.Channel;
import br.com.net.netapp.service.mobilebackend.models.ErrorMessage;
import br.com.net.netapp.service.mobilebackend.models.Highlight;
import br.com.net.netapp.service.mobilebackend.models.Screen;
import br.com.net.netapp.service.mobilebackend.models.ScreenItem;
import io.realm.RealmResults;
import io.realm.Sort;


@EFragment(R.layout.fragment_highlight)
public class HighlightsFragment extends BaseFragment {

    public static final String DEEP_LINK = "DeepLink";
    public static final String BANNER = "Banner";
    public static final String HOME = "Home";
    public static final String LANDING = "Landing";
    public static final String TV_SHOW = "TvShow";
    public static final String EPG = "EPG";
    private static final String TAG = "HighlightsFragment";

    @Bean
    DeepLinkHelper deepLinkHelper;

    @Bean
    SessionManager sessionManager;

    @ViewById
    AsymmetricGridView gridView;

    @ViewById
    TextView textViewErrorMessage;


    @ViewById
    ProgressBar progressComponent;

    @ViewById
    RelativeLayout loading;

    private RealmResults<Highlight> highlights;

    protected ScreensClient screenClient;

    private ItemAdapter highlightAdapter;
    private Highlight highlight;

    @AfterViews
    protected void setupFragment() {
        this.screenClient = new ScreensClient();
        this.highlights = this.realm.where(Highlight.class).findAll().sort("order", Sort.ASCENDING);
        addRealmChangeListener();

        loadHighlights();
    }

    public void addRealmChangeListener() {
        this.highlights.addChangeListener(element ->
        {
            if (!element.isEmpty()) {
                setupHighlightDataSource(element);
            } else {
                if (progressComponent != null) {
                    progressComponent.setVisibility(View.INVISIBLE);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                    textViewErrorMessage.setText(getContext().getString(R.string.no_content));
                }
            }
        });
    }

    public HighlightsFragment() {
    }

    protected void loadHighlights() {
        screenClient.getHighLights(String.valueOf(sessionManager.getAppSettings().getSettings().getSelectedCityId()), new RealmCallbackClient<Screen>() {
            @Override
            public void onError(ErrorMessage errorMessage) {
                if (progressComponent != null) {
                    progressComponent.setVisibility(View.INVISIBLE);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                    textViewErrorMessage.setText(errorMessage.getMessage());
                }
            }
        });
        if (this.highlights != null && !this.highlights.isEmpty()) {
            setupHighlightDataSource(this.highlights);
        }
    }

    protected void setupHighlightDataSource(final RealmResults<Highlight> highlights) {
        List<ScreenItem> items = new ArrayList<>();
        for (int i = 0; i < highlights.size(); i++) {

            Highlight highlight = highlights.get(i);

            int colSpan = 0;
            int rowSpan = 0;

            if (highlight.getWidth() == 100 && highlight.getProportion() == 1) {

                colSpan = 2;
                rowSpan = 2;

            } else if (highlight.getWidth() == 100 && highlight.getProportion() < 1) {

                colSpan = 2;
                rowSpan = 1;

            } else if (highlight.getWidth() < 100 && highlight.getProportion() == 1) {

                colSpan = 1;
                rowSpan = 1;

            } else if (highlight.getWidth() < 100 && highlight.getProportion() > 1) {

                colSpan = 1;
                rowSpan = 2;
            }

            ScreenItem item = new ScreenItem(colSpan, rowSpan, highlight);
            items.add(item);
        }
        //FIXME check why the gridview is null when reload the screen
        if (getActivity() != null && gridView != null) {

            highlightAdapter = new HighlightAdapter(getContext(), items);
            gridView.setRequestedHorizontalSpacing(Utils.dpToPx(getContext(), 1));
            gridView.setAdapter(new AsymmetricGridViewAdapter(getContext(), gridView, highlightAdapter));
            gridView.setOnItemClickListener((parent, view, position, id) -> {
                highlight = highlights.get(position);


                if (highlights != null && highlight.getNeedAuthenticated() != null && highlight.getNeedAuthenticated()) {
                    if (sessionManager.isActive()) {
                        actionByType(highlight);
                    } else {

                        ActivityRoutes.getInstance().openLoginActivityFromFragment(getActivity(), this, Constants.LOGIN_RESULT);

                    }
                } else {
                    actionByType(highlight);

                }
            });
            loading.animate().alpha(1.0f);
            loading.setVisibility(View.INVISIBLE);
        }

    }

    @OnActivityResult(Constants.LOGIN_RESULT)
    void onResultReminder(int resultCode) {
        if (resultCode == getActivity().RESULT_OK) {
            actionByType(highlight);
        }
    }

    private void actionByType(Highlight highlight) {

        if (highlight.getType().equals(LANDING)) {
            String url = highlight.getLandingUrl();
            if(sessionManager.isActive()) {
                    url = url.replace("{documentId}", sessionManager.getUser().getDocumentId());
            }
            WebViewActivity_.intent(getContext())
                    .url(url)
                    .title(highlight.getTitle())
                    .flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
            trackManagerEventPush(HOME, BANNER,
                    LANDING, "");
        } else if (highlight.getType().equals(DEEP_LINK)) {
            deepLinkHelper.openDeepLink(highlight.getTitle(),
                    highlight.getDeepLink(), highlight.getGooglePlayUrl(),
                    highlight.getLandingUrl());
            trackManagerEventPush(HOME, BANNER, DEEP_LINK, null);
        } else if (highlight.getType().equals(TV_SHOW)) {
            SinopseActivity_.intent(getContext()).idExhibition(highlight.getExhibitionId()).start();
        } else if (highlight.getType().equals(EPG)) {
            ArrayList<String> hightlightChannels = new ArrayList<String>();
            for (Channel channel : highlight.getChannels()) {
                hightlightChannels.add(String.valueOf(channel.getId()));
            }
            EpgActivity_.intent(getContext()).channels(hightlightChannels).start();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.highlights.removeAllChangeListeners();
    }
}
