package br.com.net.netapp.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import br.com.net.netapp.R;
import br.com.net.netapp.event.FingerPrintAuthCanceledEvent;
import br.com.net.netapp.event.FingerPrintAuthSuccessEvent;

/**
 * Created by Leandro on 30/09/17.
 */
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private static final String TAG = "FingerprintHandler";
    private Context context;

    private TextView messageAuthResult;
    private MaterialDialog dialog;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "NETApp";
    private Cipher cipher;

    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;
    }

    public boolean isSensorAvialable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED &&
                    context.getSystemService(FingerprintManager.class).isHardwareDetected();
        } else {
            return FingerprintManagerCompat.from(context).isHardwareDetected();
        }
    }

    public boolean fingerPrintAvailable() {

        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(context.KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(context.FINGERPRINT_SERVICE);

        if (!fingerprintManager.isHardwareDetected()) {

            return false;

        } else {
            // Checks whether fingerprint permission is set on manifest
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

                return false;

            } else {
                // Check whether at least one fingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {

                    return false;
                } else {
                    // Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {
                        return false;
                    } else {

                        return true;

                    }
                }
            }
        }
    }

    public void startAuth() {
        generateKey();
        FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(context.FINGERPRINT_SERVICE);

        if (cipherInit()) {
            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
            FingerprintHandler helper = new FingerprintHandler(context);
            helper.startAuth(fingerprintManager, cryptoObject);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .customView(R.layout.dialog_fingerprint, true)
                .title("Login")
                .cancelable(false)
                .onNegative((dialog1, which) -> {
                    EventBus.getDefault().post(new FingerPrintAuthCanceledEvent());
                    dialog1.dismiss();
                })
                .negativeText("cancelar");


        dialog = builder.build();
        dialog.show();

        messageAuthResult = (TextView) dialog.getView().findViewById(R.id.label_fingerprint);

        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        ImageView icFingerprint = (ImageView) dialog.getView().findViewById(R.id.ic_fingerprint);

        icFingerprint.setImageResource(R.drawable.ic_fingerprint_error);
        messageAuthResult.setTextColor(ContextCompat.getColor(context, R.color.finger_print_error));
        messageAuthResult.setText("Muitas tentativas. Tente novamente mais tarde.");

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            dialog.dismiss();
            //            EventBus.getDefault().post(new FingerPrintAuthCanceledEvent());
        }, 1000);

    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        showUpdateFailed(helpString.toString());
        Log.d(TAG, "help" + helpString.toString());
    }

    @Override
    public void onAuthenticationFailed() {
        showUpdateFailed("Impressão digital não reconhecida");
    }

    private void showUpdateFailed(String error) {
        ImageView icFingerprint = (ImageView) dialog.getView().findViewById(R.id.ic_fingerprint);

        icFingerprint.setImageResource(R.drawable.ic_fingerprint_error);
        messageAuthResult.setTextColor(ContextCompat.getColor(context, R.color.finger_print_error));
        messageAuthResult.setText(error);

        final Handler handler = new Handler();
        handler.postDelayed(() -> updateMessage(), 1500);

    }

    protected void updateMessage() {

        ImageView icFingerprint = (ImageView) dialog.getView().findViewById(R.id.ic_fingerprint);
        icFingerprint.setImageResource(R.drawable.ic_fingerprint);
        messageAuthResult.setText("Toque no sensor");
        messageAuthResult.setTextColor(ContextCompat.getColor(context, R.color.finger_print_original_text));
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        showUpdateSuccess("Impressão digital reconhecida");

    }

    private void showUpdateSuccess(String message) {
        ImageView icFingerprint = (ImageView) dialog.getView().findViewById(R.id.ic_fingerprint);

        icFingerprint.setImageResource(R.drawable.ic_fingerprint_success);
        messageAuthResult.setTextColor(ContextCompat.getColor(context, R.color.color_accent));
        messageAuthResult.setText(message);

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            EventBus.getDefault().post(new FingerPrintAuthSuccessEvent());
            dialog.dismiss();

        }, 1000);

    }

}
