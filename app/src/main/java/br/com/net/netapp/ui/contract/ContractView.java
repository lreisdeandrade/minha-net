package br.com.net.netapp.ui.contract;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.Decoder;
import br.com.net.netapp.ui.decoder.DecoderListener;
import br.com.net.netapp.ui.decoder.DecoderView;
import br.com.net.netapp.ui.decoder.DecoderView_;

/**
 * Created by Leandro on 24/07/17.
 */
@EViewGroup(R.layout.contract_view)
public class ContractView extends RelativeLayout {

    @ViewById
    FrameLayout frame;


    @ViewById
    ExpandableLayout expandableLayout;

    @ViewById
    ImageView icArrowIndicator;

    @ViewById
    TextView contractCode, contractAddress;

    @ViewById
    LinearLayout containerReceptor, emptyView;

    Context context;

    ContractListener contractlistener;
    DecoderListener  decoderlistener;

    Contract contract;

    public ContractView(Context context, Contract contract, ContractListener contractListener, DecoderListener decoderListener) {
        super(context);
        this.context = context;
        this.contract = contract;
        this.contractlistener = contractListener;
        this.decoderlistener = decoderListener;
    }

    @AfterViews
    protected void afterView() {

        contractCode.setText(contract.getCode());
        if(contract.getAddress() != null)
        contractAddress.setText(contract.getAddress().getCity());

        if (contract.getDecoders().isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            setupDecoders();
        }
    }

    private void setupDecoders() {
        for (Decoder decoder : contract.getDecoders()) {
            DecoderView receptorView = DecoderView_.build(context, decoder, (DecoderListener) context);
            containerReceptor.addView(receptorView);
        }
    }

    @Click(R.id.frame)
    protected void frameClicked() {

        contractlistener.onContractClicked(contract);

        if (expandableLayout.isExpanded()) {
            expandableLayout.collapse();
            icArrowIndicator.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        } else {
            icArrowIndicator.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            expandableLayout.expand();

        }
    }

    public void collapse() {
        expandableLayout.collapse();
        icArrowIndicator.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
    }
}
