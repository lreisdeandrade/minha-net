package br.com.net.netapp.activity;

import android.annotation.SuppressLint;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.HashMap;
import java.util.Map;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.R;
import br.com.net.netapp.manager.SessionManager;

/**
 * Created by Leandro on 22/04/17.
 */

@EActivity(R.layout.activity_web_view)
public class WebViewActivity extends BaseActivity {

    protected static final String TAG = "WebViewActivity";

    @ViewById
    WebView activityWebview;

    @Extra
    String url;

    @Extra
    String title;

    @ViewById
    RelativeLayout loading;

    @ViewById
    ImageView imgMenu;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView txtTituloAppBar;

    @Bean
    SessionManager sessionManager;

    @SuppressLint("SetJavaScriptEnabled")
    @AfterViews
    protected void setupWebview() {

        CookieManager cookieManager = CookieManager.getInstance();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            cookieManager.removeAllCookie();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        activityWebview.getSettings().setJavaScriptEnabled(true);
        activityWebview.getSettings().setDomStorageEnabled(true);
        txtTituloAppBar.setText(title);
        txtTituloAppBar.setVisibility(View.VISIBLE);

        activityWebview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                loading.animate().alpha(0.0f);
                loading.setVisibility(View.INVISIBLE);
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                loading.animate().alpha(0.0f);
                loading.setVisibility(View.INVISIBLE);

                final AlertDialog.Builder builder = new AlertDialog.Builder(WebViewActivity.this);
                builder.setMessage("Deseja continuar ?");
                builder.setPositiveButton("sim",
                        (dialog, which) -> handler.proceed());
                builder.setNegativeButton("não",
                        (dialog, which) -> {
                            handler.cancel();
                            finish();
                            Toast.makeText(getApplicationContext(),
                                    "Ação não concluida!",
                                    Toast.LENGTH_SHORT).show();

                        });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                loading.animate().alpha(0.0f);
                loading.setVisibility(View.INVISIBLE);
            }
        });

        activityWebview.getSettings().setGeolocationDatabasePath(getFilesDir().getPath());

        loading.animate().alpha(1.0f);
        loading.setVisibility(View.VISIBLE);

        activityWebview.loadUrl(url, getCustomHeaders());
    }

    @Click(R.id.img_menu)
    protected void toolBarBackClick() {
       onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (activityWebview.canGoBack()) {
            activityWebview.goBack();
        } else {
            finish();
        }
    }

    private Map<String, String> getCustomHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("x-api-key", BuildConfig.API_KEY);
        if (sessionManager.isActive()) {
            headers.put("x-access-token", sessionManager.getUser().getToken());
            headers.put("x-app", "minhanetapp");

        }
        return headers;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (activityWebview.canGoBack()) {
                        activityWebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
