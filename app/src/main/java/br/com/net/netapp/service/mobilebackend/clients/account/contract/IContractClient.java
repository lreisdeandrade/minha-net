package br.com.net.netapp.service.mobilebackend.clients.account.contract;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.Contract;
import br.com.net.netapp.service.mobilebackend.models.ResponseDataCollection;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;

/**
 * Created by Leandro on 25/07/17.
 */

public interface IContractClient {

    @GET("users/contracts")
    Call<ResponseDataCollection<Contract>> loadContracts(@Header("x-access-token") String xAccessToken, @QueryMap HashMap<String, String> queryString);
}