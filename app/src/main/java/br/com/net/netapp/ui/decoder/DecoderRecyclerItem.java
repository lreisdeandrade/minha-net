package br.com.net.netapp.ui.decoder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import br.com.net.netapp.R;
import br.com.net.netapp.service.mobilebackend.models.Decoder;
import br.com.net.netapp.ui.component.customRecycler.CustomRecyclerItem;

/**
 * Created by Leandro on 25/09/17.
 */

public class DecoderRecyclerItem extends CustomRecyclerItem<Decoder,
        DecoderRecyclerItem, DecoderRecyclerItem.ViewHolder> {

    //the static ViewHolderFactory which will be used to generate the ViewHolder for this Item
    private static final ViewHolderFactory<? extends DecoderRecyclerItem.ViewHolder> FACTORY = new DecoderRecyclerItem.ItemFactory();
    private Context context;

    public DecoderRecyclerItem(Context context, Decoder item) {
        super(item);
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.decoder_recycler_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.decoder_recycler_item;
    }

    @Override
    public void bindView(DecoderRecyclerItem.ViewHolder viewHolder) {
        super.bindView(viewHolder);
        Decoder viewModel = getModel();

//        viewHolder.decoderCode.setText(viewModel.getCode());
        viewHolder.decoderPoint.setText(viewModel.getLocalization());
        viewHolder.decoderLocal.setText(viewModel.getType());

    }


    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster
     * for ViewHolder creation, and it is also many many times more efficient
     * if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<DecoderRecyclerItem.ViewHolder> {
        public DecoderRecyclerItem.ViewHolder create(View v) {
            return new DecoderRecyclerItem.ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends DecoderRecyclerItem.ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView decoderCode, decoderPoint, decoderLocal;


        public ViewHolder(View view) {
            super(view);
            this.decoderCode = (TextView) view.findViewById(R.id.decoder_code);
            this.decoderPoint = (TextView) view.findViewById(R.id.point);
            this.decoderLocal = (TextView) view.findViewById(R.id.local);
        }
    }
}