package br.com.net.netapp.activity;

import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import br.com.net.netapp.R;
import br.com.net.netapp.fragment.EpgFragment;
import br.com.net.netapp.fragment.EpgFragment_;

/**
 * Created by Leandro on 31/05/17.
 */

@EActivity(R.layout.activity_epg)
public class EpgActivity extends SearchActivity {

    @Extra
    ArrayList<String> channels;

    @ViewById
    ViewGroup container;

    @AfterViews
    protected void bindActivity() {
        EpgFragment gradeFragment = EpgFragment_.builder().highLightChannels(channels).build();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_container, gradeFragment);
        fragmentTransaction.commitAllowingStateLoss();

    }

    @Click(R.id.img_menu)
    protected void backClicked() {
        onBackPressed();
    }

    @Override
    public void onLongItemClick(View view, Object item, int position) {

    }
}
