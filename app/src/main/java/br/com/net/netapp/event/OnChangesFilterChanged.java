package br.com.net.netapp.event;

/**
 * Created by elourenco on 11/05/17.
 */

public class OnChangesFilterChanged {

    private int count = 0;

    public OnChangesFilterChanged(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
