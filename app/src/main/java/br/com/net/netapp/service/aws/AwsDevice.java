package br.com.net.netapp.service.aws;

/**
 * Created by Leandro on 14/09/17.
 */

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.net.netapp.BuildConfig;
import br.com.net.netapp.manager.SessionManager;
import br.com.net.netapp.pref.NETAppPref_;

/**
 * Created by elourenco on 13/10/16.
 */

@EBean
public class AwsDevice {

    @Bean
    SessionManager sessionManager;

    @Pref
    NETAppPref_ netAppPref;

    public HashMap<String, String> getDeviceInfoMap() {
        HashMap<String, String> deviceInfoMap = new LinkedHashMap<>();

        deviceInfoMap.put("app", BuildConfig.APPLICATION_ID);
        deviceInfoMap.put("notificationToken", netAppPref.gcmToken().get());
        deviceInfoMap.put("logged", String.valueOf(sessionManager.isActive() ? "true" : "false"));

        if (sessionManager.isActive()) {
            deviceInfoMap.put("nome", sessionManager.getUser().getName());
            deviceInfoMap.put("userId", sessionManager.getUser().getDocumentId());
        }

        return deviceInfoMap;
    }
}
