package br.com.net.netapp.ui.component.customRecycler;

import android.content.Context;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.adapters.FastItemAdapter;

import java.util.List;

import br.com.net.netapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leandro on 05/06/17.
 */

public class CustomRecycler<T extends CustomRecyclerItem> extends FrameLayout {

    @BindView(R.id.fragment_event_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.fragment_event_progress_bar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.fragment_event_no_content_text)
    TextView noContentText;
    //We use this to know if the fragment is initialized with 0 values and show the loadingComponent instead
    //of the noContentView
    private boolean viewLoaded = false;
    private FastItemAdapter<T> itemAdapter;

    public CustomRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRecycler(Context context, CustomRecyclerListener<T> listener, boolean hasFixedSize) {
        super(context);
        setupRecycler(context, listener, hasFixedSize);
    }

    public CustomRecycler(Context context, CustomRecyclerListener<T> listener) {
        super(context);
        setupRecycler(context, listener, true);

    }

    private void setupRecycler(Context context, CustomRecyclerListener<T> listener,
                               boolean hasFixedSize) {
        View view = inflate(context, R.layout.custom_recycler_view_container, this);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setHasFixedSize(hasFixedSize);
        itemAdapter = new FastItemAdapter<>();
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            if (listener != null) {
                listener.onItemClick(v, item, position);
            } else {
                Log.d("TudusRecycler: Item clicked(%s) position: %s", item.toString());
            }
            return true;
        });
        itemAdapter.withOnLongClickListener((v, adapter, item, position) -> {
            listener.onLongItemClick(v, item, position);
            return true;
        });
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(itemAdapter);
    }

    public void add(T recyclerItem) {
        progressBar.setVisibility(View.INVISIBLE);
        noContentText.setVisibility(View.INVISIBLE);
        itemAdapter.add(recyclerItem);
    }

    public void add(List<T> items) {
        if (viewLoaded || !items.isEmpty()) {
            progressBar.setVisibility(View.INVISIBLE);
            itemAdapter.clear();
            itemAdapter.add(items);
            if (items.isEmpty()) {
                noContentText.setVisibility(View.VISIBLE);
            } else {
                noContentText.setVisibility(View.GONE);
            }
        }
    }

    public void removeAll() {
        int size = itemAdapter.getAdapterItemCount();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.itemAdapter.remove(0);
            }
        }
        noContentText.setVisibility(View.VISIBLE);
    }

    public void removeAll(boolean showPlaceholderText) {
        int size = itemAdapter.getAdapterItemCount();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.itemAdapter.remove(0);
            }
        }
        if (showPlaceholderText) {
            noContentText.setVisibility(View.VISIBLE);
        } else {
            noContentText.setVisibility(View.GONE);
        }

    }

    public void remove(int index) {
        itemAdapter.remove(index);
        if (itemAdapter.getItemCount() <= 0) {
            noContentText.setVisibility(View.VISIBLE);
        }
    }

    public boolean isViewLoaded() {
        return viewLoaded;
    }

    public void setViewLoaded(boolean viewLoaded) {
        this.viewLoaded = viewLoaded;
    }

    public void setNoContentText(String text) {
        noContentText.setText(text);
//        noContentText.setCompoundDrawablesWithIntrinsicBounds(
//                0, 0, 0, R.drawable.ic_favorite_none_36dp);
 }
}