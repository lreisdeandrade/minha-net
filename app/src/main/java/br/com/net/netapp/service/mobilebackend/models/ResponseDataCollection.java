package br.com.net.netapp.service.mobilebackend.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmModel;

public class ResponseDataCollection<T extends RealmModel> {
    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("requestId")
    private String requestId;

    @SerializedName("data")
    private RealmList<T> dataInfo;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public RealmList<T> getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(RealmList<T> dataInfo) {
        this.dataInfo = dataInfo;
    }
}
