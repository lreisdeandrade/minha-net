package br.com.net.netapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.ViewById;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.com.net.netapp.R;

/**
 * Created by Leandro on 10/05/17.
 */

@EBean
public class CalendarAdapter extends BaseAdapter {


    private static final String TAG = "CalendarioReprisesAdapter";
    private ArrayList<String> diasCalendarioNet;

    public int positionSelected;

    Context context;

    @ViewById
    TextView dayName, dayNumber;

    public CalendarAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return diasCalendarioNet.size();
    }

    @Override
    public Object getItem(int position) {
        return diasCalendarioNet.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layout = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layout.inflate(R.layout.item_calendar, null);
        }

        Date date = null;
        Calendar calendar = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yyyyMMdd").parse(diasCalendarioNet.get(position));
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String diaDaSemana = sdf.format(date);

        TextView dayName = (TextView) convertView.findViewById(R.id.day_name);
        TextView dayNumber = (TextView) convertView.findViewById(R.id.day_number);

        dayNumber.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        dayName.setText(diaDaSemana.substring(0, 3));

        if (position == positionSelected) {
            dayNumber.setTextColor(Color.parseColor("#7dd7ff"));
            dayName.setTextColor(Color.parseColor("#7dd7ff"));
        } else {
            dayNumber.setTextColor(context.getResources().getColor(R.color.white));
            dayName.setTextColor(context.getResources().getColor(R.color.white));

        }

        return convertView;
    }

    public void setDiasCalendario(ArrayList<String> diasCalendarioNet) {
        this.diasCalendarioNet = diasCalendarioNet;
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }
}
