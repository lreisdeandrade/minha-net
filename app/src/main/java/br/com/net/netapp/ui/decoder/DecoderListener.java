package br.com.net.netapp.ui.decoder;

import br.com.net.netapp.service.mobilebackend.models.Decoder;

/**
 * Created by Leandro on 27/07/17.
 */

public interface DecoderListener {

    void onDecoderSelected(Decoder decoder);
}
