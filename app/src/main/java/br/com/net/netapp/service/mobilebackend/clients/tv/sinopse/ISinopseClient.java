package br.com.net.netapp.service.mobilebackend.clients.tv.sinopse;

import java.util.HashMap;

import br.com.net.netapp.service.mobilebackend.models.Exhibition;
import br.com.net.netapp.service.mobilebackend.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Leandro on 17/05/17.
 */

public interface ISinopseClient {

    @GET("epg/exhibitions/{idExhibition}")
    Call<ResponseData<Exhibition>> loadExhibitionDetail(@Path("idExhibition") String idExhibition);

    @POST("epg/exhibitions/{idExhibition}/record")
    Call<ResponseData<Exhibition>> recordExhibition(@Header("x-access-token") String xAccessToken, @Path("idExhibition") String idExhibition, @Body HashMap<String, String> decoderId);
}
